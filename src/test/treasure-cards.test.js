import fabric from '../../node_modules/fabric/dist/fabric.js';

import { 
	Barbarian,
	Fimir
} from "../characters";

import { 
	goldTreasure,
	powerUpPotion,
	healingPotion,
	holyWater,
	wanderingMonster,
	trap,
	goldTreasureWithTimeLoss
} from "../treasure-cards";

import {
	_generateTestMapGraph
} from "./test-utils";

test('Testing wanderingMonster', () => {
	var mapGraph = _generateTestMapGraph(true);
	const canvasFabric = new fabric.fabric.Canvas('');
	const monsterDrawMock = jest.fn();
	const runMonsterTurnMock = jest.fn();
	Fimir.prototype.draw = monsterDrawMock;
	Fimir.prototype.runMonsterTurn = runMonsterTurnMock;

	var gameState = {
		"mapGraph": mapGraph,
		"fabricContext": canvasFabric,
	    "currentScenarioIsWon": false,
	    "currentScenarioIsLost": false,
	    "monstersInPlay": [],
	    "escapedCharacters": [],
	    "deadCharacters": [],
	    "currentScenarioIsOver": false,
	    "activeCharacter": null,
	    "trapsInPlay": [],
	    "wanderingMonster": Fimir
	};

	const character1 = new Barbarian("2-5", gameState);
	gameState.charactersInPlay = [character1];

	const wanderingMonsterFunc = wanderingMonster('');
	wanderingMonsterFunc(character1);
	expect(gameState.monstersInPlay.length).toBe(1);
	expect(character1.getCurrentNode().isAdjacent(
		gameState.monstersInPlay[0].coordinates
	)).toBe(true);
	expect(monsterDrawMock).toHaveBeenCalledTimes(1);
	expect(runMonsterTurnMock).toHaveBeenCalledTimes(1);

	// Testing that the monster appears in the only available 
	// free square in case other adjacents are blocked
	gameState = {
		"mapGraph": mapGraph,
		"fabricContext": canvasFabric,
	    "currentScenarioIsWon": false,
	    "currentScenarioIsLost": false,
	    "monstersInPlay": [],
	    "escapedCharacters": [],
	    "deadCharacters": [],
	    "currentScenarioIsOver": false,
	    "activeCharacter": null,
	    "trapsInPlay": [],
	    "wanderingMonster": Fimir
	};
	const character2 = new Barbarian("0-5", gameState);
	const character3 = new Barbarian("0-4", gameState);
	gameState.charactersInPlay = [character2, character3];

	wanderingMonsterFunc(character2);
	expect(gameState.monstersInPlay.length).toBe(1);
	expect(character2.getCurrentNode().isAdjacent(
		gameState.monstersInPlay[0].coordinates
	)).toBe(true);
	expect(gameState.monstersInPlay[0].coordinates).toBe("1-5");
	expect(monsterDrawMock).toHaveBeenCalledTimes(2);
	expect(runMonsterTurnMock).toHaveBeenCalledTimes(2);
});

test('Testing goldTreasure', () => {
	var mapGraph = _generateTestMapGraph(true);
	const canvasFabric = new fabric.fabric.Canvas('');

	var gameState = {
		"mapGraph": mapGraph,
		"fabricContext": canvasFabric,
	    "currentScenarioIsWon": false,
	    "currentScenarioIsLost": false,
	    "monstersInPlay": [],
	    "escapedCharacters": [],
	    "deadCharacters": [],
	    "currentScenarioIsOver": false,
	    "activeCharacter": null,
	    "wanderingMonster": Fimir
	};
	const character1 = new Barbarian("2-5", gameState);
	gameState.charactersInPlay = [character1];

	expect(character1.gold).toBe(0);
	const goldTreasureFunc = goldTreasure(50, "");
	goldTreasureFunc(character1);
	expect(character1.gold).toBe(50);
});

test('Testing goldTreasureWithTimeLoss', () => {
	var mapGraph = _generateTestMapGraph(true);
	const canvasFabric = new fabric.fabric.Canvas('');

	var gameState = {
		"mapGraph": mapGraph,
		"fabricContext": canvasFabric,
	    "currentScenarioIsWon": false,
	    "currentScenarioIsLost": false,
	    "monstersInPlay": [],
	    "escapedCharacters": [],
	    "deadCharacters": [],
	    "currentScenarioIsOver": false,
	    "activeCharacter": null,
	    "wanderingMonster": Fimir
	};
	const character1 = new Barbarian("2-5", gameState);
	gameState.charactersInPlay = [character1];

	expect(character1.gold).toBe(0);
	goldTreasureWithTimeLoss(character1);
	expect(character1.gold).toBeGreaterThan(0);
	expect(character1.losesTurn).toBe(true);
});

test('Testing trap', () => {
	var mapGraph = _generateTestMapGraph(true);
	const canvasFabric = new fabric.fabric.Canvas('');

	var gameState = {
		"mapGraph": mapGraph,
		"fabricContext": canvasFabric,
	    "currentScenarioIsWon": false,
	    "currentScenarioIsLost": false,
	    "monstersInPlay": [],
	    "escapedCharacters": [],
	    "deadCharacters": [],
	    "currentScenarioIsOver": false,
	    "activeCharacter": null,
	    "wanderingMonster": Fimir
	};
	const character1 = new Barbarian("2-5", gameState);
	gameState.charactersInPlay = [character1];
	const basicTrapFunc = trap(1, "");
	basicTrapFunc(character1);
	expect(character1.bodyPoints).toBe(7);
	expect(character1.hasMovedThisTurn).toBe(false);

	const pitTrapFunc = trap(1, "", true);
	pitTrapFunc(character1);
	expect(character1.bodyPoints).toBe(6);
	expect(character1.hasMovedThisTurn).toBe(true);
});

test('Testing powerUpPotion', () => {
	var mapGraph = _generateTestMapGraph(true);
	const canvasFabric = new fabric.fabric.Canvas('');

	var gameState = {
		"mapGraph": mapGraph,
		"fabricContext": canvasFabric,
	    "currentScenarioIsWon": false,
	    "currentScenarioIsLost": false,
	    "monstersInPlay": [],
	    "escapedCharacters": [],
	    "deadCharacters": [],
	    "currentScenarioIsOver": false,
	    "activeCharacter": null,
	    "wanderingMonster": Fimir
	};
	const character1 = new Barbarian("2-5", gameState);
	gameState.charactersInPlay = [character1];
	const potionFunc = powerUpPotion("", "", "someProp");
	potionFunc(character1);
	expect(character1.potions.length).toBe(1);
});

test('Testing healingPotion', () => {
	var mapGraph = _generateTestMapGraph(true);
	const canvasFabric = new fabric.fabric.Canvas('');

	var gameState = {
		"mapGraph": mapGraph,
		"fabricContext": canvasFabric,
	    "currentScenarioIsWon": false,
	    "currentScenarioIsLost": false,
	    "monstersInPlay": [],
	    "escapedCharacters": [],
	    "deadCharacters": [],
	    "currentScenarioIsOver": false,
	    "activeCharacter": null,
	    "wanderingMonster": Fimir
	};
	const character1 = new Barbarian("2-5", gameState);
	gameState.charactersInPlay = [character1];
	const potionFunc = healingPotion("", "");
	potionFunc(character1);
	expect(character1.potions.length).toBe(1);
});

test('Testing holyWater', () => {
	var mapGraph = _generateTestMapGraph(true);
	const canvasFabric = new fabric.fabric.Canvas('');

	var gameState = {
		"mapGraph": mapGraph,
		"fabricContext": canvasFabric,
	    "currentScenarioIsWon": false,
	    "currentScenarioIsLost": false,
	    "monstersInPlay": [],
	    "escapedCharacters": [],
	    "deadCharacters": [],
	    "currentScenarioIsOver": false,
	    "activeCharacter": null,
	    "wanderingMonster": Fimir
	};
	const character1 = new Barbarian("2-5", gameState);
	gameState.charactersInPlay = [character1];
	const potionFunc = holyWater("", "");
	potionFunc(character1);
	expect(character1.utilityObjects.length).toBe(1);
});
