import fabric from '../../node_modules/fabric/dist/fabric.js';
import { buildMapGraph } from '../map-building';

function _generateTestMapGraph(openDoor=false) {
	// A 3 x 4 room with a corridor surrounding it and an open door on the lower side
	const mapData = {
	    "0-0": {
	        "adjacencyList": ["0-1", "1-0"],
	        "rooms": [],
	        "corridor": true
	    },
	    "0-1": {
	        "adjacencyList": ["0-0", "0-2"],
	        "rooms": [],
	        "corridor": true
	    },
	    "0-2": {
	        "adjacencyList": ["0-1", "0-3"],
	        "rooms": [],
	        "corridor": true
	    },
	    "0-3": {
	        "adjacencyList": ["0-2", "0-4"],
	        "rooms": [],
	        "corridor": true
	    },
	    "0-4": {
	        "adjacencyList": ["0-3", "0-5"],
	        "rooms": [],
	        "corridor": true
	    },
	    "0-5": {
	        "adjacencyList": ["0-4", "1-5"],
	        "rooms": [],
	        "corridor": true
	    },
	    "1-0": {
	        "adjacencyList": ["0-0", "2-0"],
	        "rooms": [],
	        "corridor": true
	    },
	    "1-1": {
	        "adjacencyList": ["1-2", "2-1"],
	        "rooms": [0],
	    },
	    "1-2": {
	        "adjacencyList": ["1-1", "2-2"],
	        "rooms": [0],
	    },
	    "1-3": {
	        "adjacencyList": ["1-2", "2-3"],
	        "rooms": [0],
	    },
	    "1-4": {
	        "adjacencyList": ["1-3", "2-4"],
	        "rooms": [0],
	    },
	    "1-5": {
	        "adjacencyList": ["0-5", "2-5"],
	        "rooms": [],
	        "corridor": true
	    },
	    "2-0": {
	        "adjacencyList": ["1-0", "3-0"],
	        "rooms": [],
	        "corridor": true
	    },
	    "2-1": {
	        "adjacencyList": ["1-1", "3-1", "2-2"],
	        "rooms": [0],
	    },
	    "2-2": {
	        "adjacencyList": ["1-2", "2-1", "3-2", "2-3"],
	        "rooms": [0],
	    },
	    "2-3": {
	        "adjacencyList": ["2-2", "1-3", "3-3", "2-4"],
	        "rooms": [0],
	    },
	    "2-4": {
	        "adjacencyList": ["1-4", "3-4", "2-3"],
	        "rooms": [0],
	    },
	    "2-5": {
	        "adjacencyList": ["1-5", "3-5"],
	        "rooms": [],
	        "corridor": true
	    },
	    "3-0": {
	        "adjacencyList": ["2-0", "4-0"],
	        "rooms": [],
	        "corridor": true
	    },
	    "3-1": {
	        "adjacencyList": ["3-2", "2-1"],
	        "rooms": [0],
	    },
	    "3-2": {
	        "adjacencyList": ["3-1", "2-2"],
	        "rooms": [0],
	    },
	    "3-3": {
	        "adjacencyList": ["3-2", "2-3", "3-4"],
	        "rooms": [0],
	    },
	    "3-4": {
	        "adjacencyList": ["3-3", "2-4"],
	        "rooms": [0],
	    },
	    "3-5": {
	        "adjacencyList": ["2-5", "4-5"],
	        "rooms": [],
	        "corridor": true
	    },
	    "4-0": {
	        "adjacencyList": ["3-0", "4-1"],
	        "rooms": [],
	        "corridor": true
	    },
	    "4-1": {
	        "adjacencyList": ["4-0", "4-2"],
	        "rooms": [],
	        "corridor": true
	    },
	    "4-2": {
	        "adjacencyList": ["4-1", "4-3"],
	        "rooms": [],
	        "corridor": true
	    },
	    "4-3": {
	        "adjacencyList": ["4-2", "4-4"],
	        "rooms": [],
	        "corridor": true
	    },
	    "4-4": {
	        "adjacencyList": ["4-3", "4-5"],
	        "rooms": [],
	        "corridor": true
	    },
	    "4-5": {
	        "adjacencyList": ["4-4", "3-5"],
	        "rooms": [],
	        "corridor": true
	    },
	};
	const mapGraph = buildMapGraph(mapData);
	if (openDoor === true) {
		mapGraph.addEdge("2-4", "2-5");
	}
	return mapGraph;

}

function _generateGameState(openDoor=false) {
	const context = new fabric.fabric.Canvas('');
	const gameState = {
	    "mapGraph": _generateTestMapGraph(openDoor),
	    "activeCharacter": null,
	    "blocksInPlay": [],
	    "charactersInPlay": [],
	    "monstersInPlay": [],
	    "doorsInPlay": [],
	    "furnitureInPlay": [],
	    "trapsInPlay": [],
	    "currentScenario": null,
	    "fabricContext": context,
	};
	return gameState;
}

export { _generateTestMapGraph, _generateGameState };