import fabric from '../../node_modules/fabric/dist/fabric.js';

import { 
	Barbarian,
	Dwarf,
	Fimir
} from "../characters";

import {
	Armour,
	ProtectiveItem,
	Shield,
	Toolkit,
	Weapon
} from "../character-equipment";

import {
	Bookcase,
	Door,
	OneSquareBlock,
	PitTrap,
	SecretDoor,
	Table
 } from "../map-objects";

import { 
	characterOpensDoor
} from "../game-logic";

import {
	_generateTestMapGraph
} from "./test-utils";

test('Testing character.move', () => {
	// Setting up
	const mapGraph = _generateTestMapGraph();
	const canvasFabric = new fabric.fabric.Canvas('');
	const gameState = {"mapGraph": mapGraph, "fabricContext": canvasFabric, "trapsInPlay": []};

	// Closed door
	const door = new Door(["2-4", "2-5"], gameState, true);

	// Character in front of the door, outside the room
	const character1 = new Barbarian("2-5", gameState, true);

	// Character inside the room
	const character2 = new Barbarian("1-4", gameState, true);

	// Bookcase splitting the room in two
	const bookcase = new Bookcase(["2-1", "2-1"], ["2-1", "2-3"], gameState);
	bookcase.blockGridCoordinates();

	// Fimir inside the room
	const monster1 = new Fimir("3-2", gameState);

	gameState.monstersInPlay = [monster1];
	gameState.furnitureInPlay = [bookcase];
	gameState.doorsInPlay = [door];
	gameState.charactersInPlay = [character1, character2];
	gameState.blocksInPlay = [];
	gameState.activeCharacter = character1;

	// Mocking draw() methods on Table and Fimir as image imports
	// don't play well with Jest
	jest.spyOn(monster1, 'draw').mockImplementation(() => '' );
	jest.spyOn(bookcase, 'draw').mockImplementation(() => '' );
	const mockedDrawMovement = jest.spyOn(character1, 'drawMovement').mockImplementation(() => '' );

	// character1 opens the door, objects are revealed
	characterOpensDoor(door, character1, gameState);

	// Move to square occupied by other character - impossible
	character1.move(gameState.mapGraph.nodes.get("1-4"), gameState);
	expect(character1.coordinates).toBe("2-5");
	expect(character1.moves).toBe(6);
	expect(mockedDrawMovement).toHaveBeenCalledTimes(0);

	// Move through square occupied by enemy - impossible
	character1.move(gameState.mapGraph.nodes.get("3-1"), gameState);
	expect(character1.coordinates).toBe("2-5");
	expect(character1.moves).toBe(6);
	expect(mockedDrawMovement).toHaveBeenCalledTimes(0);

	// Move to square occupied by furniture - impossible
	character1.move(gameState.mapGraph.nodes.get("2-3"), gameState);
	expect(character1.coordinates).toBe("2-5");
	expect(character1.moves).toBe(6);
	expect(mockedDrawMovement).toHaveBeenCalledTimes(0);

	// Move to free square through square occupied by friendly character
	character1.move(gameState.mapGraph.nodes.get("1-2"), gameState);
	expect(character1.coordinates).toBe("1-2");
	expect(character1.moves).toBe(2);
	expect(mockedDrawMovement).toHaveBeenCalledTimes(1);

	// Move to free square without obstacles
	character1.moves = 6;
	character1.coordinates = "2-5";
	character1.move(gameState.mapGraph.nodes.get("0-4"), gameState);
	expect(character1.coordinates).toBe("0-4");
	expect(character1.moves).toBe(3);
	expect(mockedDrawMovement).toHaveBeenCalledTimes(2);

	// Move to free square outside movement limit - impossible
	character1.moves = 6;
	character1.coordinates = "2-5";
	character1.move(gameState.mapGraph.nodes.get("0-0"), gameState);
	expect(character1.coordinates).toBe("2-5");
	expect(character1.moves).toBe(6);
	expect(mockedDrawMovement).toHaveBeenCalledTimes(2);

	// Move to adjacent square with single move
	character1.moves = 1;
	character1.coordinates = "2-5";
	character1.move(gameState.mapGraph.nodes.get("3-5"), gameState);
	expect(character1.coordinates).toBe("3-5");
	expect(character1.moves).toBe(0);
	expect(mockedDrawMovement).toHaveBeenCalledTimes(3);

	// Move attempt while in pit trap- impossible
	character1.isInAPit = true;
	character1.moves = 6;
	character1.coordinates = "2-5";
	character1.move(gameState.mapGraph.nodes.get("2-3"), gameState);
	expect(character1.coordinates).toBe("2-5");
	expect(character1.moves).toBe(6);
	expect(mockedDrawMovement).toHaveBeenCalledTimes(3);
});

test('Testing character.move with traps', () => {
	// Setting up
	const mapGraph = _generateTestMapGraph();
	const canvasFabric = new fabric.fabric.Canvas('');
	const gameState = {"mapGraph": mapGraph, "fabricContext": canvasFabric};

	// Character in front of the door, outside the room
	const character1 = new Barbarian("1-1", gameState, true);

	// Trap inside the room
	const hiddenTrap = new PitTrap("1-3", gameState, false);
	const visibleTrap = new PitTrap("2-1", gameState, true);

	const mockHiddenTrapActivate = jest.spyOn(hiddenTrap, 'activate').mockImplementation(() => '');
	const mockVisibleTrapActivate = jest.spyOn(visibleTrap, 'activate').mockImplementation(() => '');
	const mockVisibleTrapBypass = jest.spyOn(visibleTrap, 'bypass').mockImplementation(() => '');
	const mockedDrawMovement = jest.spyOn(character1, 'drawMovement').mockImplementation(() => '' );

	gameState.monstersInPlay = [];
	gameState.furnitureInPlay = [];
	gameState.doorsInPlay = [];
	gameState.charactersInPlay = [character1];
	gameState.trapsInPlay = [hiddenTrap, visibleTrap];
	gameState.activeCharacter = character1;

	// Character walks over hidden trap
	character1.move(gameState.mapGraph.nodes.get("1-4"), gameState);
	expect(mockHiddenTrapActivate).toHaveBeenCalledTimes(1);
	expect(mockHiddenTrapActivate).toHaveBeenCalledWith(character1);

	// Character returns at starting point, does movement that includes revealed trap, attempting to bypass it
	character1.updateCoordinates("1-1");
	character1.move(gameState.mapGraph.nodes.get("3-1"), gameState);
	expect(mockVisibleTrapBypass).toHaveBeenCalledTimes(1);
	expect(mockVisibleTrapBypass).toHaveBeenCalledWith(character1);
});



test('Testing character.getImpassableCoordinates', () => {
	const mapGraph = _generateTestMapGraph();
	const canvasFabric = new fabric.fabric.Canvas('');
	const gameState = {"mapGraph": mapGraph, "fabricContext": canvasFabric};

	const character1 = new Barbarian("2-5", gameState);
	const character2 = new Barbarian("2-4", gameState);
	
	const monster1 = new Fimir("3-4", gameState);
	const monster2 = new Fimir("2-4", gameState);


	gameState.monstersInPlay = [monster1, monster2];
	gameState.charactersInPlay = [character1, character2];

	expect(character1.getImpassableCoordinates()).toEqual(["3-4", "2-4"]);
	expect(monster1.getImpassableCoordinates()).toEqual(["2-5", "2-4"]);
});

test('Testing character.getBlockedLastSquareCoordinates', () => {
	const mapGraph = _generateTestMapGraph();
	const canvasFabric = new fabric.fabric.Canvas('');
	const gameState = {"mapGraph": mapGraph, "fabricContext": canvasFabric, "trapsInPlay": []};

	const character1 = new Barbarian("2-5", gameState);
	const character2 = new Barbarian("2-4", gameState);
	
	const monster1 = new Fimir("3-4", gameState);
	const monster2 = new Fimir("2-4", gameState);

	gameState.monstersInPlay = [monster1, monster2];
	gameState.charactersInPlay = [character1, character2];

	expect(character1.getBlockedLastSquareCoordinates()).toEqual(["2-5", "2-4", "3-4", "2-4"]);
	expect(monster1.getBlockedLastSquareCoordinates()).toEqual(["2-5", "2-4", "3-4", "2-4"]);
});

test('Testing character.getAttackDiceValidValues', () => {
	const mapGraph = _generateTestMapGraph();
	const gameState = {"mapGraph": mapGraph};

	const character1 = new Barbarian("2-5", gameState);
	expect(character1.getAttackDiceValidValues()).toEqual([1, 2, 3]);
	
	const monster1 = new Fimir("3-4", gameState);
	expect(monster1.getAttackDiceValidValues()).toEqual([1, 2, 3]);
});

test('Testing character.getDefenseDiceValidValues', () => {
	const mapGraph = _generateTestMapGraph();
	const gameState = {"mapGraph": mapGraph};

	const character1 = new Barbarian("2-5", gameState);
	expect(character1.getDefenseDiceValidValues()).toEqual([4, 5]);
	
	const monster1 = new Fimir("3-4", gameState);
	expect(monster1.getDefenseDiceValidValues()).toEqual([6]);
});

test('Testing character.coordinatesAreInAttackRange', () => {
	const mapGraph = _generateTestMapGraph(true);
	const canvasFabric = new fabric.fabric.Canvas('');
	const gameState = {
		"mapGraph": mapGraph, 
		"fabricContext": canvasFabric,
		"monstersInPlay": [],
	};

	const character1 = new Barbarian("1-1", gameState);
	gameState.charactersInPlay = [character1];
	const adjacent = "1-2";
	const diagonal = "2-2";
	const inSightNotAdjacentSameRoom = "1-4";
	const inSightNotAdjacentDifferentRoom = "2-5";
	const diagonalNotInSight = "0-0";
	const adjacentNotInSight = "0-1";

	// Regular weapon
	expect(character1.coordinatesAreInAttackRange(adjacent)).toBe(true);
	expect(character1.coordinatesAreInAttackRange(diagonal)).toBe(false);
	expect(character1.coordinatesAreInAttackRange(inSightNotAdjacentSameRoom)).toBe(false);
	expect(character1.coordinatesAreInAttackRange(inSightNotAdjacentDifferentRoom)).toBe(false);
	expect(character1.coordinatesAreInAttackRange(diagonalNotInSight)).toBe(false);
	expect(character1.coordinatesAreInAttackRange(adjacentNotInSight)).toBe(false);

	// Weapon attacks diagonally
	const spear = new Weapon("Spear", "", 2, true);
	spear.equip(character1);

	expect(character1.coordinatesAreInAttackRange(adjacent)).toBe(true);
	expect(character1.coordinatesAreInAttackRange(diagonal)).toBe(true);
	expect(character1.coordinatesAreInAttackRange(inSightNotAdjacentSameRoom)).toBe(false);
	expect(character1.coordinatesAreInAttackRange(inSightNotAdjacentDifferentRoom)).toBe(false);
	expect(character1.coordinatesAreInAttackRange(diagonalNotInSight)).toBe(false);
	expect(character1.coordinatesAreInAttackRange(adjacentNotInSight)).toBe(false);

	// Ranged weapon
	const crossbow = new Weapon("Crossbow", "", 3, false, true);
	crossbow.equip(character1);

	expect(character1.coordinatesAreInAttackRange(adjacent)).toBe(false);
	expect(character1.coordinatesAreInAttackRange(diagonal)).toBe(true);
	expect(character1.coordinatesAreInAttackRange(inSightNotAdjacentSameRoom)).toBe(true);
	expect(character1.coordinatesAreInAttackRange(inSightNotAdjacentDifferentRoom)).toBe(true);
	expect(character1.coordinatesAreInAttackRange(diagonalNotInSight)).toBe(false);
	expect(character1.coordinatesAreInAttackRange(adjacentNotInSight)).toBe(false);

	// Thrown weapon
	const hatchet = new Weapon("Hatchet", "", 2, false, false, true);
	hatchet.equip(character1);

	expect(character1.coordinatesAreInAttackRange(adjacent)).toBe(true);
	expect(character1.coordinatesAreInAttackRange(diagonal)).toBe(true);
	expect(character1.coordinatesAreInAttackRange(inSightNotAdjacentSameRoom)).toBe(true);
	expect(character1.coordinatesAreInAttackRange(inSightNotAdjacentDifferentRoom)).toBe(true);
	expect(character1.coordinatesAreInAttackRange(diagonalNotInSight)).toBe(false);
	expect(character1.coordinatesAreInAttackRange(adjacentNotInSight)).toBe(false);

});

test('Testing character.attackTarget', () => {
	const mapGraph = _generateTestMapGraph();
	const canvasFabric = new fabric.fabric.Canvas('');
	const gameState = {"mapGraph": mapGraph, "fabricContext": canvasFabric};

	const character1 = new Barbarian("2-3", gameState);
	const monster1 = new Fimir("2-4", gameState);
	const monster2 = new Fimir("0-0", gameState);
	gameState.charactersInPlay = [character1];
	gameState.monstersInPlay = [monster1, monster2];

	const mockedRollAttackDice = jest.spyOn(character1, 'rollAttackDice').mockImplementation(() => [1, 1, 1]);
	const mockedResolveAttack1 = jest.spyOn(monster1, 'resolveAttack').mockImplementation(() => '');
	const mockedRollDefenseDice = jest.spyOn(monster1, 'rollDefenseDice').mockImplementation(() => [1, 2]);
	const mockedResolveAttack2 = jest.spyOn(monster2, 'resolveAttack').mockImplementation(() => '');

	// Attack not in range
	character1.attackEnemy(monster2);
	expect(mockedResolveAttack2).toHaveBeenCalledTimes(0);

	// Attack in range
	character1.attackEnemy(monster1);
	expect(mockedResolveAttack1).toHaveBeenCalledTimes(1);
	expect(mockedResolveAttack1).toHaveBeenCalledWith(character1, 3, 0, 'body');

	// Character has already attacked
	character1.hasActedThisTurn = true;
	character1.attackEnemy(monster1);
	expect(mockedResolveAttack1).toHaveBeenCalledTimes(1);
});


test('Testing character.attackTarget with thrown weapon', () => {
	const mapGraph = _generateTestMapGraph();
	const canvasFabric = new fabric.fabric.Canvas('');
	const gameState = {"mapGraph": mapGraph, "fabricContext": canvasFabric};

	const character1 = new Barbarian("1-1", gameState);
	const monster1 = new Fimir("1-2", gameState);
	const monster2 = new Fimir("1-3", gameState);
	gameState.charactersInPlay = [character1];
	gameState.monstersInPlay = [monster1, monster2];

	const mockedRollAttackDice = jest.spyOn(character1, 'rollAttackDice').mockImplementation(() => [1, 1]);
	const mockedResolveAttack1 = jest.spyOn(monster1, 'resolveAttack').mockImplementation(() => '');
	const mockedRollDefenseDice1 = jest.spyOn(monster1, 'rollDefenseDice').mockImplementation(() => [1, 2]);
	const mockedRollDefenseDice2 = jest.spyOn(monster2, 'rollDefenseDice').mockImplementation(() => [1, 2]);
	const mockedResolveAttack2 = jest.spyOn(monster2, 'resolveAttack').mockImplementation(() => '');

	const hatchet = new Weapon("Hatchet", "", 2, false, false, true);
	hatchet.equip(character1);

	// Attack against adjacent
	character1.attackEnemy(monster1);
	expect(mockedResolveAttack1).toHaveBeenCalledTimes(1);
	expect(mockedResolveAttack1).toHaveBeenCalledWith(character1, 2, 0, 'body');
	expect(character1.equippedWeapon).toBe(hatchet);

	// Resetting
	character1.hasActedThisTurn = false;

	// Attack against ranged
	character1.attackEnemy(monster2);
	expect(mockedResolveAttack2).toHaveBeenCalledTimes(1);
	expect(mockedResolveAttack2).toHaveBeenCalledWith(character1, 2, 0, 'body');
	expect(character1.equippedWeapon).toBe(null);
});



test('Testing character.resolveAttack', () => {
	const mapGraph = _generateTestMapGraph();
	const canvasFabric = new fabric.fabric.Canvas('');
	var gameState = {
		"mapGraph": mapGraph,
		"fabricContext": canvasFabric,
	    "currentScenarioIsWon": false,
	    "currentScenarioIsLost": false,
	    "escapedCharacters": [],
	    "deadCharacters": [],
	    "currentScenarioIsOver": false,
	    "activeCharacter": null
	};

	const character1 = new Barbarian("4-2", gameState);
	const character2 = new Barbarian("2-2", gameState);
	const monster1 = new Fimir("3-2", gameState);
	gameState.charactersInPlay = [character1, character2];
	gameState.monstersInPlay = [monster1];

	const mockedRemoveFromCanvasChar1 = jest.spyOn(
		character1, 'removeFromCanvas'
	).mockImplementation(() => [1, 2]);
	const mockedRemoveFromCanvasChar2 = jest.spyOn(
		character2, 'removeFromCanvas'
	).mockImplementation(() => '');
	const mockedRemoveFromCanvasMon1 = jest.spyOn(
		monster1, 'removeFromCanvas'
	).mockImplementation(() => '');

	// Fimir attack is parried by Barbarian
	character1.resolveAttack(monster1, 3, 3, 'body');
	// Assumes Barbarian starts with 8 body points
	expect(character1.bodyPoints).toBe(8);

	// Fimir attack wounds Barbarian - Barbarian has correct number of HP
	character1.resolveAttack(monster1, 3, 0, 'body');
	// Assumes Barbarian starts with 8 body points
	expect(character1.bodyPoints).toBe(5);

	// Fimir attack using psionic attack (!) wounds Barbarian - Barbarian has correct number of mind points
	character1.resolveAttack(monster1, 1, 0, 'mind');
	// Assumes Barbarian starts with 2 mind points
	expect(character1.mindPoints).toBe(1);

	// Fimir attack kills Barbarian - Barbarian removed from play
	character1.resolveAttack(monster1, 5, 0, 'body');
	expect(gameState.charactersInPlay.length).toBe(1);
	expect(gameState.charactersInPlay[0].coordinates).toBe(character2.coordinates);
	expect(gameState.deadCharacters.length).toBe(1);
	expect(gameState.deadCharacters[0].coordinates).toEqual(character1.coordinates);
	expect(mockedRemoveFromCanvasChar1).toHaveBeenCalledTimes(1);

	// Fimir attack kills Barbarian - Barbarian removed from play, scenario is lost
	character2.resolveAttack(monster1, 8, 0, 'body');
	expect(gameState.charactersInPlay).toEqual([]);
	expect(gameState.deadCharacters.length).toBe(2);
	expect(gameState.deadCharacters[0].coordinates).toEqual(character1.coordinates);
	expect(gameState.deadCharacters[1].coordinates).toEqual(character2.coordinates);
	expect(gameState.currentScenarioIsLost).toBe(true);
	expect(gameState.currentScenarioIsOver).toBe(true);

	// Resetting state
	character1.bodyPoints = 8;
	character2.bodyPoints = 8;
	gameState = {
		"mapGraph": mapGraph,
		"fabricContext": canvasFabric,
	    "currentScenarioIsWon": false,
	    "currentScenarioIsLost": false,
	    "deadCharacters": [],
	    "currentScenarioIsOver": false,
	    "charactersInPlay": [character1],
	    "escapedCharacters": [character2],
	    "monstersInPlay": [monster1]
	};
	character1.gameState = gameState;
	monster1.gameState = gameState;

	// Fimir attack kills Barbarian - Barbarian removed from play, scenario is over but not lost
	character1.resolveAttack(monster1, 8, 0, 'body');
	expect(gameState.deadCharacters.length).toBe(1);
	expect(gameState.deadCharacters[0].coordinates).toEqual(character1.coordinates);
	expect(gameState.currentScenarioIsLost).toBe(false);
	expect(gameState.currentScenarioIsOver).toBe(true);
	expect(mockedRemoveFromCanvasChar1).toHaveBeenCalledTimes(1);

	// Resetting state
	character1.bodyPoints = 8;
	gameState = {
		"mapGraph": mapGraph,
		"fabricContext": canvasFabric,
	    "currentScenarioIsWon": false,
	    "currentScenarioIsLost": false,
	    "deadCharacters": [],
	    "currentScenarioIsOver": false,
	    "charactersInPlay": [character1],
	    "escapedCharacters": [],
	    "monstersInPlay": [monster1],
	    "activeCharacter": character1
	};
	character1.gameState = gameState;
	monster1.gameState = gameState;

	// Barbarian kills Fimir - Fimir removed from play
	monster1.resolveAttack(character1, 1, 0, 'body');
	expect(gameState.monstersInPlay.length).toBe(0);
	expect(mockedRemoveFromCanvasMon1).toHaveBeenCalledTimes(1);
});

test('Testing monster.runMonsterTurn', () => {
	var mapGraph = _generateTestMapGraph(true);
	const canvasFabric = new fabric.fabric.Canvas('');
	var gameState = {
		"mapGraph": mapGraph,
		"fabricContext": canvasFabric,
	    "currentScenarioIsWon": false,
	    "currentScenarioIsLost": false,
	    "escapedCharacters": [],
	    "deadCharacters": [],
	    "trapsInPlay": [],
	    "currentScenarioIsOver": false,
	    "activeCharacter": null
	};

	const character1 = new Barbarian("1-1", gameState);
	const character2 = new Barbarian("1-5", gameState);
	const monster = new Fimir("2-5", gameState);
	gameState.charactersInPlay = [character1, character2];
	gameState.monstersInPlay = [monster];

	// Mock monster.attackEnemy and monster.drawMovement
	const mockedResolveAttack1 = jest.spyOn(
		character1, 'resolveAttack'
	).mockImplementation('');
	const mockedResolveAttack2 = jest.spyOn(
		character2, 'resolveAttack'
	).mockImplementation('');
	const mockedDrawMovement = jest.spyOn(
		monster, 'drawMovement'
	).mockImplementation('');
	const mockedAttackEnemy = jest.spyOn(
		monster, 'attackEnemy'
	).mockImplementation('');
	const mockedRollAttackDice = jest.spyOn(monster, 'rollAttackDice').mockImplementation(() => [1, 1]);
	const mockedRollDefenseDice = jest.spyOn(character1, 'rollDefenseDice').mockImplementation(() => [4, 4]);

	// Test that monster attacks adjacent character even
	// if there's another character available
	monster.runMonsterTurn();
	expect(mockedAttackEnemy).toHaveBeenCalledTimes(1);
	expect(mockedAttackEnemy).toHaveBeenCalledWith(character2);
	expect(mockedDrawMovement).toHaveBeenCalledTimes(0);

	// Test that monster attacks the closest character within
	// reach
	gameState.activeCharacter = null;
	character1.coordinates = "0-0";
	character2.coordinates = "1-1";
	monster.coordinates = "2-5";
	monster.runMonsterTurn();
	expect(mockedAttackEnemy).toHaveBeenCalledTimes(2);
	expect(mockedAttackEnemy).toHaveBeenCalledWith(character2);
	expect(mockedDrawMovement).toHaveBeenCalledTimes(1);

	// Test that monster tries to get close to the closest character
	// outside of attack range
	gameState.activeCharacter = null;
	character1.coordinates = "3-0";
	character2.coordinates = "2-0";
	monster.coordinates = "1-5";
	monster.runMonsterTurn();
	expect(mockedAttackEnemy).toHaveBeenCalledTimes(2);
	expect(mockedDrawMovement).toHaveBeenCalledTimes(2);
	expect(monster.coordinates).toBe("0-1");

	// Test that with no valid path monster doesn't do anything
	// In this case, the monster is shut outside the room where
	// the characters are
	mapGraph = _generateTestMapGraph();
	gameState.activeCharacter = null;
	gameState.mapGraph = mapGraph;
	character1.coordinates = "1-1";
	character2.coordinates = "2-1";
	monster.coordinates = "2-5";
	monster.runMonsterTurn();
	expect(mockedAttackEnemy).toHaveBeenCalledTimes(2);
	expect(mockedDrawMovement).toHaveBeenCalledTimes(2);
	expect(monster.coordinates).toBe("2-5");
});

test('Testing character.searchForTreasure', () => {
	var mapGraph = _generateTestMapGraph();
	const canvasFabric = new fabric.fabric.Canvas('');
	const specialSearch = jest.fn();
	var gameState = {
		"mapGraph": mapGraph,
		"fabricContext": canvasFabric,
	    "roomsSearchedForTreasure": [],
	    "corridorCoordinatesSearchedForTreasure": [],
	    "specialTreasureSearches": {
	    	0: {"func": specialSearch, "removeFromDeck": true}
	    },
	    "activeCharacter": null
	};

	const character1 = new Barbarian("1-1", gameState);
	const character2 = new Barbarian("0-3", gameState);
	const monster = new Fimir("2-5", gameState);
	monster.visible = true;
	gameState.charactersInPlay = [character1, character2];
	gameState.monstersInPlay = [monster];

	const mockedDrawTreasureCard = jest.spyOn(
		character1, 'drawTreasureCard'
	).mockImplementation('');
	const mockTurnCycle = jest.spyOn(
		character1, 'checkHeroTurnNeedsToCycle'
	).mockImplementation('');

	// Character searches for treasure in the room - there is a special search
	// which takes place
	character1.searchForTreasure();
	expect(specialSearch).toHaveBeenCalledTimes(1);
	expect(specialSearch).toHaveBeenCalledWith(character1);
	expect(gameState.specialTreasureSearches).toEqual({});
	expect(gameState.roomsSearchedForTreasure).toEqual([0]);
	expect(character1.hasActedThisTurn).toBe(true);

	// Check character can't check again after one search
	character1.searchForTreasure();
	expect(specialSearch).toHaveBeenCalledTimes(1);
	expect(character1.hasActedThisTurn).toBe(true);

	// Character tries to search for treasure in the room - no special search in place, should work
	character1.hasActedThisTurn = false;
	gameState.roomsSearchedForTreasure = [];

	character1.searchForTreasure();
	expect(mockedDrawTreasureCard).toHaveBeenCalledTimes(1);
	expect(gameState.roomsSearchedForTreasure).toEqual([0]);
	expect(character1.hasActedThisTurn).toBe(true);

	// Character tries to search for treasure in the room but this time there's a monster
	// - the search fails, no actions are spent
	character1.hasActedThisTurn = false;
	gameState.roomsSearchedForTreasure = [];
	const monster2 = new Fimir("2-1", gameState);
	monster2.visible = true;
	gameState.monstersInPlay = [monster, monster2];
	character1.searchForTreasure();
	expect(mockedDrawTreasureCard).toHaveBeenCalledTimes(1);
	expect(gameState.roomsSearchedForTreasure).toEqual([]);
	expect(character1.hasActedThisTurn).toBe(false);

	// Character tries to search for treasure in the corridor with a monster in sight
	// - the search fails, no actions are spent
	character1.coordinates = "1-5";

	character1.searchForTreasure();
	expect(mockedDrawTreasureCard).toHaveBeenCalledTimes(1);
	expect(gameState.corridorCoordinatesSearchedForTreasure).toEqual([]);
	expect(character1.hasActedThisTurn).toBe(false);

	// Character tries to search for treasure in the corridor with the other character -
	// should work, and there shouldn't be any visibility issue
	character1.coordinates = "0-2";
	character1.searchForTreasure();
	expect(mockedDrawTreasureCard).toHaveBeenCalledTimes(2);
	expect(gameState.roomsSearchedForTreasure).toEqual([]);
	expect(gameState.corridorCoordinatesSearchedForTreasure).toEqual(
		["0-0", "0-1", "0-2", "0-3", "0-4", "0-5"]
	);
	expect(character1.hasActedThisTurn).toBe(true);

	// Character tries again to search for treasure in the same corridor
	// - the search fails, no actions are spent
	character1.hasActedThisTurn = false;
	character1.searchForTreasure();
	expect(mockedDrawTreasureCard).toHaveBeenCalledTimes(2);
	expect(gameState.roomsSearchedForTreasure).toEqual([]);
	expect(gameState.corridorCoordinatesSearchedForTreasure).toEqual(
		["0-0", "0-1", "0-2", "0-3", "0-4", "0-5"]
	);
	expect(character1.hasActedThisTurn).toBe(false);
});

test('Testing character.drawTreasureCard', () => {
	var mapGraph = _generateTestMapGraph();
	const canvasFabric = new fabric.fabric.Canvas('');
	const treasureFunc = jest.fn();
	var gameState = {
		"mapGraph": mapGraph,
		"fabricContext": canvasFabric,
	    "roomsSearchedForTreasure": [],
	    "corridorCoordinatesSearchedForTreasure": [],
	    "activeCharacter": null,
	    "treasureDeck": [
			{"func": treasureFunc, "removeFromDeck": true}
	    ]
	};

	const character1 = new Barbarian("1-1", gameState);
	gameState.charactersInPlay = [character1];

	// Card that needs to be removed
	character1.drawTreasureCard();
	expect(treasureFunc).toHaveBeenCalledTimes(1);
	expect(treasureFunc).toHaveBeenCalledWith(character1);
	expect(gameState.treasureDeck).toEqual([]);

	// Card that goes back to deck
	gameState.treasureDeck = [{"func": treasureFunc, "removeFromDeck": false}];

	character1.drawTreasureCard();
	expect(treasureFunc).toHaveBeenCalledTimes(2);
	expect(treasureFunc).toHaveBeenCalledWith(character1);
	expect(gameState.treasureDeck).toEqual([{"func": treasureFunc, "removeFromDeck": false}]);
});

test('Testing character.checkTargetIsInSight', () => {
	var mapGraph = _generateTestMapGraph(true);
	const canvasFabric = new fabric.fabric.Canvas('');
	const specialSearch = jest.fn();
	var gameState = {
		"mapGraph": mapGraph,
		"fabricContext": canvasFabric,
	    "roomsSearchedForTreasure": [],
	    "corridorCoordinatesSearchedForTreasure": [],
	    "trapsInPlay": [],
	    "specialTreasureSearches": {
	    	0: {"func": specialSearch, "removeFromDeck": true}
	    },
	    "activeCharacter": null
	};

	const character1 = new Barbarian("1-1", gameState);
	const character2 = new Barbarian("0-4", gameState);
	const monster1 = new Fimir("1-3", gameState);
	const monster2 = new Fimir("0-5", gameState);
	monster1.visible = true;
	monster2.visible = true;
	gameState.charactersInPlay = [character1, character2];
	gameState.monstersInPlay = [monster1, monster2];

	// Character in room
	expect(character1.checkTargetIsInSight(monster1, false)).toBe(true);
	expect(character1.checkTargetIsInSight(character2, false)).toBe(false);
	expect(character1.checkTargetIsInSight(monster2, false)).toBe(false);

	expect(character1.checkTargetIsInSight(monster1)).toBe(true);
	expect(character1.checkTargetIsInSight(character2)).toBe(false);
	expect(character1.checkTargetIsInSight(monster2)).toBe(false);

	// Character in left corridor
	character1.coordinates = "0-2";
	expect(character1.checkTargetIsInSight(monster1, false)).toBe(false);
	expect(character1.checkTargetIsInSight(character2, false)).toBe(true);
	expect(character1.checkTargetIsInSight(monster2, false)).toBe(true);

	expect(character1.checkTargetIsInSight(monster1)).toBe(false);
	expect(character1.checkTargetIsInSight(character2)).toBe(true);
	expect(character1.checkTargetIsInSight(monster2)).toBe(false);

	// Character in upper corridor
	character1.coordinates = "3-0";
	expect(character1.checkTargetIsInSight(monster1, false)).toBe(false);
	expect(character1.checkTargetIsInSight(character2, false)).toBe(false);
	expect(character1.checkTargetIsInSight(monster2, false)).toBe(false);

	expect(character1.checkTargetIsInSight(monster1)).toBe(false);
	expect(character1.checkTargetIsInSight(character2)).toBe(false);
	expect(character1.checkTargetIsInSight(monster2)).toBe(false);

	// Character in lower corridor
	character1.coordinates = "2-5";
	expect(character1.checkTargetIsInSight(monster1, true)).toBe(true);
	expect(character1.checkTargetIsInSight(character2, true)).toBe(false);
	expect(character1.checkTargetIsInSight(monster2, true)).toBe(true);

	expect(character1.checkTargetIsInSight(monster1)).toBe(true);
	expect(character1.checkTargetIsInSight(character2)).toBe(false);
	expect(character1.checkTargetIsInSight(monster2)).toBe(true);

	// Character in room, monster on door
	character1.coordinates = "2-1";
	monster1.coordinates = "2-5";
	expect(character1.checkTargetIsInSight(monster1, true)).toBe(true);
	expect(character1.checkTargetIsInSight(character2, true)).toBe(false);
	expect(character1.checkTargetIsInSight(monster2, true)).toBe(false);

	expect(character1.checkTargetIsInSight(monster1)).toBe(true);
	expect(character1.checkTargetIsInSight(character2)).toBe(false);
	expect(character1.checkTargetIsInSight(monster2)).toBe(false);
});

test('Testing character.processDeath', () => {
	var mapGraph = _generateTestMapGraph(true);
	const canvasFabric = new fabric.fabric.Canvas('');
	var gameState = {
		"mapGraph": mapGraph,
		"fabricContext": canvasFabric,
	    "currentScenarioIsWon": false,
	    "currentScenarioIsLost": false,
	    "escapedCharacters": [],
	    "deadCharacters": [],
	    "currentScenarioIsOver": false,
	    "activeCharacter": null
	};

	const character1 = new Barbarian("1-1", gameState);
	const character2 = new Barbarian("1-5", gameState);
	gameState.charactersInPlay = [character1, character2];

	const mockedRemoveFromCanvas1 = jest.spyOn(
		character1, 'removeFromCanvas'
	).mockImplementation('');
	const mockedRemoveFromCanvas2 = jest.spyOn(
		character2, 'removeFromCanvas'
	).mockImplementation('');

	character1.processDeath();
	expect(gameState.charactersInPlay).toEqual([character2]);
	expect(mockedRemoveFromCanvas1).toHaveBeenCalledTimes(1);

	character2.processDeath();
	expect(gameState.charactersInPlay).toEqual([]);
	expect(mockedRemoveFromCanvas2).toHaveBeenCalledTimes(0);
	expect(gameState.currentScenarioIsLost).toBe(true);
	expect(gameState.currentScenarioIsOver).toBe(true);
});

test('Testing character.checkCharacterCanSearch', () => {
	var mapGraph = _generateTestMapGraph();
	const canvasFabric = new fabric.fabric.Canvas('');
	var gameState = {
		"mapGraph": mapGraph,
		"fabricContext": canvasFabric,
	    "roomsSearchedForTreasure": [],
	    "corridorCoordinatesSearchedForTreasure": [],
	    "activeCharacter": null
	};

	const character1 = new Barbarian("1-1", gameState);
	const character2 = new Barbarian("0-3", gameState);
	const monster = new Fimir("2-5", gameState);
	monster.visible = true;
	gameState.charactersInPlay = [character1, character2];
	gameState.monstersInPlay = [monster];

	const mockedDrawTreasureCard = jest.spyOn(
		character1, 'drawTreasureCard'
	).mockImplementation('');
	const mockTurnCycle = jest.spyOn(
		character1, 'checkHeroTurnNeedsToCycle'
	).mockImplementation('');

	// Character can search in the room
	expect(character1.checkCharacterCanSearch()).toBe(true);

	// Character tries to search in the room but this time there's a monster
	const monster2 = new Fimir("2-1", gameState);
	monster2.visible = true;
	gameState.monstersInPlay = [monster, monster2];
	character1.searchForTreasure();
	expect(character1.checkCharacterCanSearch()).toBe(false);

	// Character tries to search for treasure in the corridor with a monster in sight
	character1.coordinates = "1-5";
	expect(character1.checkCharacterCanSearch()).toBe(false);

	// Character tries to search for treasure in the corridor with a monster standing behind other character
	// - it should not be possible
	character1.coordinates = "0-2";
	monster.coordinates = "0-5";
	expect(character1.checkCharacterCanSearch()).toBe(false);

	// Character can't search if they have already acted
	character1.coordinates = "4-0";
	character1.hasActedThisTurn = true;
	expect(character1.checkCharacterCanSearch()).toBe(false);
});

test('Testing character.searchForSecretDoorsAndTraps', () => {
	var mapGraph = _generateTestMapGraph();
	const canvasFabric = new fabric.fabric.Canvas('');
	const specialTrapSearch = jest.fn();

	var gameState = {
		"mapGraph": mapGraph,
		"fabricContext": canvasFabric,
	    "roomsSearchedForTreasure": [],
	    "corridorCoordinatesSearchedForTreasure": [],
	    "activeCharacter": null,
	    "trapsInPlay": [],
		"secretDoorsInPlay": [],
		"doorsInPlay": [],
		"blocksInPlay": [],
		"furnitureInPlay": [],
		"roomsSearchedForTrapsAndSecretDoors": [],
		"corridorCoordinatesSearchedForSecretDoorsAndTraps": [],
		"specialTrapSearches": {
			0: {"func": specialTrapSearch, "removeFromDeck": true}
		}
	};

	const character1 = new Barbarian("1-1", gameState);
	const character2 = new Barbarian("0-3", gameState);
	const monster = new Fimir("2-5", gameState);
	const trapInRoom = new PitTrap("2-1", gameState);
	const trapInCorridor = new PitTrap("0-0", gameState);
	const secretDoor = new SecretDoor(["0-2", "1-2"], gameState);
	monster.visible = true;
	gameState.charactersInPlay = [character1, character2];
	gameState.monstersInPlay = [monster];
	gameState.trapsInPlay = [trapInRoom, trapInCorridor];
	gameState.secretDoorsInPlay = [secretDoor];

	const mockedTrapInRoomDraw = jest.spyOn(
		trapInRoom, 'draw'
	).mockImplementation('');
	const mockedTrapInCorridorDraw = jest.spyOn(
		trapInCorridor, 'draw'
	).mockImplementation('');
	const mockedSecretDoorDraw = jest.spyOn(
		secretDoor, 'draw'
	).mockImplementation('');

	// Character searches for secret doors and traps in the room - there's a special
	// trap search that gets triggered, plus 
	character1.searchForSecretDoorsAndTraps();
	expect(specialTrapSearch).toHaveBeenCalledTimes(1);
	expect(specialTrapSearch).toHaveBeenCalledWith(character1);
	expect(gameState.specialTrapSearches).toEqual({});
	expect(gameState.roomsSearchedForTrapsAndSecretDoors).toEqual([0]);
	expect(trapInRoom.visible).toBe(true);
	expect(mockedTrapInRoomDraw).toHaveBeenCalledTimes(1);
	expect(secretDoor.visible).toBe(true);
	expect(mockedSecretDoorDraw).toHaveBeenCalledTimes(1);
	const s1 = new Set(["0-1", "0-3", "1-2"]);
	const s2 = new Set(["1-1", "1-3", "0-2", "2-2"]);
	expect(mapGraph.nodes.get("0-2").getAdjacents()).toEqual(s1);
	expect(mapGraph.nodes.get("1-2").getAdjacents()).toEqual(s2);
	expect(trapInCorridor.visible).toBe(false);
	expect(mockedTrapInCorridorDraw).toHaveBeenCalledTimes(0);
	expect(gameState.roomsSearchedForTrapsAndSecretDoors).toEqual([0]);
	expect(character1.hasActedThisTurn).toBe(true);

	// Check character can't check again after one search
	trapInRoom.visible = false;
	gameState.roomsSearchedForTrapsAndSecretDoors = [];
	character1.searchForSecretDoorsAndTraps();
	expect(mockedTrapInRoomDraw).toHaveBeenCalledTimes(1);
	expect(trapInRoom.visible).toBe(false);
	expect(character1.hasActedThisTurn).toBe(true);

	// Character tries to search in the room but this time there's a monster
	// - the search fails, no actions are spent
	character1.hasActedThisTurn = false;
	gameState.roomsSearchedForTreasure = [];
	const monster2 = new Fimir("1-4", gameState);
	monster2.visible = true;
	gameState.monstersInPlay = [monster, monster2];
	character1.searchForSecretDoorsAndTraps();
	expect(mockedTrapInRoomDraw).toHaveBeenCalledTimes(1);
	expect(gameState.roomsSearchedForTrapsAndSecretDoors).toEqual([]);
	expect(trapInRoom.visible).toBe(false);
	expect(character1.hasActedThisTurn).toBe(false);

	// Character tries to search in the corridor with a monster in sight
	// - the search fails, no actions are spent
	character1.coordinates = "1-5";

	character1.searchForSecretDoorsAndTraps();
	expect(gameState.corridorCoordinatesSearchedForSecretDoorsAndTraps).toEqual([]);
	expect(trapInCorridor.visible).toBe(false);
	expect(mockedTrapInCorridorDraw).toHaveBeenCalledTimes(0);
	expect(character1.hasActedThisTurn).toBe(false);

	// Character tries to search in the corridor with the other character -
	// should work, and there shouldn't be any visibility issue
	character1.coordinates = "0-2";
	character1.searchForSecretDoorsAndTraps();
	expect(gameState.roomsSearchedForTrapsAndSecretDoors).toEqual([]);
	expect(gameState.corridorCoordinatesSearchedForSecretDoorsAndTraps).toEqual(
		["0-0", "0-1", "0-2", "0-3", "0-4", "0-5"]
	);
	expect(trapInCorridor.visible).toBe(true);
	expect(mockedTrapInCorridorDraw).toHaveBeenCalledTimes(1);
	expect(character1.hasActedThisTurn).toBe(true);

	// Character tries again to search for treasure in the same corridor
	// - the search fails, no actions are spent
	character1.hasActedThisTurn = false;
	character1.searchForSecretDoorsAndTraps();
	expect(gameState.roomsSearchedForTrapsAndSecretDoors).toEqual([]);
	expect(gameState.corridorCoordinatesSearchedForSecretDoorsAndTraps).toEqual(
		["0-0", "0-1", "0-2", "0-3", "0-4", "0-5"]
	);
	expect(character1.hasActedThisTurn).toBe(false);
});

test('Testing character.checkCanDisarmTraps', () => {
	var mapGraph = _generateTestMapGraph();
	var gameState = {
		"mapGraph": mapGraph,
	    "roomsSearchedForTreasure": [],
	    "corridorCoordinatesSearchedForTreasure": [],
	    "activeCharacter": null,
	    "trapsInPlay": [],
		"secretDoorsInPlay": [],
		"roomsSearchedForTrapsAndSecretDoors": [],
		"corridorCoordinatesSearchedForSecretDoorsAndTraps": [],
	};

	const character1 = new Barbarian("1-1", gameState);
	const character2 = new Barbarian("0-3", gameState);
	const toolkit = new Toolkit("");
	toolkit.addToInventory(character2);
	const character3 = new Dwarf("0-4", gameState);

	expect(character1.checkCanDisarmTraps()).toBe(false);
	expect(character2.checkCanDisarmTraps()).toBe(true);
	expect(character3.checkCanDisarmTraps()).toBe(true);
});

test('Testing character.disarmTrap', () => {
	var mapGraph = _generateTestMapGraph();
	var gameState = {
		"mapGraph": mapGraph,
	    "roomsSearchedForTreasure": [],
	    "corridorCoordinatesSearchedForTreasure": [],
	    "activeCharacter": null,
	    "trapsInPlay": [],
		"secretDoorsInPlay": [],
		"roomsSearchedForTrapsAndSecretDoors": [],
		"corridorCoordinatesSearchedForSecretDoorsAndTraps": [],
	};

	const character1 = new Barbarian("1-1", gameState);
	const trap1 = new PitTrap("2-1", gameState);
	const trap2 = new PitTrap("0-0", gameState);
	const mockedDisable1 = jest.spyOn(trap1, 'disable').mockImplementation(() => '' );
	const mockedDisable2 = jest.spyOn(trap2, 'disable').mockImplementation(() => '' );
	const mockedTurnCycle = jest.spyOn(character1, 'checkHeroTurnNeedsToCycle').mockImplementation(() => '' );


	// Character tries to disable trap without tools - fails
	character1.disarmTrap(trap1);
	character1.disarmTrap(trap2);
	expect(mockedDisable1).toHaveBeenCalledTimes(0);
	expect(mockedDisable2).toHaveBeenCalledTimes(0);

	// Character finds toolkit
	const toolkit = new Toolkit("");
	toolkit.addToInventory(character1);

	// Character tries to disable non-adjacent trap - fails
	character1.disarmTrap(trap2);
	expect(mockedDisable1).toHaveBeenCalledTimes(0);
	expect(mockedDisable2).toHaveBeenCalledTimes(0);

	// Character tries to disable adjacent trap - succeeds
	character1.disarmTrap(trap1);
	expect(mockedDisable1).toHaveBeenCalledTimes(1);
	expect(mockedDisable2).toHaveBeenCalledTimes(0);
	expect(character1.hasActedThisTurn).toBe(true);
	expect(mockedTurnCycle).toHaveBeenCalledTimes(1);
});

test('Testing hero.getAttackDice', () => {
	var mapGraph = _generateTestMapGraph();
	var gameState = {
		"mapGraph": mapGraph,
	    "roomsSearchedForTreasure": [],
	    "corridorCoordinatesSearchedForTreasure": [],
	    "activeCharacter": null,
	    "trapsInPlay": [],
		"secretDoorsInPlay": [],
		"roomsSearchedForTrapsAndSecretDoors": [],
		"corridorCoordinatesSearchedForSecretDoorsAndTraps": [],
	};

	const character1 = new Barbarian("1-1", gameState);
	const battleaxe = new Weapon("Battle Axe", "", 4);

	expect(character1.getAttackDice()).toBe(3);

	battleaxe.equip(character1);
	expect(character1.getAttackDice()).toEqual(battleaxe.attackDice);
});

test('Testing hero.hasEquippedWeapon', () => {
	var mapGraph = _generateTestMapGraph();
	var gameState = {
		"mapGraph": mapGraph,
	    "roomsSearchedForTreasure": [],
	    "corridorCoordinatesSearchedForTreasure": [],
	    "activeCharacter": null,
	    "trapsInPlay": [],
		"secretDoorsInPlay": [],
		"roomsSearchedForTrapsAndSecretDoors": [],
		"corridorCoordinatesSearchedForSecretDoorsAndTraps": [],
	};

	const character1 = new Barbarian("1-1", gameState);
	const battleaxe = new Weapon("Battle Axe", "", 4);

	expect(character1.hasEquippedWeapon()).toBe(false);

	battleaxe.equip(character1);
	expect(character1.hasEquippedWeapon()).toBe(true);
});

test('Testing hero.getDefenseDice', () => {
	var mapGraph = _generateTestMapGraph();
	var gameState = {
		"mapGraph": mapGraph,
	    "roomsSearchedForTreasure": [],
	    "corridorCoordinatesSearchedForTreasure": [],
	    "activeCharacter": null,
	    "trapsInPlay": [],
		"secretDoorsInPlay": [],
		"roomsSearchedForTrapsAndSecretDoors": [],
		"corridorCoordinatesSearchedForSecretDoorsAndTraps": [],
	};

	const character1 = new Barbarian("1-1", gameState);
	const plateArmour = new Armour("Plate Armour", "", 4);
	const shield = new Shield("Shield", "");
	const helmet = new ProtectiveItem("Helmet", "equippedHelmet", "");
	const bracers = new ProtectiveItem("Bracers", "equippedBracers", "");
	const cloak = new ProtectiveItem("Cloak", "equippedCloak", "");


	expect(character1.getDefenseDice()).toBe(2);

	plateArmour.equip(character1);
	shield.equip(character1);
	helmet.equip(character1);
	bracers.equip(character1);
	cloak.equip(character1);

	expect(character1.getDefenseDice()).toBe(
		plateArmour.defenseDice +
		shield.defenseDice + 
		helmet.defenseDice +
		bracers.defenseDice +
		cloak.defenseDice
	);
});

test('Testing character.hasEquippedArmour', () => {
	var mapGraph = _generateTestMapGraph();
	var gameState = {
		"mapGraph": mapGraph,
	    "roomsSearchedForTreasure": [],
	    "corridorCoordinatesSearchedForTreasure": [],
	    "activeCharacter": null,
	    "trapsInPlay": [],
		"secretDoorsInPlay": [],
		"roomsSearchedForTrapsAndSecretDoors": [],
		"corridorCoordinatesSearchedForSecretDoorsAndTraps": [],
	};

	const character1 = new Barbarian("1-1", gameState);
	const plateArmour = new Armour("Plate Armour", "", 4);

	expect(character1.hasEquippedArmour()).toBe(false);

	plateArmour.equip(character1);
	expect(character1.hasEquippedArmour()).toBe(true);
});

test('Testing character.hasEquippedShield', () => {
	var mapGraph = _generateTestMapGraph();
	var gameState = {
		"mapGraph": mapGraph,
	    "roomsSearchedForTreasure": [],
	    "corridorCoordinatesSearchedForTreasure": [],
	    "activeCharacter": null,
	    "trapsInPlay": [],
		"secretDoorsInPlay": [],
		"roomsSearchedForTrapsAndSecretDoors": [],
		"corridorCoordinatesSearchedForSecretDoorsAndTraps": [],
	};

	const character1 = new Barbarian("1-1", gameState);
	const shield = new Shield("Shield", "");

	expect(character1.hasEquippedShield()).toBe(false);

	shield.equip(character1);
	expect(character1.hasEquippedShield()).toBe(true);
});
