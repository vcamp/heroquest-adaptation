import fabric from '../../node_modules/fabric/dist/fabric.js';

import { 
	checkTargetIsVisible,
	characterOpensDoor,
} from "../game-logic";

import {
	Bookcase,
	Door,
	OneSquareBlock,
	Table
 } from "../map-objects";

import { 
	Barbarian,
	Fimir
 } from "../characters";

import {
	_generateTestMapGraph
} from "./test-utils";

beforeEach(() => {
  jest.resetAllMocks();
});

test('Testing checkTargetIsVisible', () => {
	const mapGraph = _generateTestMapGraph(true);
	// Observer position is in front of the door, outside the room
	var startingNode = mapGraph.nodes.get('2-5');

	// Target straight ahead, inside the room
	var targetNode = mapGraph.nodes.get('2-3');
	expect(checkTargetIsVisible(startingNode, targetNode, mapGraph)).toBe(true);

	// Target on a diagonal tile, inside the room
	targetNode = mapGraph.nodes.get('1-4');
	expect(checkTargetIsVisible(startingNode, targetNode, mapGraph)).toBe(true);
	
	// Target is on the same corridor
	targetNode = mapGraph.nodes.get('3-5');
	expect(checkTargetIsVisible(startingNode, targetNode, mapGraph)).toBe(true);

	// Target is on the left corridor, behind a corner
	targetNode = mapGraph.nodes.get('0-2');
	expect(checkTargetIsVisible(startingNode, targetNode, mapGraph)).toBe(false);

	// Target is on the other side of the room
	targetNode = mapGraph.nodes.get('2-0');
	expect(checkTargetIsVisible(startingNode, targetNode, mapGraph)).toBe(false);

});

test('Testing characterOpensDoor - from outside the room', () => {
	// Setting up
	const mapGraph = _generateTestMapGraph();
	const canvasFabric = new fabric.fabric.Canvas('');
	const gameState = {"mapGraph": mapGraph, "fabricContext": canvasFabric};

	// Closed door #1
	const door = new Door(["2-4", "2-5"], gameState, true);

	// Other door, not visible and on the opposite side of the room
	const doorInRoom = new Door(["2-0", "2-1"], gameState);

	// Character in front of the door, outside the room
	const character1 = new Barbarian("2-5", gameState);
	
	// Character in the corridor
	const character2 = new Barbarian("0-0", gameState);
	// Table inside the room
	const table = new Table(["2-1", "3-1"], ["2-1", "2-2"], gameState);

	// Fimir inside the room
	const monster = new Fimir("3-4", gameState);

	gameState.monstersInPlay = [monster];
	gameState.furnitureInPlay = [table];
	gameState.doorsInPlay = [door, doorInRoom];


	// Mocking draw() methods on Table and Fimir as image imports
	// don't play well with Jest
	jest.spyOn(table, 'draw').mockImplementation(() => '' );
	jest.spyOn(monster, 'draw').mockImplementation(() => '' );
	jest.spyOn(doorInRoom, 'draw').mockImplementation(() => '' );

	// character2 tries to open the door - nothing should happen
	characterOpensDoor(door, character2, gameState);
	expect(door.open).toBe(false);
	expect(monster.visible).toBe(false);
	expect(table.visible).toBe(false);
	expect(doorInRoom.visible).toBe(false);
	expect(mapGraph.nodes.get("2-4").getAdjacents()).toEqual(expect.not.arrayContaining(["2-5"]));
	expect(mapGraph.nodes.get("2-5").getAdjacents()).toEqual(expect.not.arrayContaining(["2-4"]));

	// character1 opens the door - we expect the table, the Fimir and the door inside to become visible
	// and the door itself to open
	characterOpensDoor(door, character1, gameState);
	expect(door.open).toBe(true);
	expect(monster.visible).toBe(true);
	expect(table.visible).toBe(true);
	expect(doorInRoom.visible).toBe(true);
	expect(doorInRoom.open).toBe(false);
	const s1 = new Set(["2-5", "1-4", "3-4", "2-3"]);
	const s2 = new Set(["2-4", "1-5", "3-5"]);
	expect(mapGraph.nodes.get("2-4").getAdjacents()).toEqual(s1);
	expect(mapGraph.nodes.get("2-5").getAdjacents()).toEqual(s2);
});

test('Testing characterOpensDoor - from inside the room', () => {
	// Setting up
	const mapGraph = _generateTestMapGraph();
	const canvasFabric = new fabric.fabric.Canvas('');
	const gameState = {"mapGraph": mapGraph, "fabricContext": canvasFabric};

	// Closed door
	const door = new Door(["2-4", "2-5"], gameState, true);

	// Character in front of the door, inside the room
	const character = new Barbarian("2-4", gameState);

	// Fimir outside the room, in the same corridor
	const monsterVisible = new Fimir("1-5", gameState);

	// Fimir outside the room, in a different corridor
	const monsterNotVisible = new Fimir("0-0", gameState);

	// Block in the same corridor
	const blockVisible = new OneSquareBlock(["4-5"], ["4-5"], gameState);

	// Block on the other side of the map
	const blockNotVisible = new OneSquareBlock(["4-0"], ["4-0"], gameState);


	// Door in the same corridor - this should already be visible from the room
	// but for the purpose of the test it can work
	const otherDoorVisible = new Door(["3-5", "3-4"], gameState, false);

	gameState.monstersInPlay = [monsterVisible, monsterNotVisible];
	gameState.doorsInPlay = [door, otherDoorVisible];
	gameState.blocksInPlay = [blockVisible, blockNotVisible];

	// Mocking draw() methods on the various map objects as image imports
	// don't play well with Jest
	jest.spyOn(monsterVisible, 'draw').mockImplementation(() => '' );
	jest.spyOn(otherDoorVisible, 'draw').mockImplementation(() => '' );
	jest.spyOn(blockVisible, 'draw').mockImplementation(() => '' );

	// character tries to open the door - monsterVisible and otherDoorVisible
	// should be revealed
	characterOpensDoor(door, character, gameState);
	expect(door.open).toBe(true);
	expect(monsterVisible.visible).toBe(true);
	expect(monsterNotVisible.visible).toBe(false);
	expect(otherDoorVisible.visible).toBe(true);
	expect(blockVisible.visible).toBe(true);
	expect(blockNotVisible.visible).toBe(false);
	const s1 = new Set(["2-5", "1-4", "3-4", "2-3"]);
	const s2 = new Set(["2-4", "1-5", "3-5"]);
	expect(mapGraph.nodes.get("2-4").getAdjacents()).toEqual(s1);
	expect(mapGraph.nodes.get("2-5").getAdjacents()).toEqual(s2);

});

test('Testing characterOpensDoor - cannot be opened multiple times', () => {
	// Setting up
	const mapGraph = _generateTestMapGraph();
	const canvasFabric = new fabric.fabric.Canvas('');
	const gameState = {"mapGraph": mapGraph, "fabricContext": canvasFabric};

	// Closed door
	const door = new Door(["2-4", "2-5"], gameState, true);

	// Character in front of the door, inside the room
	const character = new Barbarian("2-4", gameState);

	gameState.doorsInPlay = [door];
	gameState.monstersInPlay = [];
	gameState.blocksInPlay = [];

	const mockedOpenDoor = jest.spyOn(door, 'openDoor');

	// Character opens door; mocked draw function called once
	characterOpensDoor(door, character, gameState);
	expect(door.open).toBe(true);
	expect(mockedOpenDoor).toHaveBeenCalledTimes(1);

	// Character tries to open door again; mocked draw doesn't get called again
	characterOpensDoor(door, character, gameState);
	expect(mockedOpenDoor).toHaveBeenCalledTimes(1);

});
