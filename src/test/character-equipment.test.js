import { 
	Barbarian,
	Fimir,
	Wizard
} from "../characters";

import {
	_generateTestMapGraph
} from "./test-utils";

import {
	Armour,
	EquippableItem,
	HealingPotion,
	HolyWater,
	PowerUpPotion,
	ProtectiveItem,
	Shield,
	Weapon
} from "../character-equipment";

import fabric from '../../node_modules/fabric/dist/fabric.js';

test('Testing PowerUpPotion sets correct property', () => {
	var gameState = {
		"mapGraph": null,
		"fabricContext": null,
	    "currentScenarioIsWon": false,
	    "currentScenarioIsLost": false,
	    "escapedCharacters": [],
	    "deadCharacters": [],
	    "currentScenarioIsOver": false,
	    "activeCharacter": null
	};

	const character1 = new Barbarian("4-2", gameState);
	const potion = new PowerUpPotion("Potion of Resilience", "", "potionOfResilience");
	potion.addToInventory(character1);
	expect(character1.potions).toEqual([potion]);

	potion.drink(character1);
	expect(character1.potionOfResilience).toBe(true);
	expect(character1.potions).toEqual([]);
});

test('Testing HealingPotion', () => {
	var gameState = {
		"mapGraph": null,
		"fabricContext": null,
	    "currentScenarioIsWon": false,
	    "currentScenarioIsLost": false,
	    "escapedCharacters": [],
	    "deadCharacters": [],
	    "currentScenarioIsOver": false,
	    "activeCharacter": null
	};

	const character1 = new Barbarian("4-2", gameState);
	const potion = new HealingPotion("Healing Potion", "");
	character1.bodyPoints = 5;

	potion.addToInventory(character1);
	expect(character1.potions).toEqual([potion]);

	potion.drink(character1);
	expect(character1.bodyPoints).toBe(8);
	expect(character1.potions).toEqual([]);
});

test('Testing EquippableItem', () => {
	var gameState = {
		"mapGraph": null,
		"fabricContext": null,
	    "currentScenarioIsWon": false,
	    "currentScenarioIsLost": false,
	    "escapedCharacters": [],
	    "deadCharacters": [],
	    "currentScenarioIsOver": false,
	    "activeCharacter": null
	};

	const character1 = new Barbarian("4-2", gameState);
	const item1 = new EquippableItem("Knife", "", "weapons", "equippedWeapon");
	const item2 = new EquippableItem("Shiv", "", "weapons", "equippedWeapon");

	item1.equip(character1);
	expect(character1.equippedWeapon).toEqual(item1);
	expect(character1.weapons).toEqual([]);

	item2.equip(character1);
	expect(character1.equippedWeapon).toEqual(item2);
	expect(character1.weapons).toEqual([item1]);
});

test('Testing wizard-allowed weapons', () => {
	var gameState = {
		"mapGraph": null,
		"fabricContext": null,
	};

	const wizard = new Wizard("4-3", gameState);
	const allowedWeapon = new Weapon("Staff", "", "weapons", "equippedWeapon", false, false, false, true);
	const notAllowedWeapon = new Weapon("Sword", "", "weapons", "equippedWeapon");

	notAllowedWeapon.equip(wizard);
	expect(wizard.equippedWeapon).toBe(null);

	allowedWeapon.equip(wizard);
	expect(wizard.equippedWeapon).toEqual(allowedWeapon);
});

test('Testing armours and protective items forbidden to wizards', () => {
	var gameState = {
		"mapGraph": null,
		"fabricContext": null,
	};

	const wizard = new Wizard("4-3", gameState);

	const notAllowedArmour = new Armour("Plate Armour", "");
	const notAllowedItem = new ProtectiveItem("Helmet", "equippedHelmet", "");
	const notAllowedShield = new Shield("Shield", "");

	notAllowedArmour.equip(wizard);
	expect(wizard.equippedArmour).toBe(null);

	notAllowedItem.equip(wizard);
	expect(wizard.equippedHelmet).toBe(null);

	notAllowedShield.equip(wizard);
	expect(wizard.equippedShield).toBe(null);
});

test('Testing wizard-only armours and protective items ', () => {
	var gameState = {
		"mapGraph": null,
		"fabricContext": null,
	};

	const wizard = new Wizard("4-3", gameState);
	const barbarian = new Barbarian("4-2", gameState);

	const wizardOnlyArmour = new Armour("Magic Armour", "", 1, true);
	const wizardOnlyItem = new ProtectiveItem("Headband", "equippedHelmet", "", "protectiveObjects", 1, true);
	const wizardOnlyShield = new Shield("Magic Shield", "", 1, true);

	wizardOnlyArmour.equip(wizard);
	expect(wizard.equippedArmour).toEqual(wizardOnlyArmour);
	wizardOnlyArmour.equip(barbarian);
	expect(barbarian.equippedArmour).toBe(null);

	wizardOnlyItem.equip(wizard);
	expect(wizard.equippedHelmet).toEqual(wizardOnlyItem);
	wizardOnlyItem.equip(barbarian);
	expect(barbarian.equippedHelmet).toBe(null);

	wizardOnlyShield.equip(wizard);
	expect(wizard.equippedShield).toEqual(wizardOnlyShield);
	wizardOnlyShield.equip(barbarian);
	expect(barbarian.equippedShield).toBe(null);
});

test('Testing two-handed weapons and shields equipment', () => {
	var gameState = {
		"mapGraph": null,
		"fabricContext": null,
	};

	const barbarian = new Barbarian("4-2", gameState);
	const twoHandedAxe = new Weapon("Great Axe", "", 4, false, false, false, true);
	const shield = new Shield("Shield", "");

	shield.equip(barbarian);
	twoHandedAxe.equip(barbarian);
	expect(barbarian.equippedShield).toEqual(shield);
	expect(barbarian.equippedWeapon).toBe(null);

	shield.unequip(barbarian);
	twoHandedAxe.equip(barbarian);
	shield.equip(barbarian);
	expect(barbarian.equippedShield).toBe(null);
	expect(barbarian.equippedWeapon).toEqual(twoHandedAxe);
});

test('Testing heavy armour', () => {
	var gameState = {
		"mapGraph": null,
		"fabricContext": null,
	};
	const character1 = new Barbarian("1-1", gameState);
	const plateArmour = new Armour("Plate Armour", "", 4, false, true);
	expect(character1.getMovesDice()).toBe(2);
	plateArmour.equip(character1);
	expect(character1.getMovesDice()).toBe(1);
	plateArmour.unequip(character1);
	expect(character1.getMovesDice()).toBe(2);
});

test('Testing two-handed equipment', () => {
	var gameState = {
		"mapGraph": null,
		"fabricContext": null,
	};

	const barbarian = new Barbarian("4-2", gameState);
	gameState.charactersInPlay = [barbarian];

	const twoHandedAxe = new Weapon("Great Axe", "", 4, false, false, false, true);
	const shield = new Shield("Shield", "");

	shield.equip(barbarian);
	twoHandedAxe.equip(barbarian);
	expect(barbarian.equippedShield).toEqual(shield);
	expect(barbarian.equippedWeapon).toBe(null);

	shield.unequip(barbarian);
	twoHandedAxe.equip(barbarian);
	shield.equip(barbarian);
	expect(barbarian.equippedShield).toBe(null);
	expect(barbarian.equippedWeapon).toEqual(twoHandedAxe);
});

test('Testing Heroic Brew', () => {
	const mapGraph = _generateTestMapGraph();
	const canvasFabric = new fabric.fabric.Canvas('');
	const gameState = {"mapGraph": mapGraph, "fabricContext": canvasFabric};

	const character1 = new Barbarian("2-3", gameState);
	gameState.charactersInPlay = [character1];

	const monster1 = new Fimir("2-4", gameState);
	const monster2 = new Fimir("1-3", gameState);
	gameState.monstersInPlay = [monster1, monster2];

	const mockedRollAttackDice = jest.spyOn(character1, 'rollAttackDice').mockImplementation(() => [6]);
	const mockedResolveAttack1 = jest.spyOn(monster1, 'resolveAttack');
	const mockedResolveAttack2 = jest.spyOn(monster2, 'resolveAttack');
	const potion = new PowerUpPotion("Heroic Brew", "", "doubleAttack");

	// Clean starting state
	expect(character1.doubleAttack).toBe(false);

	// Character drinks potion
	potion.drink(character1);
	expect(character1.doubleAttack).toBe(true);
	expect(character1.hasAttackedThisTurn).toBe(false);
	expect(character1.hasActedThisTurn).toBe(false);

	// Character starts attacking
	character1.attackEnemy(monster1);
	expect(character1.doubleAttack).toBe(true);
	expect(character1.hasAttackedThisTurn).toBe(true);
	expect(character1.hasActedThisTurn).toBe(false);
	expect(mockedResolveAttack1).toHaveBeenCalledTimes(1);

	// Character is able to attack again
	character1.attackEnemy(monster2);
	expect(character1.doubleAttack).toBe(false);
	expect(character1.hasAttackedThisTurn).toBe(true);
	expect(character1.hasActedThisTurn).toBe(true);
	expect(mockedResolveAttack2).toHaveBeenCalledTimes(1);

	// Character can't attack anymore
	character1.attackEnemy(monster1);
	expect(character1.doubleAttack).toBe(false);
	expect(character1.hasAttackedThisTurn).toBe(true);
	expect(character1.hasActedThisTurn).toBe(true);
	expect(mockedResolveAttack1).toHaveBeenCalledTimes(1);
});

test('Testing Potion of Strength', () => {
	const mapGraph = _generateTestMapGraph();
	const canvasFabric = new fabric.fabric.Canvas('');
	const gameState = {"mapGraph": mapGraph, "fabricContext": canvasFabric};

	const character1 = new Barbarian("2-3", gameState);
	const potion = new PowerUpPotion("Potion of Strength", "", "potionOfStrength");

	// Clean starting state
	expect(character1.potionOfStrength).toBe(false);

	// Character drinks potion
	potion.drink(character1);
	expect(character1.potionOfStrength).toBe(true);

	// First call
	expect(character1.getAttackDice()).toBe(5);
	expect(character1.potionOfStrength).toBe(false);

	// Second call - potion has no more effect
	expect(character1.getAttackDice()).toBe(3);
	expect(character1.potionOfStrength).toBe(false);
});

test('Testing Potion of Speed', () => {
	const mapGraph = _generateTestMapGraph();
	const canvasFabric = new fabric.fabric.Canvas('');
	const gameState = {"mapGraph": mapGraph, "fabricContext": canvasFabric};

	const character1 = new Barbarian("2-3", gameState);
	const potion = new PowerUpPotion("Potion of Speed", "", "doubleMovement");

	// Clean starting state
	expect(character1.doubleMovement).toBe(false);

	// Character drinks potion
	potion.drink(character1);
	expect(character1.doubleMovement).toBe(true);

	// First call
	expect(character1.getMovesDice()).toBe(4);
	expect(character1.doubleMovement).toBe(false);

	// Second call - potion has no more effect
	expect(character1.getMovesDice()).toBe(2);
	expect(character1.doubleMovement).toBe(false);
});

test('Testing Potion of Resilience', () => {
	const mapGraph = _generateTestMapGraph();
	const canvasFabric = new fabric.fabric.Canvas('');
	const gameState = {"mapGraph": mapGraph, "fabricContext": canvasFabric};

	const character1 = new Barbarian("2-3", gameState);
	const potion = new PowerUpPotion("Potion of Resilience", "", "potionOfResilience");

	// Clean starting state
	expect(character1.potionOfResilience).toBe(false);

	// Character drinks potion
	potion.drink(character1);
	expect(character1.potionOfResilience).toBe(true);

	// First call
	expect(character1.getDefenseDice()).toBe(4);
	expect(character1.potionOfResilience).toBe(false);

	// Second call - potion has no more effect
	expect(character1.getDefenseDice()).toBe(2);
	expect(character1.potionOfResilience).toBe(false);
});
