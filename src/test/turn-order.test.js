import fabric from '../../node_modules/fabric/dist/fabric.js';

import { 
	cycleCharactersTurn,
	runMonstersTurn
} from "../turn-order";

import { 
	Barbarian,
	Fimir
 } from "../characters";

import {
	_generateTestMapGraph
} from "./test-utils";

beforeEach(() => {
  jest.resetAllMocks();
});

test('Testing cycleCharactersTurn', () => {
	const gameState = {"currentScenarioIsOver": false};

	const character1 = new Barbarian("2-5", gameState, true);
	const character2 = new Barbarian("1-4", gameState, true);
	character2.isInAPit = true;
	const monster = new Fimir("3-4", gameState);

	gameState.monstersInPlay = [monster];
	gameState.charactersInPlay = [character1, character2];
	gameState.activeCharacter = character1;

	cycleCharactersTurn(gameState);
	expect(character1.hasActedThisTurn).toBe(true);
	expect(character2.hasActedThisTurn).toBe(false);
	expect(character2.isInAPit).toBe(false);
	// Can't do equality check based on serialization due to circular references
	expect(gameState.activeCharacter.coordinates).toBe(character2.coordinates);

	cycleCharactersTurn(gameState);
	expect(character1.hasActedThisTurn).toBe(false);
	expect(character2.hasActedThisTurn).toBe(false);
});

test('Testing runMonstersTurn', () => {
	const gameState = {"currentScenarioIsOver": false};

	const character1 = new Barbarian("2-5", gameState, true);
	const character2 = new Barbarian("1-4", gameState, true);
	const monster = new Fimir("3-4", gameState);

	gameState.monstersInPlay = [monster];
	gameState.charactersInPlay = [character1, character2];
	gameState.activeCharacter = null;

	// Mock monster.runMonsterTurn
	const mockedRunMonsterTurn = jest.spyOn(
		monster, 'runMonsterTurn'
	).mockImplementation('');

	// Check that if all monsters are not visible nothing gets called
	runMonstersTurn(gameState);
	expect(mockedRunMonsterTurn).toHaveBeenCalledTimes(0);

	// Check that if there is a visible monster they run their turn
	gameState.activeCharacter = null;
	monster.visible = true;
	runMonstersTurn(gameState);
	expect(mockedRunMonsterTurn).toHaveBeenCalledTimes(1);
});
