import {
	Door,
    FallingBoulderTrap,
    OneSquareBlock,
	PieceOfFurniture,
    PitTrap,
    SpearTrap,
	Table,
    Trap
} from "../map-objects";

import { 
    Barbarian
} from "../characters";


import {
    _generateTestMapGraph,
	_generateGameState
} from "./test-utils";

test('Testing top left grid coordinate of door is calculated correctly', () => {
    const horizontalDoor1 = new Door(["2-9", "2-8"], true);
    const horizontalDoor2 = new Door(["2-8", "2-9"], true);
    expect(horizontalDoor1.getTopLeftGridCoords()).toBe('2-9');
    expect(horizontalDoor2.getTopLeftGridCoords()).toBe('2-9');

    const verticalDoor1 = new Door(["5-11", "4-11"], true);
    const verticalDoor2 = new Door(["4-11", "5-11"], true);
    expect(verticalDoor1.getTopLeftGridCoords()).toBe('5-11');
    expect(verticalDoor2.getTopLeftGridCoords()).toBe('5-11');

	}
);

test('Testing top left grid coordinate of furniture is calculated correctly', () => {
    const horizontalFurniture1 = new PieceOfFurniture(["6-5", "8-5"], ["6-5", "6-6"], true);
    const horizontalFurniture2 = new PieceOfFurniture(["8-5", "6-5"], ["6-6", "6-5"], true);
    expect(horizontalFurniture1.getTopLeftGridCoords()).toBe('6-5');
    expect(horizontalFurniture2.getTopLeftGridCoords()).toBe('6-5');

    const verticalFurniture1 = new PieceOfFurniture(["2-4", "3-4"], ["2-4", "2-6"], true);
    const verticalFurniture2 = new PieceOfFurniture(["3-4", "2-4"], ["2-6", "2-4"], true);
    expect(verticalFurniture1.getTopLeftGridCoords()).toBe('2-4');
    expect(verticalFurniture2.getTopLeftGridCoords()).toBe('2-4');

	}
);

test('Testing door orientation is calculated correctly', () => {
        const horizontalDoor1 = new Door(["2-9", "2-8"], true);
        const horizontalDoor2 = new Door(["2-8", "2-9"], true);
        expect(horizontalDoor1.determineOrientation()).toBe('horizontal');
        expect(horizontalDoor2.determineOrientation()).toBe('horizontal');

        const verticalDoor1 = new Door(["2-9", "1-9"], true);
        const verticalDoor2 = new Door(["1-9", "2-9"], true);
        expect(verticalDoor1.determineOrientation()).toBe('vertical');
        expect(verticalDoor2.determineOrientation()).toBe('vertical');
	}
);

test('Testing furniture orientation is calculated correctly', () => {
        const horizontalFurniture1 = new PieceOfFurniture(["6-5", "8-5"], ["6-5", "6-6"], true);
        const horizontalFurniture2 = new PieceOfFurniture(["8-5", "6-5"], ["6-6", "6-5"], true);
        expect(horizontalFurniture1.determineOrientation()).toBe('horizontal');
        expect(horizontalFurniture2.determineOrientation()).toBe('horizontal');

        const verticalFurniture1 = new PieceOfFurniture(["2-4", "3-4"], ["2-4", "2-6"], true);
        const verticalFurniture2 = new PieceOfFurniture(["3-4", "2-4"], ["2-6", "2-4"], true);
        expect(verticalFurniture1.determineOrientation()).toBe('vertical');
        expect(verticalFurniture2.determineOrientation()).toBe('vertical');
	}
);

test('Testing furniture coordinate occupancy is calculated correctly', () => {
        const horizontalFurniture = new PieceOfFurniture(["6-5", "8-5"], ["6-5", "6-6"], true);
        const verticalFurniture = new PieceOfFurniture(["2-4", "3-4"], ["2-4", "2-6"], true);
        const singleSquareFurniture = new PieceOfFurniture(["2-4"], ["2-4"], true);
        expect(horizontalFurniture.determineOccupiedGridCoordinates()).toEqual(new Set(["6-5", "7-5", "8-5", "6-6", "7-6", "8-6"]));
        expect(verticalFurniture.determineOccupiedGridCoordinates()).toEqual(new Set(["2-4", "3-4", "2-5", "3-5", "2-6", "3-6"]));
        expect(singleSquareFurniture.determineOccupiedGridCoordinates()).toEqual(new Set(["2-4"]));
	}
);

test('Testing coordinates are properly blocked by furniture', () => {
		const gameState = _generateGameState();

		// Table-like vertical piece of furniture in the room
	    const verticalFurniture = new PieceOfFurniture(["1-1", "2-1"], ["1-1", "1-3"], gameState, true);
	    verticalFurniture.blockGridCoordinates();

	    // Testing table coords
        expect(gameState.mapGraph.nodes.get("1-1").getAdjacents()).toEqual(new Set([]));	    
        expect(gameState.mapGraph.nodes.get("2-1").getAdjacents()).toEqual(new Set([]));
        expect(gameState.mapGraph.nodes.get("1-2").getAdjacents()).toEqual(new Set([]));
        expect(gameState.mapGraph.nodes.get("1-3").getAdjacents()).toEqual(new Set([]));
        expect(gameState.mapGraph.nodes.get("2-2").getAdjacents()).toEqual(new Set([]));
        expect(gameState.mapGraph.nodes.get("2-3").getAdjacents()).toEqual(new Set([]));

        // Testing adjacent coords
        expect(gameState.mapGraph.nodes.get("1-4").getAdjacents()).toEqual(new Set(["2-4"]));
        expect(gameState.mapGraph.nodes.get("2-4").getAdjacents()).toEqual(new Set(["1-4", "3-4"]));
        expect(gameState.mapGraph.nodes.get("3-1").getAdjacents()).toEqual(new Set(["3-2"]));
        expect(gameState.mapGraph.nodes.get("3-2").getAdjacents()).toEqual(new Set(["3-1", "3-3"]));
        expect(gameState.mapGraph.nodes.get("3-3").getAdjacents()).toEqual(new Set(["3-2", "3-4"]));
        expect(gameState.mapGraph.nodes.get("3-4").getAdjacents()).toEqual(new Set(["2-4", "3-3"]));

	}
);

test('Testing Trap.removeFromPlay', () => {
    const mapGraph = _generateTestMapGraph();
    const gameState = {"mapGraph": mapGraph};

    const trap1 = new Trap("1-3", gameState);
    const trap2 = new Trap("2-1", gameState);

    gameState.trapsInPlay = [trap1, trap2];
    trap1.removeFromPlay();
    expect(gameState.trapsInPlay).toEqual([trap2]);
});

test('Testing Trap.disable', () => {
    const mapGraph = _generateTestMapGraph();
    const gameState = {"mapGraph": mapGraph};

    const trap1 = new Trap("1-3", gameState);

    gameState.trapsInPlay = [trap1];

    const mockRemoveFromCanvas = jest.spyOn(trap1, 'removeFromCanvas').mockImplementation(() => '');
    const mockRemoveFromPlay = jest.spyOn(trap1, 'removeFromPlay');

    trap1.disable();
    expect(mockRemoveFromCanvas).toHaveBeenCalledTimes(1);
    expect(mockRemoveFromPlay).toHaveBeenCalledTimes(1);
    expect(gameState.trapsInPlay).toEqual([]);
});

test('Testing SpearTrap.activate', () => {
    const mapGraph = _generateTestMapGraph();
    const gameState = {"mapGraph": mapGraph};

    const character1 = new Barbarian("1-1", gameState, true);
    expect(character1.bodyPoints).toBe(8);
    expect(character1.hasMovedThisTurn).toBe(false);

    const spearTrap = new SpearTrap("1-3", gameState, false);
    gameState.charactersInPlay = [character1];
    gameState.trapsInPlay = [spearTrap];
    const mockedProcessDeath = jest.spyOn(character1, 'processDeath').mockImplementation(() => '');
    const mockReveal = jest.spyOn(spearTrap, 'reveal').mockImplementation(() => '');

    // Lucky roll
    jest.spyOn(character1, 'rollAttackDice').mockImplementation(() => [6]);
    spearTrap.activate(character1);
    expect(character1.bodyPoints).toBe(8);
    expect(character1.hasMovedThisTurn).toBe(true);
    expect(mockReveal).toHaveBeenCalledTimes(1);

    // Bad roll
    jest.spyOn(character1, 'rollAttackDice').mockImplementation(() => [1]);
    spearTrap.activate(character1);
    expect(character1.bodyPoints).toBe(7);
    expect(character1.hasMovedThisTurn).toBe(true);
    expect(mockReveal).toHaveBeenCalledTimes(2);

    // Worst roll
    jest.spyOn(character1, 'rollAttackDice').mockImplementation(() => [1]);
    character1.bodyPoints = 1;
    spearTrap.activate(character1);
    expect(character1.bodyPoints).toBe(0);
    expect(character1.hasMovedThisTurn).toBe(true);
    expect(mockReveal).toHaveBeenCalledTimes(3);
    expect(mockedProcessDeath).toHaveBeenCalledTimes(1);
});

test('Testing SpearTrap.reveal', () => {
    const mapGraph = _generateTestMapGraph();
    const gameState = {"mapGraph": mapGraph};

    const trap1 = new SpearTrap("1-3", gameState);
    const mockRemoveFromPlay = jest.spyOn(trap1, 'removeFromPlay').mockImplementation(() => '');

    gameState.trapsInPlay = [trap1];
    trap1.reveal();
    expect(mockRemoveFromPlay).toHaveBeenCalledTimes(1);
});

test('Testing PitTrap.activate', () => {
    const mapGraph = _generateTestMapGraph();
    const gameState = {"mapGraph": mapGraph};

    const character1 = new Barbarian("1-1", gameState, true);
    expect(character1.bodyPoints).toBe(8);
    expect(character1.hasMovedThisTurn).toBe(false);
    expect(character1.isInAPit).toBe(false);

    const pitTrap = new PitTrap("1-3", gameState, false);
    gameState.charactersInPlay = [character1];
    gameState.trapsInPlay = [pitTrap];
    const mockedDrawMovement = jest.spyOn(character1, 'drawMovement').mockImplementation(() => '');
    const mockedProcessDeath = jest.spyOn(character1, 'processDeath').mockImplementation(() => '');
    const mockReveal = jest.spyOn(pitTrap, 'reveal').mockImplementation(() => '');

    pitTrap.activate(character1);
    expect(character1.bodyPoints).toBe(7);
    expect(character1.hasMovedThisTurn).toBe(true);
    expect(character1.isInAPit).toBe(true);
    expect(mockedDrawMovement).toHaveBeenCalledTimes(1);
    expect(mockReveal).toHaveBeenCalledTimes(1);

    // really bad luck
    character1.bodyPoints = 1;
    pitTrap.activate(character1);
    expect(character1.bodyPoints).toBe(0);
    expect(mockedProcessDeath).toHaveBeenCalledTimes(1);
});

test('Testing PitTrap.bypass', () => {
    const mapGraph = _generateTestMapGraph();
    const gameState = {"mapGraph": mapGraph};

    const character1 = new Barbarian("1-1", gameState, true);
    const pitTrap = new PitTrap("1-3", gameState, false);
    gameState.charactersInPlay = [character1];
    gameState.trapsInPlay = [pitTrap];
    const mockActivate = jest.spyOn(pitTrap, 'activate').mockImplementation(() => '');

    // Lucky roll
    jest.spyOn(character1, 'rollAttackDice').mockImplementation(() => [6]);
    pitTrap.bypass(character1);
    expect(mockActivate).toHaveBeenCalledTimes(0);

    // Bad roll
    jest.spyOn(character1, 'rollAttackDice').mockImplementation(() => [1]);
    pitTrap.bypass(character1);
    expect(mockActivate).toHaveBeenCalledTimes(1);
    expect(mockActivate).toHaveBeenCalledWith(character1);

    // Other character in the pit
    const character2 = new Barbarian("1-3", gameState, true);
    gameState.charactersInPlay.push(character2);
    pitTrap.bypass(character1);
    expect(mockActivate).toHaveBeenCalledTimes(1);
});

test('Testing FallingBoulderTrap.activate - happy roll', () => {
    const mapGraph = _generateTestMapGraph();
    const gameState = {"mapGraph": mapGraph, "blocksInPlay": [], "monstersInPlay": []};

    const character1 = new Barbarian("0-1", gameState, true);
    const character2 = new Barbarian("0-0", gameState, true);
    expect(character2.bodyPoints).toBe(8);

    const boulderTrap = new FallingBoulderTrap("1-0", gameState, "0-0", false);
    gameState.charactersInPlay = [character1, character2];
    gameState.trapsInPlay = [boulderTrap];
    const mockedDisable = jest.spyOn(boulderTrap, 'disable').mockImplementation(() => '');
    const mockedGenerateNewBlock = jest.spyOn(boulderTrap, 'generateNewBlock').mockImplementation(() => '');

    const mockedProcessDeath = jest.spyOn(character2, 'processDeath').mockImplementation(() => '');
    const mockedDrawMovement = jest.spyOn(character2, 'drawMovement').mockImplementation(() => '');

    // Trap activated, character2 has really bad luck with dice rolls but survives
    jest.spyOn(character2, 'rollAttackDice').mockImplementation(() => [1, 1, 1]);
    boulderTrap.activate(character1);
    expect(character2.bodyPoints).toBe(5);
    expect(character2.coordinates).toBe("1-0");
    expect(mockedProcessDeath).toHaveBeenCalledTimes(0);
    expect(mockedDrawMovement).toHaveBeenCalledTimes(1);
    expect(mockedDisable).toHaveBeenCalledTimes(1);
    expect(mockedGenerateNewBlock).toHaveBeenCalledTimes(1);
});

test('Testing FallingBoulderTrap.activate - unhappy roll', () => {
    const mapGraph = _generateTestMapGraph();
    const gameState = {"mapGraph": mapGraph, "blocksInPlay": [], "monstersInPlay": []};

    const character1 = new Barbarian("0-1", gameState, true);
    const character2 = new Barbarian("0-0", gameState, true);
    character2.bodyPoints = 3;

    const boulderTrap = new FallingBoulderTrap("1-0", gameState, "0-0", false);
    gameState.charactersInPlay = [character1, character2];
    gameState.trapsInPlay = [boulderTrap];
    const mockedDisable = jest.spyOn(boulderTrap, 'disable').mockImplementation(() => '');
    const mockedGenerateNewBlock = jest.spyOn(boulderTrap, 'generateNewBlock').mockImplementation(() => '');
    const mockedProcessDeath = jest.spyOn(character2, 'processDeath').mockImplementation(() => '');
    const mockedDrawMovement = jest.spyOn(character2, 'drawMovement').mockImplementation(() => '');

    // Trap activated, character2 has really bad luck with dice rolls and dies
    jest.spyOn(character2, 'rollAttackDice').mockImplementation(() => [1, 1, 1]);
    boulderTrap.activate(character1);
    expect(character2.bodyPoints).toBe(0);
    expect(character2.coordinates).toBe("0-0");
    expect(mockedProcessDeath).toHaveBeenCalledTimes(1);
    expect(mockedDrawMovement).toHaveBeenCalledTimes(0);
    expect(mockedDisable).toHaveBeenCalledTimes(1);
    expect(mockedGenerateNewBlock).toHaveBeenCalledTimes(1);
    expect(mockedGenerateNewBlock).toHaveBeenCalledWith("0-0");
});

test('Testing FallingBoulderTrap.activate - unhappy positioning', () => {
    const mapGraph = _generateTestMapGraph();
    const gameState = {"mapGraph": mapGraph, "blocksInPlay": [], "monstersInPlay": []};

    const character1 = new Barbarian("0-1", gameState, true);
    const character2 = new Barbarian("0-0", gameState, true);
    const character3 = new Barbarian("1-0", gameState, true);
    expect(character2.bodyPoints).toBe(8);

    const boulderTrap = new FallingBoulderTrap("1-0", gameState, "0-0", false);
    gameState.charactersInPlay = [character1, character2, character3];
    gameState.trapsInPlay = [boulderTrap];
    const mockedDisable = jest.spyOn(boulderTrap, 'disable').mockImplementation(() => '');
    const mockedGenerateNewBlock = jest.spyOn(boulderTrap, 'generateNewBlock').mockImplementation(() => '');
    const mockedProcessDeath = jest.spyOn(character2, 'processDeath').mockImplementation(() => '');
    const mockedDrawMovement = jest.spyOn(character2, 'drawMovement').mockImplementation(() => '');

    // Trap activated, character2 has no adjacent space to move to and dies
    jest.spyOn(character2, 'rollAttackDice').mockImplementation(() => [1, 1, 1]);
    boulderTrap.activate(character1);
    expect(character2.bodyPoints).toBe(5);
    expect(character2.coordinates).toBe("0-0");
    expect(mockedProcessDeath).toHaveBeenCalledTimes(1);
    expect(mockedDrawMovement).toHaveBeenCalledTimes(0);
    expect(mockedDisable).toHaveBeenCalledTimes(1);
    expect(mockedGenerateNewBlock).toHaveBeenCalledTimes(1);
});

test('Testing FallingBoulderTrap.bypass', () => {
    const mapGraph = _generateTestMapGraph();
    const gameState = {"mapGraph": mapGraph, "blocksInPlay": [], "monstersInPlay": []};

    const character1 = new Barbarian("0-1", gameState, true);
    const boulderTrap = new FallingBoulderTrap("1-0", gameState, "0-0", false);
    gameState.charactersInPlay = [character1];
    gameState.trapsInPlay = [boulderTrap];
    const mockedActivate = jest.spyOn(boulderTrap, 'activate').mockImplementation(() => '');

    boulderTrap.bypass(character1);
    expect(mockedActivate).toHaveBeenCalledTimes(1);
    expect(mockedActivate).toHaveBeenCalledWith(character1);
});
