import mapData from "./mapdata";

class Node {
    constructor(coordinates, rooms) {
        this.coordinates = coordinates;
        this.adjacentCoordinates = new Set(); // adjacency list
        this.rooms = rooms;
        this.corridor = mapData[coordinates].hasOwnProperty("corridor");
        this.occupied = false;
    }

    addAdjacent(coordinates) {
        this.adjacentCoordinates.add(coordinates);
    }

    removeAdjacent(coordinates) {
        this.adjacentCoordinates.delete(coordinates);
    }

    getAdjacents() {
        return this.adjacentCoordinates;
    }

    isAdjacent(coordinates) {
        return this.adjacentCoordinates.has(coordinates);
    }

    isUnreachable() {
        return this.adjacentCoordinates.size === 0;
    }

    getXCoordinate() {
        return this.coordinates.split("-")[0];
    }

    getYCoordinate() {
        return this.coordinates.split("-")[1];
    }

    isInCorridor() {
        return this.rooms.length === 0;
    }

    isInSameRoom(otherNode) {
        const startNodeRoom = JSON.stringify(this.rooms);
        const otherNodeRoom = JSON.stringify(otherNode.rooms);
        return startNodeRoom === otherNodeRoom && this.rooms.length !== 0;
    }

    calculateNodeDistance(otherNode) {
        return [
            Math.abs(this.getXCoordinate() - otherNode.getXCoordinate()),
            Math.abs(this.getYCoordinate() - otherNode.getYCoordinate()),
        ];
    }

}

class Graph {
        constructor(sourceData) {
        this.nodes = new Map();
        this.sourceData = sourceData;
    }

    addEdge(sourceCoords, destinationCoords) {
        const sourceNode = this.addVertex(sourceCoords);
        const destinationNode = this.addVertex(destinationCoords);

        sourceNode.addAdjacent(destinationNode.coordinates);

        // Undirected graph
        destinationNode.addAdjacent(sourceNode.coordinates);

        return [sourceNode, destinationNode];
    }

    addVertex(coordinates) {
        if(this.nodes.has(coordinates)) {
            return this.nodes.get(coordinates);
        } else {
            const vertex = new Node(coordinates, this.sourceData[coordinates].rooms);
            this.nodes.set(coordinates, vertex);
            return vertex;
        }
    }

    removeVertex(coordinates) {
        const current = this.nodes.get(coordinates);
        if (current) {
            for (const node of this.nodes.values()) {
                  node.removeAdjacent(current.coordinates);
            }
        }
        return this.nodes.delete(coordinates);
    }

    removeEdge(source, destination) {
        const sourceNode = this.nodes.get(source);
        const destinationNode = this.nodes.get(destination);

        if(sourceNode && destinationNode) {
            sourceNode.removeAdjacent(destinationNode.coordinates);

            // Undirected graph
            destinationNode.removeAdjacent(sourceNode.coordinates);

        }

        return [sourceNode, destinationNode];
    }

    getNeighbours(nodeCoordinates) {
        const neighbourNodes = [];
        const node = this.nodes.get(nodeCoordinates);
        const iteratorAdjacents = node.getAdjacents().values();
        for (let i = 0; i < node.getAdjacents().size; i++) {
            let neighbourNode = this.nodes.get(iteratorAdjacents.next().value);
            neighbourNodes.push(neighbourNode);
        }
        return neighbourNodes;
    }

}

export { Node, Graph };
