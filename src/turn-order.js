function startCharactersTurn(state) {
    state.activeCharacter = state.charactersInPlay[0];
    state.activeCharacter.logAction("starts his turn", state.activeCharacter.name);
    state.activeCharacter.isInAPit = false;
    state.activeCharacter.rollForMovement(state.activeCharacter.getMovesDice());
    state.activeCharacter.updateActiveCharacterDisplay();
    return;
}

function cycleCharactersTurn(state) {
    // Activate first character or deactivate current char
    const activeCharacter = getActiveCharacter(state);
    // Making sure character is marked as inactive
    activeCharacter.hasActedThisTurn = true;
    activeCharacter.hasMovedThisTurn = true;

    // Terminating spell effects
    activeCharacter.passThroughRocks = false;

    // If the scenario is over, exit here
    if (state.currentScenarioIsOver) {
        // TODO add logic for what happens next
        return;
    }

    // Activate next char
    const eligibleCharacters = state.charactersInPlay.filter(
        character => !(character.hasActedThisTurn)
    );
    for (let i = 0; i < eligibleCharacters.length; i++) {
        const nextChar = eligibleCharacters[i];
        if (nextChar.losesTurn === true) {
            nextChar.logAction("skips a turn", nextChar.name);
            nextChar.hasActedThisTurn = true;
            nextChar.hasMovedThisTurn = true;
            nextChar.losesTurn = false;
            continue;
        }
        nextChar.attemptToAwake();
        if (nextChar.isAsleep) {
            nextChar.logAction("is still asleep and skips a turn", nextChar.name);
            nextChar.hasActedThisTurn = true;
            nextChar.hasMovedThisTurn = true;
            continue;
        }
        // If the new character ended their turn in a pit trap, make sure they'll
        // climb out of it
        nextChar.isInAPit = false;
        nextChar.rollForMovement(nextChar.getMovesDice());
        state.activeCharacter = nextChar;
        nextChar.logAction("starts his turn", nextChar.name);
        nextChar.updateActiveCharacterDisplay();
        return;
    }

    // No eligible char to activate, exit function
    state.activeCharacter = null;
    runMonstersTurn(state);
}

function runMonstersTurn(state) {
    const activatedMonsters = state.monstersInPlay.filter(monster => monster.visible === true);
    // TODO: Monster logic
    for (let i = 0; i < activatedMonsters.length; i++) {
        const monster = activatedMonsters[i];
        monster.runMonsterTurn();
        // If the scenario is over due to the actions of the monsters, exit here
        if (state.currentScenarioIsOver) {
            // TODO add logic for what happens next
            return;
        }
    }

    // Reactivate all characters 
    state.charactersInPlay.map((character) => {
        character.hasActedThisTurn = false;
        character.hasMovedThisTurn = false;
    });
    startCharactersTurn(state);
}

function getActiveCharacter(state) {
    return state.activeCharacter;
}


// Alternative
// Heroes have actions that cause turns being cycled
// When no hero can be cycled anymore, run all the monster turns
// At the end of the monster turns, call again the function that cycles characters 
// How to make this testable?
// Heroes technically can only win during their turn, so you only break at the end
// of a character turn
// Characters can lose if they're all killed (during heroes' turn by walking on a trap
// or during monsters' turn) or if an objective fails (technically only case
// is if Sir Ragnar gets killed - this can happen at the end of Sir Ragnar's turn if he walks on
// a trap or during the monsters' turn)
// Edge case: unsolved scenario (characters all leave or die without the scenario being
// lost or won)


export {
	cycleCharactersTurn,
	getActiveCharacter,
	runMonstersTurn,
    startCharactersTurn,
};