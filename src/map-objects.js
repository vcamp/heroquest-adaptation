import VerticalDoorImage from './assets/Door_EU_vertical.svg';
import HorizontalDoorImage from './assets/Door_EU.svg';
import HorizontalSecretDoorImage from './assets/SecretDoor_EU_vertical.svg';
import VerticalSecretDoorImage from './assets/SecretDoor_EU.svg';


import HorizontalTableImage from './assets/Table_EU.svg';
import VerticalTableImage from './assets/Table_EU_vertical.svg';
import HorizontalCupboardImage from './assets/Cupboard_EU.svg';
import VerticalCupboardImage from './assets/Cupboard_EU_vertical.svg';
import HorizontalRackImage from './assets/Rack_EU.svg';
import VerticalRackImage from './assets/Rack_EU_vertical.svg';
import HorizontaTombImage from './assets/Tomb_EU.svg';
import VerticalTombImage from './assets/Tomb_EU_vertical.svg';
import HorizontaBookcaseImage from './assets/Bookcase_EU.svg';
import VerticalBookcaseImage from './assets/Bookcase_EU_vertical.svg';
import HorizontalWeaponsRackImage from './assets/WeaponsRack_EU.svg';
import VerticalWeaponsRackImage from './assets/WeaponsRack_EU_vertical.svg';
import HorizontalAlchemistsBenchImage from './assets/AlchemistsBench_EU.svg';
import VerticalAlchemistsBenchImage from './assets/AlchemistsBench_EU_vertical.svg';
import HorizontalSorcerersTableImage from './assets/SorcerersTable_EU.svg';
import VerticalSorcerersTableImage from './assets/SorcerersTable_EU_vertical.svg';
import HorizontalFireplaceImage from './assets/Fireplace_EU.svg';
import VerticalFireplaceImage from './assets/Fireplace_EU_vertical.svg';
import StairsImage from './assets/Stairsway_EU.svg';
import TreasureChestImage from './assets/TreasureChest_EU.svg';
import ThroneImage from './assets/Throne_EU.svg';

import TwoSquareBlockImage from './assets/DoubleBlockedSquare_EU.svg';
import VerticalTwoSquareBlockImage from './assets/DoubleBlockedSquare_EU_vertical.svg';
import OneSquareBlockImage from './assets/SingleBlockedSquare_EU.svg';

import SpearTrapImage from './assets/SpearTrap_EU.svg';
import FallingBoulderTrapImage from './assets/FallingRock_EU.svg';
import PitTrapImage from './assets/PitTrap_EU.svg';


import {
    cellWidth
} from './map-constants';

import {
    characterOpensDoor,
    revealCorridor,
    revealRoom
} from './game-logic';

import {
    getIntegerCoords,
    getTopLeftCanvasSquareCoords,
    logAction
} from './utils';

import 'fabric';

class MapObject {
    constructor(gameState, visible = false) {
        this.visible = visible;
        this.gameState = gameState;
        this.offsetVals = [7, 5];
    }

    getTopLeftGridCoords() {
        return;
    }

    getTopLeftCanvasSquareCoords(coordinates) {
        const integerCoords = getIntegerCoords(coordinates);
        const squareCoords = getTopLeftCanvasSquareCoords(integerCoords[0], integerCoords[1]);
        return squareCoords;
    }

    getOffsetCanvasCoords() {
        const orientation = this.determineOrientation();
        if (orientation === 'horizontal') {
            this.offsetVals = this.offsetVals.reverse();
        }
        const squareCoords = this.getTopLeftCanvasSquareCoords(this.getTopLeftGridCoords());
        const x = squareCoords[0] + this.offsetVals[0];
        const y = squareCoords[1] + this.offsetVals[1];
        return [x, y];
    }

    determineOrientation() {
        return;
    }

    determineOccupiedGridCoordinates() {
        return [];
    }

    chooseImageSrc() {
        const orientation = this.determineOrientation();
        var src;
        if (orientation === 'horizontal') {
            src = this.horizontalImage;
        } else {
            src = this.verticalImage;
        }
        return src;
    }

    reveal() {
        this.visible = true;
        this.draw();
    }

    draw() {
        return;
    }

    removeFromCanvas() {
        const context = this.gameState.fabricContext;
        context.remove(this.image);
    }

    logAction(verb, actor='', target = '') {
        logAction(verb, this.gameState, actor, target);
    }
}

class Door extends MapObject {
    constructor(coordinates, gameState, visible, open = false) {
        // Expects a single array with two coordinates for the two sides of the door
        super(gameState, visible);
        this.coordinates = coordinates;
        this.open = open;
        this.horizontalImage = HorizontalDoorImage;
        this.verticalImage = VerticalDoorImage;
        this.offsetVals = [-10, 0];
    }

    getTopLeftGridCoords() {
        var coords = this.coordinates;
        var orientation = this.determineOrientation();
        function _compareCoords(a, b) {
            var coordIndex;
            if (orientation === 'horizontal') {
                coordIndex = 1;
            } else {
                coordIndex = 0;
            }
            return getIntegerCoords(b)[coordIndex] - getIntegerCoords(a)[coordIndex];
        }
        coords.sort(_compareCoords);
        return coords[0];
    }

    determineOrientation() {
        const [xA, yA] = getIntegerCoords(this.coordinates[0]);
        const [xB, yB] = getIntegerCoords(this.coordinates[1]);

        if (xA === xB) {
            return 'horizontal';
        }

        return 'vertical';
    }

    determineOccupiedGridCoordinates() {
        return this.coordinates;
    }

    overlooksCorridor() {
        const mapGraph = this.gameState.mapGraph;
        for (let i = 0; i < this.coordinates.length; i++) {
            const coord = this.coordinates[i]; 
            if (mapGraph.nodes.get(coord).rooms.length === 0) {
                return true;
            }
        }
        return false;
    }

    overlooksRoom(room) {
        const mapGraph = this.gameState.mapGraph;
        for (let i = 0; i < this.coordinates.length; i++) {
            const coord = this.coordinates[i]; 
            if (mapGraph.nodes.get(coord).rooms.length === 0) {
                const roomNo = mapGraph.nodes.get(coord).rooms[0];
                if (roomNo === room) {
                    return true;
                }
            }
        }
        return false;
    }

    getCurrentRooms() {
        const rooms = [];
        const coord1 = this.coordinates[0];
        const coord2 = this.coordinates[1];
        const mapGraph = this.gameState.mapGraph;
        if (mapGraph.nodes.get(coord1).rooms.length > 0) {
            rooms.push(mapGraph.nodes.get(coord1).rooms[0]);
        }
        if (mapGraph.nodes.get(coord2).rooms.length > 0) {
            rooms.push(mapGraph.nodes.get(coord2).rooms[0]);
        }
        return rooms;
    }

    openDoor() {
        const coordinates = this.coordinates;
        this.gameState.mapGraph.addEdge(coordinates[0], coordinates[1]);
        this.open = true;
    }

    draw() {
        const context = this.gameState.fabricContext;
        const [x, y] = this.getOffsetCanvasCoords();
        const imageSrc = this.chooseImageSrc();
        const orientation = this.determineOrientation();
        const doorObj = this;
        const gameState = this.gameState;
        fabric.Image.fromURL(
            imageSrc, function(oImg) {
                context.add(oImg);
                oImg.mapObject = doorObj;
                if (orientation === 'horizontal') {
                    oImg.scaleToWidth(cellWidth);
                } else if (orientation === 'vertical') {
                    oImg.scaleToHeight(cellWidth);
                }
                oImg.on('mousedown', function(e){
                    const activeCharacter = gameState.activeCharacter;
                    if (doorObj.open) {
                        return;
                    }
                    characterOpensDoor(doorObj, gameState.activeCharacter, gameState);
                });
            }, 
            {'left': x, 'top': y, 'selectable': false, 'hoverCursor': 'pointer'}
        );
    }
}

class SecretDoor extends Door {
    constructor(coordinates, gameState, visible = false, open = false) {
        // Expects a single array with two coordinates for the two sides of the door
        super(coordinates, gameState, visible, open);
        this.horizontalImage = HorizontalSecretDoorImage;
        this.verticalImage = VerticalSecretDoorImage;
        this.offsetVals = [0, 0];
    }

    getTopLeftGridCoords() {
        // Less sophisticated method to accommodate the variability in placement
        // in the scenario maps - chooses always the first set of coordinates given
        // in the constructor
        return this.coordinates[0];
    }

    openDoor() {
        super.openDoor();
        const gameState = this.gameState;
        this.coordinates.forEach(
            coord => {
                const node = gameState.mapGraph.nodes.get(coord);
                if (node.rooms.length > 0) {
                    revealRoom(node.rooms[0], gameState);
                }
                revealCorridor(node, gameState);
            }
        );
    }

    reveal() {
        this.visible = true;
        this.openDoor();
        this.draw();
    }

    draw() {
        const context = this.gameState.fabricContext;
        const [x, y] = this.getOffsetCanvasCoords();
        const imageSrc = this.chooseImageSrc();
        const doorObj = this;
        const orientation = this.determineOrientation();
        var paddingAdjust = 0;
        if (orientation === 'vertical') {
            paddingAdjust += 14;
        }
        fabric.Image.fromURL(
            imageSrc, function(oImg) {
                context.add(oImg);
                if (orientation === 'horizontal') {
                    oImg.scaleToWidth(cellWidth);
                } else if (orientation === 'vertical') {
                    oImg.scaleToHeight(cellWidth);
                }
                // Secret door opens on reveal
                doorObj.openDoor();
            },
            {'left': x + paddingAdjust, 'top': y, 'selectable': false}
        );
    }
}

class Trap extends MapObject {
    constructor(coordinates, gameState, visible = false) {
        super(gameState, visible);
        this.coordinates = coordinates;
        this.offsetVals = [5, 0];
    }

    getTopLeftGridCoords() {
        return this.coordinates;
    }


    activate() {
        return;
    }

    disable() {
        this.removeFromPlay();
        this.removeFromCanvas();
    }

    removeFromPlay() {
        const gameState = this.gameState;
        for(let i = 0; i < gameState.trapsInPlay.length; i++){ 
            if (gameState.trapsInPlay[i].coordinates === this.coordinates) {
                gameState.trapsInPlay.splice(i, 1); 
            }
        }
    }

    getCurrentRoom() {
        const mapGraph = this.gameState.mapGraph;
        return mapGraph.nodes.get(this.coordinates).rooms[0];        
    }

    draw() {
        const context = this.gameState.fabricContext;
        const [startX, startY] = this.getOffsetCanvasCoords();
        const imageSrc = this.imageSrc;
        const orientation = this.determineOrientation();
        const gameState = this.gameState;
        const trapObj = this;

        fabric.Image.fromURL(
            imageSrc, function(oImg) {
                context.add(oImg);
                oImg.scaleToWidth(cellWidth);
                trapObj.image = oImg;
                oImg.on('mousedown', function(e){
                    gameState.activeCharacter.disarmTrap(trapObj);
                });
                context.sendToBack(oImg);
            }, 
            {
                'left': startX,
                'top': startY,
                'selectable': false,
                'hoverCursor': 'pointer'
            }
        );
    }
}

class SpearTrap extends Trap {
    constructor(coordinates, gameState, visible = false) {
        super(coordinates, gameState, visible);
        this.imageSrc = SpearTrapImage;
    }

    activate(character) {
        this.logAction("activated a Spear Trap!", character.name);
        character.hasMovedThisTurn = true;

        const trapDiceResults = character.rollAttackDice(1).filter(
            result => character.getAttackDiceValidValues().includes(result)
        ).reduce((partialSum, a) => partialSum + 1, 0);
        if (trapDiceResults > 0) {
            this.logAction(`receives ${trapDiceResults} point of damage from the Spear Trap`, character.name);
        } else {
            this.logAction(`manages to dodge the Spear Trap`, character.name);
        }
        character.bodyPoints -= trapDiceResults;
        if (character.bodyPoints <= 0) {
            character.processDeath();
        }

        // Trap is revealed (and consequently deactivated)
        this.reveal();
    }

    reveal() {
        // When revealed this trap is rendered harmless - remove from game
        this.logAction("A Spear Trap is now sprung and deactivated");
        this.removeFromPlay();
    }
}

class PitTrap extends Trap {
    constructor(coordinates, gameState, visible = false) {
        super(coordinates, gameState, visible);
        this.imageSrc = PitTrapImage;
    }

    activate(character) {
        this.logAction("fell into a Pit Trap, losing 1 body point. They cannot move again this turn", character.name);
        character.bodyPoints -= 1;
        this.reveal();
        character.drawMovement();
        if (character.bodyPoints <= 0) {
            character.processDeath();
        }
        character.hasMovedThisTurn = true;
        character.isInAPit = true;
    }

    bypass(character) {
        const trapCoords = this.coordinates;
        const charactersInThePit = this.gameState.charactersInPlay.filter(
            character => character.coordinates === trapCoords
        );
        if (charactersInThePit.length > 0) {
            // There's already someone in the pit, bypass is automatic
            this.logAction("manages to jump over the Pit Trap", character.name);
            return;
        }

        const trapDiceResults = character.rollAttackDice(1).filter(
            result => character.getAttackDiceValidValues().includes(result)
        ).reduce((partialSum, a) => partialSum + 1, 0);
        if (trapDiceResults > 0) {
            // Character falls in the pit
            this.activate(character);
        }
        this.logAction("manages to jump over the Pit Trap", character.name);
    }
}

class FallingBoulderTrap extends Trap {
    constructor(coordinates, gameState, fallCoordinates, visible = false) {
        super(coordinates, gameState, visible);
        this.fallCoordinates = fallCoordinates;
        this.imageSrc = FallingBoulderTrapImage;
        this.offsetVals = [0, 0];
    }

    activate(character) {
        this.logAction("activated a Falling Boulder Trap! It falls in the passage, blocking it and crushing everything on its path", character.name);
        const fallCoords = this.fallCoordinates;
        const adjacentCoords = this.gameState.mapGraph.nodes.get(fallCoords).getAdjacents();
        const potentialTargets = this.gameState.charactersInPlay.concat(this.gameState.monstersInPlay);
        potentialTargets.forEach(
            (target) => {
                if (target.coordinates === fallCoords) {
                    const trapDiceResults = target.rollAttackDice(3).filter(
                        result => target.getAttackDiceValidValues().includes(result)
                    ).reduce((partialSum, a) => partialSum + 1, 0);
                    if (trapDiceResults > 0) {
                        var plural = '';
                        if (trapDiceResults > 1) {
                            plural = 's';
                        }
                        this.logAction(`receives ${trapDiceResults} point${plural} of damage from the Falling Boulder Trap`, target.name);
                    } else {
                        this.logAction(`manages to dodge the Falling Boulder Trap`, target.name);
                    }
                    target.bodyPoints -= trapDiceResults;
                    if (target.bodyPoints <= 0) {
                        target.processDeath();
                        return;
                    }
                    // Character moves to a random adjacent square
                    const impassableCoords = target.getBlockedLastSquareCoordinates();
                    var charHasMoved = false;
                    adjacentCoords.forEach(
                        (coords) => {
                            if (!(impassableCoords.includes(coords)) && charHasMoved === false) {
                                target.updateCoordinates(coords);
                                target.drawMovement();
                                charHasMoved = true;
                            }
                        }
                    );
                    if (charHasMoved === true) {
                        return;
                    }
                    this.logAction("was crushed to death by the Falling Boulder Trap!", target.name);
                    // No adjacent square available - character dies!
                    target.processDeath();
                }
            } 
        );

        // Add a block where the boulder trap fell
        this.generateNewBlock(fallCoords);

        // Deactivate the sprung trap
        this.disable();
    }

    generateNewBlock(fallCoords) {
        const block = new OneSquareBlock([fallCoords], [fallCoords], this.gameState);
        block.reveal();
        block.blockGridCoordinates();
        this.gameState.blocksInPlay.push(block);        
    }


    bypass(character) {
        // Can't bypass - trap is sprung
        this.activate(character);
    }
}

class PieceOfFurniture extends MapObject {
    constructor(coordinatesX, coordinatesY, gameState, visible) {
        super(gameState, visible);
        // Expects two arrays of 2 coordinates, given from left topmost square, order doesn't matter
        this.coordinatesX = coordinatesX;
        this.coordinatesY = coordinatesY;
        this.sizeAdjustment = cellWidth * 0.8;
        this.name = "Piece of Furniture";
    }

    getCurrentRoom() {
        const mapGraph = this.gameState.mapGraph;
        return mapGraph.nodes.get(this.coordinatesX[0]).rooms[0];
    }

    getTopLeftGridCoords() {
        var coords = this.coordinatesX;
        var orientation = this.determineOrientation();
        function _compareCoords(a, b) {
            return getIntegerCoords(a)[0] - getIntegerCoords(b)[0];
        }
        coords.sort(_compareCoords);
        return coords[0];
    }

    getCoordinatesXLength() {
        if (this.coordinatesX.length === 1) {
            return 0;
        }
        return Math.abs(
            getIntegerCoords(this.coordinatesX[1])[0] - getIntegerCoords(this.coordinatesX[0])[0]
        );
    }

    getCanvasXLength() {
        const lengthCoordinatesX = this.getCoordinatesXLength();
        return (lengthCoordinatesX * cellWidth) + this.sizeAdjustment;
    }

    getCoordinatesYLength() {
        if (this.coordinatesY.length === 1) {
            return 0;
        }
        return Math.abs(
            getIntegerCoords(this.coordinatesY[1])[1] - getIntegerCoords(this.coordinatesY[0])[1]
        );
    }

    getCanvasYLength() {
        const lengthCoordinatesY = this.getCoordinatesYLength();
        return (lengthCoordinatesY * cellWidth) + this.sizeAdjustment;
    }

    determineOrientation() {
        if (this.getCanvasXLength() > this.getCanvasYLength()) {
            return 'horizontal';
        } else if (this.getCanvasXLength() < this.getCanvasYLength()) {
            return 'vertical';
        }
        return 'single-square';
    }

    determineOccupiedGridCoordinates() {
        if (this.coordinatesX.length === 1 && this.coordinatesY.length === 1) {
            return new Set(this.coordinatesX);
        }
        const topLeftGridCoordsInteger = getIntegerCoords(this.getTopLeftGridCoords());
        const coordinatesXLength = this.getCoordinatesXLength();
        const coordinatesYLength = this.getCoordinatesYLength();
        const occupiedGridCoordinates = new Set();
        for (var i = 0; i <= coordinatesXLength; i++) {
            for (var j = 0; j <= coordinatesYLength; j++) {
                const newCoords = [topLeftGridCoordsInteger[0] + i, topLeftGridCoordsInteger[1] + j];
                occupiedGridCoordinates.add(newCoords[0].toString() + '-' + newCoords[1].toString());
            }
        }
        return occupiedGridCoordinates;
    }

    blockGridCoordinates(mapGraph=this.gameState.mapGraph) {
        // Removes graph edges due to the presence of this piece of furniture
        const occupiedGridCoordinates = Array.from(this.determineOccupiedGridCoordinates());
        for (let i = 0; i < occupiedGridCoordinates.length; i++) {
            const startCoordinates = occupiedGridCoordinates[i];
            const adjacents = Array.from(
                mapGraph.nodes.get(startCoordinates).getAdjacents()
            );
            for (let j = 0; j < adjacents.length; j++) {
                const endCoordinates = adjacents[j];
                mapGraph.removeEdge(startCoordinates, endCoordinates);
            }
        }
    }

    draw() {
        const context = this.gameState.fabricContext;
        const [startX, startY] = this.getOffsetCanvasCoords();
        const xLength = this.getCanvasXLength();
        const yLength = this.getCanvasYLength();
        const imageSrc = this.chooseImageSrc();
        const orientation = this.determineOrientation();
        const furnitureObj = this;
        fabric.Image.fromURL(
            imageSrc, function(oImg) {
                context.add(oImg);
                if (orientation === 'horizontal' || orientation === 'single-square') {
                    oImg.scaleToWidth(xLength);
                } else if (orientation === 'vertical') {
                    oImg.scaleToHeight(yLength);
                }
                oImg.on('mousedown', function(e){
                    furnitureObj.logAction(`Examining a ${furnitureObj.name}`);
                });
            }, 
            {
                'left': startX,
                'top': startY,
                'selectable': false,
                'hoverCursor': 'pointer'
            }
        );
    }
}

class Table extends PieceOfFurniture {
    constructor(coordinatesX, coordinatesY, gameState, visible) {
        super(coordinatesX, coordinatesY, gameState, visible);
        this.horizontalImage = HorizontalTableImage;
        this.verticalImage = VerticalTableImage;
    }
}

class Cupboard extends PieceOfFurniture {
    constructor(coordinatesX, coordinatesY, gameState, visible) {
        super(coordinatesX, coordinatesY, gameState, visible);
        this.horizontalImage = HorizontalCupboardImage;
        this.verticalImage = VerticalCupboardImage;
        this.offsetVals = [2, 5];
    }
}

class TreasureChest extends PieceOfFurniture {
    constructor(coordinatesX, coordinatesY, gameState, visible) {
        super(coordinatesX, coordinatesY, gameState, visible);
        this.horizontalImage = TreasureChestImage;
        this.verticalImage = TreasureChestImage;
        this.offsetVals = [3, 5];
        this.sizeAdjustment = cellWidth * 0.9;
    }
}

class Throne extends PieceOfFurniture {
    constructor(coordinatesX, coordinatesY, gameState, visible) {
        super(coordinatesX, coordinatesY, gameState, visible);
        this.horizontalImage = ThroneImage;
        this.verticalImage = ThroneImage;
        this.offsetVals = [5, 3];
        this.sizeAdjustment = cellWidth * 0.8;
    }
}

class Bookcase extends PieceOfFurniture {
    constructor(coordinatesX, coordinatesY, gameState, visible) {
        super(coordinatesX, coordinatesY, gameState, visible);
        this.horizontalImage = HorizontaBookcaseImage;
        this.verticalImage = VerticalBookcaseImage;
        this.offsetVals = [2, 5];
    }
}

class WeaponsRack extends PieceOfFurniture {
    constructor(coordinatesX, coordinatesY, gameState, visible) {
        super(coordinatesX, coordinatesY, gameState, visible);
        this.horizontalImage = HorizontalWeaponsRackImage;
        this.verticalImage = VerticalWeaponsRackImage;
        this.offsetVals = [2, 5];
    }
}

class Fireplace extends PieceOfFurniture {
    constructor(coordinatesX, coordinatesY, gameState, visible) {
        super(coordinatesX, coordinatesY, gameState, visible);
        this.horizontalImage = HorizontalFireplaceImage;
        this.verticalImage = VerticalFireplaceImage;
        this.offsetVals = [2, 5];
    }
}

class Rack extends PieceOfFurniture {
    constructor(coordinatesX, coordinatesY, gameState, visible) {
        super(coordinatesX, coordinatesY, gameState, visible);
        this.horizontalImage = HorizontalRackImage;
        this.verticalImage = VerticalRackImage;
        this.offsetVals = [3, 5];
    }
}

class Tomb extends PieceOfFurniture {
    constructor(coordinatesX, coordinatesY, gameState, visible) {
        super(coordinatesX, coordinatesY, gameState, visible);
        this.horizontalImage = HorizontaTombImage;
        this.verticalImage = VerticalTombImage;
    }
}

class AlchemistsBench extends PieceOfFurniture {
    constructor(coordinatesX, coordinatesY, gameState, visible) {
        super(coordinatesX, coordinatesY, gameState, visible);
        this.horizontalImage = HorizontalAlchemistsBenchImage;
        this.verticalImage = VerticalAlchemistsBenchImage;
    }
}

class SorcerersTable extends PieceOfFurniture {
    constructor(coordinatesX, coordinatesY, gameState, visible) {
        super(coordinatesX, coordinatesY, gameState, visible);
        this.horizontalImage = HorizontalSorcerersTableImage;
        this.verticalImage = VerticalSorcerersTableImage;
    }
}

class Stairs extends PieceOfFurniture {
    constructor(coordinatesX, coordinatesY, gameState, visible) {
        super(coordinatesX, coordinatesY, gameState, visible);
        this.horizontalImage = StairsImage;
        this.verticalImage = StairsImage;
        this.offsetVals = [0, 0];
        this.sizeAdjustment = cellWidth;
    }
}

class OneSquareBlock extends PieceOfFurniture {
    constructor(coordinatesX, coordinatesY, gameState, visible) {
        super(coordinatesX, coordinatesY, gameState, visible);
        this.horizontalImage = OneSquareBlockImage;
        this.verticalImage = OneSquareBlockImage;
        this.offsetVals = [0, 0];
        this.sizeAdjustment = cellWidth;
    }
}

class TwoSquareBlock extends PieceOfFurniture {
    constructor(coordinatesX, coordinatesY, gameState, visible) {
        super(coordinatesX, coordinatesY, gameState, visible);
        this.horizontalImage = TwoSquareBlockImage;
        this.verticalImage = VerticalTwoSquareBlockImage;
        this.offsetVals = [0, 0];
        this.sizeAdjustment = cellWidth;
    }
}

export {
    AlchemistsBench,
    Bookcase,
    Cupboard, 
    Door,
    FallingBoulderTrap,
    Fireplace,
    OneSquareBlock,
    PieceOfFurniture,
    PitTrap,
    Rack,
    Table,
    Tomb,
    Throne,
    SecretDoor,
    SorcerersTable,
    Stairs,
    Trap,
    TreasureChest,
    TwoSquareBlock,
    SpearTrap,
    WeaponsRack
};