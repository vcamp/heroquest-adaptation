import {
    logAction
} from './utils';


class InventoryItem {
	constructor(name, description, inventoryPropName, removeAtEndScenario=false) {
		this.name = name;
		this.description = description;
		this.inventoryPropName = inventoryPropName;
		this.removeAtEndScenario = removeAtEndScenario;
	}

	addToInventory(character) {
		character[this.inventoryPropName].push(this);
	}

	removeFromInventory(character) {
		const inventoryPropName = this.inventoryPropName;
		for (let i = 0; i < character[inventoryPropName].length; i++) {
			const potion = character[inventoryPropName][i];
			if (potion.name === this.name) {
				character[inventoryPropName].splice(i, 1);
			}
		}
	}
}

class PowerUpPotion extends InventoryItem {
	constructor(name, description, boostedPropName, removeAtEndScenario=true, inventoryPropName='potions') {
		super(name, description, inventoryPropName, removeAtEndScenario);
		this.boostedPropName = boostedPropName;
	}

	drink(character) {
		character[this.boostedPropName] = true;
		this.removeFromInventory(character);
	}
}

class HealingPotion extends InventoryItem {
	constructor(name, description, removeAtEndScenario=true, inventoryPropName='potions') {
		super(name, description, inventoryPropName, removeAtEndScenario);
	}

	drink(character) {
		character.heal(4);
		this.removeFromInventory(character);
	}
}

class HolyWater extends InventoryItem {
	constructor(name, description, removeAtEndScenario=true, inventoryPropName='utilityObjects') {
		super(name, description, inventoryPropName, removeAtEndScenario);
	}
}

class Toolkit extends InventoryItem {
	constructor(description, inventoryPropName='utilityObjects', name='Took Kit') {
		super(name, description, inventoryPropName);
		this.canDisarmTraps = true;
		this.cost = 250;
	}
}

class EquippableItem extends InventoryItem {
	constructor(name, description, inventoryPropName='utilityObjects', equipSlot='', cost=0) {
		super(name, description, inventoryPropName);
		this.equipSlot = equipSlot;
		this.cost = cost;
	}

	equip(character) {
		if (character[this.equipSlot] !== null) {
			character[this.equipSlot].unequip(character);
		}
		character[this.equipSlot] = this;
	}

	unequip(character) {
		this.addToInventory(character);
		character[this.equipSlot] = null;
	}
}

class Weapon extends EquippableItem {
	constructor(
		name, 
		description, 
		attackDice=1, 
		diagonalAttack=false, 
		ranged=false,
		thrown=false,
		twoHanded=false,
		wizardAllowed=false,
		inventoryPropName='weapons',
		equipSlot='equippedWeapon',
		cost=0
	) {
		super(name, description, inventoryPropName, equipSlot, cost);
		this.attackDice = attackDice;
		this.diagonalAttack = diagonalAttack;
		this.ranged = ranged;
		this.thrown = thrown;
		this.twoHanded = twoHanded;
		this.wizardAllowed = wizardAllowed;
	}

	equip(character) {
		if (this.twoHanded === true && character.hasEquippedShield() === true) {
            character.logAction("cannot equip this weapon while they are using a Shield", character.name);
            return;
		}
		if (character.name === 'Wizard' && !(this.wizardAllowed)) {
            character.logAction("Wizards cannot equip this weapon");
            return;			
		}
		super.equip(character);
	}
}

class Armour extends EquippableItem {
	constructor(
		name, 
		description, 
		defenseDice=1, 
		wizardOnly=false,
		limitsMovement=false,
		inventoryPropName='armour',
		equipSlot='equippedArmour',
		cost=0
	) {
		super(name, description, inventoryPropName, equipSlot, cost);
		this.defenseDice = defenseDice;
		this.wizardOnly = wizardOnly;
		this.limitsMovement = limitsMovement;
	}

	equip(character) {
		if (character.name === 'Wizard' && this.wizardOnly === false) {
            character.logAction("Wizards cannot equip this armour");
            return;
		} else if (character.name !== 'Wizard' && this.wizardOnly === true) {
            character.logAction("Only Wizards can equip this armour");
            return;
		}
		super.equip(character);
	}
}

class ProtectiveItem extends EquippableItem {
	constructor(
		name,
		equipSlot,
		description,
		inventoryPropName='protectiveObjects',
		defenseDice=1, 
		wizardOnly=false,
		cost=0
	) {
		super(name, description, inventoryPropName, equipSlot, cost);
		this.defenseDice = defenseDice;
		this.wizardOnly = wizardOnly;
	}

	equip(character) {
		if (character.name === 'Wizard' && this.wizardOnly === false) {
            character.logAction("Wizards cannot equip this item");
            return;
		} else if (character.name !== 'Wizard' && this.wizardOnly === true) {
            character.logAction("Only Wizards can equip this item");
            return;
		}
		super.equip(character);
	}
}

class Shield extends ProtectiveItem {
	constructor(
		name, 
		description, 
		defenseDice=1, 
		wizardOnly=false, 
		inventoryPropName='shields',
		equipSlot='equippedShield',
		cost=0
	) {
		super(name, equipSlot, description, inventoryPropName, defenseDice, wizardOnly, cost);
	}

	equip(character) {
		if (character.hasEquippedWeapon() === true && character.equippedWeapon.twoHanded === true) {
            character.logAction(
            	"cannot equip this shield while they are using a two-handed weapon", 
            	character.name
            );
            return;			
		}
		super.equip(character);
	}
}

export {
	Armour,
	EquippableItem,
	PowerUpPotion,
	HealingPotion,
	HolyWater,
	ProtectiveItem,
	Shield,
	Toolkit,
	Weapon
};