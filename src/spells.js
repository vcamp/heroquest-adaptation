import {
    revealCorridor,
    revealRoom
} from './game-logic';

const spells = [
	{"name": "Heal Body", "requiresVisibility": true, "type": "Earth", "targets": ["charactersInPlay"], "func": healBody},
	{"name": "Pass Through Rock", "requiresVisibility": true, "type": "Earth", "targets": ["charactersInPlay"], "func": passThroughRock},
	{"name": "Rock Skin", "requiresVisibility": true, "type": "Earth", "targets": ["charactersInPlay"], "func": rockSkin},
	{"name": "Ball Of Flame", "requiresVisibility": true, "type": "Fire", "targets": ["charactersInPlay", "monstersInPlay"], "func": ballOfFlame},
	{"name": "Courage", "requiresVisibility": true, "type": "Fire", "targets": ["charactersInPlay"], "func": courage},
	{"name": "Fire of Wrath", "requiresVisibility": false, "type": "Fire", "targets": ["charactersInPlay", "monstersInPlay"], "func": fireOfWrath},
	{"name": "Genie", "requiresVisibility": false, "type": "Air", "targets": ["doorsInPlay", "charactersInPlay", "monstersInPlay"], "func": genie},
	{"name": "Swift Wind", "requiresVisibility": true, "type": "Air", "targets": ["charactersInPlay"], "func": swiftWind},
	{"name": "Tempest", "requiresVisibility": true, "type": "Air", "targets": ["charactersInPlay", "monstersInPlay"], "func": tempest},
	{"name": "Sleep", "requiresVisibility": true, "type": "Water", "targets": ["charactersInPlay", "monstersInPlay"], "func": sleep},
	{"name": "Veil of Mist", "requiresVisibility": true, "type": "Water", "targets": ["charactersInPlay"], "func": veilOfMist},
	{"name": "Water of Healing", "requiresVisibility": true, "type": "Water", "targets": ["charactersInPlay"], "func": waterOfHealing}
];

function healBody(caster, target) {
	target.heal(4);
	target.logAction("recovers 4 body points", target.name);
}

function passThroughRock(caster, target) {
	target.passThroughRock = true;
	return;
}

function rockSkin(caster, target) {
	target.rockSkin = true;
	return;
}

function ballOfFlame(caster, target) {
    const defenseDiceResults = target.rollDefenseDice(
        2
    ).filter(
        result => target.getDefenseDiceValidValues().includes(result)
    ).reduce((partialSum, a) => partialSum + 1, 0);

    target.resolveAttack(caster, 2, defenseDiceResults, 'body');
}

function courage(caster, target) {
	target.courage = true;
}

function fireOfWrath(caster, target) {
    const defenseDiceResults = target.rollDefenseDice(
        1
    ).filter(
        result => target.getDefenseDiceValidValues().includes(result)
    ).reduce((partialSum, a) => partialSum + 1, 0);

    target.resolveAttack(caster, 1, defenseDiceResults, 'body');
}

function genie(caster, target) {
	if (target.type === 'monster' || target.type === 'hero') {
		caster.logAction('The Genie attacks the target');
        const attackDiceResults = caster.rollAttackDice(
            5
        ).filter(
            result => caster.getAttackDiceValidValues().includes(result)
        ).reduce((partialSum, a) => partialSum + 1, 0);
        caster.logAction(`rolled ${attackDiceResults} skulls`, caster.name);

        const defenseDiceResults = target.rollDefenseDice(
            target.getDefenseDice()
        ).filter(
            result => target.getDefenseDiceValidValues().includes(result)
        ).reduce((partialSum, a) => partialSum + 1, 0);
        caster.logAction(`rolled ${defenseDiceResults} shields`, target.name);

        // Resolve attack
        target.resolveAttack(caster, attackDiceResults, defenseDiceResults, 'body');
        return;
	}
	caster.logAction('The Genie opens the door');
	target.openDoor();
	target.coordinates.forEach(
		(coords) => {
			const node = caster.gameState.mapGraph.nodes.get(coords);
		    if (node.rooms.length > 0) {
		        revealRoom(node.rooms[0], caster.gameState);
		        return;
		    }
		    revealCorridor(node, caster.gameState);
		}
	);
}

function swiftWind(caster, target) {
	target.doubleMovement = true;
}

function tempest(caster, target) {
	target.losesTurn = true;
}

function sleep(caster, target) {
    const defenseDiceResults = target.rollDefenseDice(
        target.mindPoints
    ).filter(
        result => target.getDefenseDiceValidValues().includes(result)
    ).reduce((partialSum, a) => partialSum + 1, 0);
    if (defenseDiceResults < 1) {
	    target.logAction("fell into a deep sleep", target.name);
	    target.isAsleep = true;
	    return;
    }
    target.logAction("resisted the sleep spell", target.name);
}

function veilOfMist(caster, target) {
	target.veilOfMist = true;
}

function waterOfHealing(caster, target) {
	target.heal(4);
	target.logAction("recovers 4 body points", target.name);
}



export {
	spells
};