import buckets from "buckets-js";
import mapData from "./mapdata";

import { Graph } from './data-structures';

import {
    getIntegerCoords,
    getTopLeftCanvasSquareCoords,
    setIntersection
} from "./utils.js";

import {
    cycleCharactersTurn,
} from "./turn-order.js";

import {
    getRoomLines,
    drawRooms
} from './map-building';

import {
    cellWidth,
} from "./map-constants.js";

function breadthFirst(startNode, destinationNode, mapGraph) {
    // Stolen from here: https://www.redblobgames.com/pathfinding/a-star/introduction.html
    const frontier = buckets.Queue();
    frontier.enqueue(startNode.coordinates);
    const cameFrom = new Map();
    cameFrom.set(startNode.coordinates, null);

    while (!frontier.isEmpty()) {
        var current = frontier.dequeue();

        // Exit early
        if (current === destinationNode.coordinates) {
            const path = [];
            var backTrack = current;
            while (backTrack != startNode.coordinates) {
                path.push(backTrack);
                backTrack = cameFrom.get(backTrack);
            }
            path.push(startNode.coordinates);
            return path;
        }
        
        const neighbours = mapGraph.getNeighbours(current);
        for (let nextNode of neighbours) {
            if (cameFrom.get(nextNode.coordinates) === undefined) {
                frontier.enqueue(nextNode.coordinates);
                cameFrom.set(nextNode.coordinates, current);
            }
        }
    }

    // No viable path
    return [];
}

function superCoverBresenham(startNode, endNode, mapGraph) {
    // Stolen and adapted from: http://eugen.dedu.free.fr/projects/bresenham/
    var [x, y] = getIntegerCoords(startNode.coordinates);
    var [x2, y2] = getIntegerCoords(endNode.coordinates);
    var nodesArray = [];
    var x1 = x;
    var y1 = y;
    var ystep;
    var xstep;
    var error;
    var errorPrev;
    var ddx;
    var ddy;
    var dx = x2 - x1;
    var dy = y2 - y1;
    nodesArray.push(startNode);
    
    if (dy < 0) {
        ystep = -1;
        dy = -dy;
    } else {
        ystep = 1;
    }
    if (dx < 0) {
        xstep = -1;
        dx = -dx;
    } else {
        xstep = 1;
    }

    // For integer arithmetic
    ddy = dy << 1;
    ddx = dx << 1;

    if (ddx >= ddy) {
        errorPrev = error = dx;

        for (let i = 0; i < dx; i++) {

            x += xstep;
            error += ddy;

            if (error > ddx) {
                y += ystep;
                error -= ddx;
                if (error + errorPrev < ddx) {
                    nodesArray.push(mapGraph.nodes.get(x.toString() + '-' + (y - ystep).toString()));

                } else if (error + errorPrev > ddx) {
                    nodesArray.push(mapGraph.nodes.get((x - xstep).toString() + '-' + y.toString()));
                }
            }

            nodesArray.push(mapGraph.nodes.get(x.toString() + '-' + y.toString()));
            errorPrev = error;
        }

    } else {
        errorPrev = error = dy;
        for (let i = 0; i < dy; i++) {

            y += ystep;
            error += ddx;

            if (error > ddy) {
                x += xstep;
                error -= ddy;

                if (error + errorPrev < ddy) {
                    nodesArray.push(mapGraph.nodes.get((x - xstep).toString() + '-' + y.toString()));
                } else if (error + errorPrev > ddy) {
                    nodesArray.push(mapGraph.nodes.get(x.toString() + '-' + (y - ystep).toString()));
                }
            }

            nodesArray.push(mapGraph.nodes.get(x.toString() + '-' + y.toString()));
            errorPrev = error;

        }
    }

    return nodesArray;
}

function checkTargetIsVisible(startNode, targetNode, mapGraph, blockedCoordinates = [], useForAttack=false) {
    // Returns true if a target object or character is visible. Can be used during a
    // corridor reveal or (when useForAttack === true) for determining line of sight.

    if (useForAttack === true) {
        // Special case - if the nodes are in the same room, the target is
        // always visible
        if (startNode.isInSameRoom(targetNode) === true) {
            return true;
        }
    }

    const nodePath = superCoverBresenham(startNode, targetNode, mapGraph);
    for (let i = 0; i < nodePath.length; i++) {
        // Exit early if we reached the end of nodePath - last square should be visible
        if (i === nodePath.length - 1) {
            break;
        }
        const currentNode = nodePath[i];
        const nextNode = nodePath[i + 1];
        if (i === nodePath.length - 2) {
            // Special case - we want blocks to be visible if they're at
            // the end of the path and we're in a corridor
            if (currentNode.isInCorridor() === true && nextNode.isInCorridor() === true) {
                break;
            }
        }
        var blockedLastSquareCoords = blockedCoordinates;

        // Special case: we don't want to consider impassable squares on the last check
        if (i === nodePath.length - 2) {
            blockedLastSquareCoords = [];
        }
        const blocked = checkNodeVisibilityIsBlocked(currentNode, nextNode, blockedLastSquareCoords);
        if (blocked === true) {
            return false;
        }
    }
    return true;
}

function checkNodeVisibilityIsBlocked(startNode, endNode, blockedCoordinates = []) {
    // The nodes can't be adjacent or share a common
    // adjacent node; endNode can't be in the blockedList
    const nodesAreAdjacent = startNode.adjacentCoordinates.has(endNode.coordinates);
    const nodesShareAdjacentNode = setIntersection(startNode.adjacentCoordinates, endNode.adjacentCoordinates).size > 0;
    const nodeIsInBlockedList = blockedCoordinates.includes(endNode.coordinates);
    return !(nodesAreAdjacent || nodesShareAdjacentNode) || nodeIsInBlockedList;
}

function characterOpensDoor (door, character, gameState) {
    const mapGraph = gameState.mapGraph;
    const characterCoordinates = character.coordinates;
    const doorCoordinates = door.coordinates;

    // Check that the character is on either side of the door
    if (!doorCoordinates.includes(characterCoordinates)) {
        character.logAction("cannot reach the door", character.name);
        return;
    }

    // Check that the door is not already open
    if (door.open === true) {
        character.logAction("The door is already open");
        return;
    }

    // Find coordinates opposite to character
    const oppositeDoorCoords = doorCoordinates.filter(
        coords => coords !== characterCoordinates
    )[0];
    const oppositeSideNode = mapGraph.nodes.get(oppositeDoorCoords);

    // Open the door
    door.openDoor();

    // Reveal the room or corridor on the other side
    if (oppositeSideNode.rooms.length > 0) {
        revealRoom(oppositeSideNode.rooms[0], gameState);
        return;
    }
    revealCorridor(oppositeSideNode, gameState);

}

function revealRoom(room, gameState) {
    const mapGraph = gameState.mapGraph;
    const monsters = gameState.monstersInPlay;
    const furniture = gameState.furnitureInPlay;
    const toCheckForReveal = monsters.concat(furniture);
    for (let i = 0; i < toCheckForReveal.length; i++) {
        const mapObject = toCheckForReveal[i];
        if (mapObject.getCurrentRoom() === room && mapObject.visible === false) {
            mapObject.reveal();
        }
    }
    const doors = gameState.doorsInPlay;
    for (let i = 0; i < doors.length; i++) {
        const door = doors[i];
        const doorCoords = door.coordinates;
        for (let j = 0; j < doorCoords.length; j++) {
            const doorNode = mapGraph.nodes.get(doorCoords[j]);
            if (doorNode.rooms[0] === room && !(door.visible)) {
                door.reveal();
            }
        }
    }
    mapGraph.nodes.forEach(
        (targetNode) => {
            if (targetNode.rooms[0] === room) {
                const integerCoords = getIntegerCoords(targetNode.coordinates);
                const squareCoords = getTopLeftCanvasSquareCoords(integerCoords[0], integerCoords[1]);
                var myCanvas = document.querySelector('#canvas');
                if (myCanvas !== null) {
                    const ctx = myCanvas.getContext("2d");
                    ctx.globalCompositeOperation = 'destination-under';
                    ctx.fillStyle = 'white';
                    ctx.fillRect(squareCoords[0] + 1, squareCoords[1] + 1, cellWidth - 2, cellWidth - 2);
                }
            }
        }
    );
    var myCanvas = document.querySelector('#canvas');
    if (myCanvas !== null) {
        var context = myCanvas.getContext("2d");
        const roomLines = getRoomLines(mapData);
        drawRooms(context, roomLines);
    }
}

function revealCorridor(startNode, gameState) {
    const mapGraph = gameState.mapGraph;
    const monsters = gameState.monstersInPlay.filter(
        monster => (monster.getCurrentRoom() === undefined && !(monster.visible))
    );
    const doors = gameState.doorsInPlay.filter(
        door => (door.overlooksCorridor() === true && !(door.visible))
    );
    const blocks = gameState.blocksInPlay.filter(
        block => !(block.visible)
    );

    for (let i = 0; i < monsters.length; i++) {
        const monster = monsters[i];
        if (checkTargetIsVisible(startNode, monster.getCurrentNode(mapGraph), mapGraph)) {
            monster.reveal();
        }

    }

    for (let i = 0; i < doors.length; i++) {
        const door = doors[i];
        for (let j = 0; j < door.coordinates.length; j++) {
            const coord = door.coordinates[j];
            if (checkTargetIsVisible(startNode, mapGraph.nodes.get(coord), mapGraph)) {
                door.reveal();
                break;
            }
        }
    }

    for (let i = 0; i < blocks.length; i++) {
        const block = blocks[i];
        const occupiedCoords = Array.from(
            block.determineOccupiedGridCoordinates()
        );
        for (let j = 0; j < occupiedCoords.length; j++) {
            const coord = block.coordinatesX[j];
            if (checkTargetIsVisible(startNode, mapGraph.nodes.get(coord), mapGraph)) {
                block.reveal();
                break;
            }
        }
    }

    mapGraph.nodes.forEach(
        (targetNode) => {
            if (checkTargetIsVisible(startNode, targetNode, mapGraph)) {
                const integerCoords = getIntegerCoords(targetNode.coordinates);
                const squareCoords = getTopLeftCanvasSquareCoords(integerCoords[0], integerCoords[1]);
                var myCanvas = document.querySelector('#canvas');
                if (myCanvas !== null) {
                    const ctx = myCanvas.getContext("2d");
                    ctx.globalCompositeOperation = 'destination-under';
                    ctx.fillStyle = 'white';
                    ctx.fillRect(squareCoords[0] + 1, squareCoords[1] + 1, cellWidth - 2, cellWidth - 2);
                }
            }
        }
    );
    var myCanvas = document.querySelector('#canvas');
    if (myCanvas !== null) {
        var context = myCanvas.getContext("2d");
        const roomLines = getRoomLines(mapData);
        drawRooms(context, roomLines);
    }
}

function greedyGraphTraversal(startingNode, mapGraph) {
    const reached = new Set();
    const toVisit = [startingNode];
    while (toVisit.length > 0) {
        const node = toVisit.pop();
        const adjacents = node.getAdjacents();
        adjacents.forEach(
            (adjacent) => {
                const reachedNode = mapGraph.nodes.get(adjacent);
                if (!reached.has(reachedNode)) {
                    toVisit.push(reachedNode);
                    reached.add(reachedNode);
                }
            }
        );
    }
    return Array.from(reached);
}

function generatePassThroughRockMap(gameState, impassableCoords) {
    const tempMapGraph = new Graph(mapData);
    const furniture = gameState.furnitureInPlay;
    const monstersInPlay = gameState.monstersInPlay;
    const unreachableCorridorCoords = gameState.unreachableCorridorCoords;

    for (var i = 0; i < Object.keys(mapData).length; i++) {
        const nodeName = Object.keys(mapData)[i];
        const nodeAdjacents = [];
        const [x, y] = getIntegerCoords(nodeName);

        // TODO filter out corridor tiles that are just not available in the dungeon (use greedy algorithm
        // from initial map)

        if (x < 25) {
            nodeAdjacents.push((x + 1).toString() + "-" + y.toString());
        }
        if (y < 18) {
            nodeAdjacents.push(x.toString() + "-" + (y + 1).toString());
        }
        nodeAdjacents.forEach((adjacent) => {tempMapGraph.addEdge(nodeName, adjacent);});
    }

    // Make furniture impassable on temp map
    furniture.forEach((pieceOfFurniture) => {
        pieceOfFurniture.blockGridCoordinates(tempMapGraph);
    });

    // Exclude non-occupied rooms on temp map
    const roomOccupants = monstersInPlay.concat(furniture);
    const notEmptyRooms = roomOccupants.map((elem) => {return elem.getCurrentRoom();});
    tempMapGraph.nodes.forEach((node) => {
        if (node.rooms.length > 0 && notEmptyRooms.includes(node.rooms[0]) === false) {
            tempMapGraph.removeVertex(node.coordinates);
        }
    });

    // Exclude impassable corridors on temp map
    const corridorNodesToExclude = [];
    for (let i = 0; i < unreachableCorridorCoords.length; i++) {
        const unreachableCoords = unreachableCorridorCoords[i];
        const unreachableNode = gameState.mapGraph.nodes.get(unreachableCoords);
        corridorNodesToExclude.push(... greedyGraphTraversal(unreachableNode, gameState.mapGraph));
    }
    corridorNodesToExclude.forEach(
        (node) => {tempMapGraph.removeVertex(node.coordinates);}
    );

    return tempMapGraph;
}


function findCharacterMovementPath(startNode, destinationNode, impassableCoords, gameState, passThroughRocks=false) {
    // Adapted from here: https://www.redblobgames.com/pathfinding/a-star/introduction.html

    var mapGraphToUse = gameState.mapGraph;

    // Character is using magic to move around - we need to generate a different, temporary map
    // that allows movement across walls, without letting them wander off the scenario's map
    // so that they remain stuck
    // TODO we can probably break this into a different function in the same file
    if (passThroughRocks === true) {
        mapGraphToUse = generatePassThroughRockMap(gameState, impassableCoords);
    }

    // Start of movement logic
    const frontier = buckets.Queue();
    frontier.enqueue(startNode.coordinates);
    const cameFrom = new Map();
    cameFrom.set(startNode.coordinates, null);

    while (!frontier.isEmpty()) {
        var current = frontier.dequeue();

        if (current === destinationNode.coordinates) {
            const path = [];
            var backTrack = current;
            while (backTrack != startNode.coordinates) {
                path.push(backTrack);
                backTrack = cameFrom.get(backTrack);
            }
            path.push(startNode.coordinates);

            // From start to last square
            return path.reverse();
        }
        
        const neighbours = mapGraphToUse.getNeighbours(current).filter(
            node => !(impassableCoords.includes(node.coordinates))
        );

        for (let nextNode of neighbours) {
            if (cameFrom.get(nextNode.coordinates) === undefined) {
                frontier.enqueue(nextNode.coordinates);
                cameFrom.set(nextNode.coordinates, current);
            }
        }
    }

    // No viable path
    return [];
}

function compareMonsterAttackTargets(targetA, targetB) {
    if (targetA.distance < targetB.distance) {
        return -1;
    } else if (targetA.distance > targetB.distance) {
        return 1;
    }

    if (targetA.character.bodyPoints < targetB.character.bodyPoints) {
        return -1;
    } else if (targetA.character.bodyPoints > targetB.character.bodyPoints) {
        return 1;
    }
    return 0;
}

export { 
    breadthFirst,
    characterOpensDoor,
    superCoverBresenham,
    checkTargetIsVisible,
    checkNodeVisibilityIsBlocked,
    findCharacterMovementPath,
    revealCorridor,
    revealRoom,
    compareMonsterAttackTargets
};
