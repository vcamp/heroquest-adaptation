import 'fabric';
import mapData from "./mapdata";

import { 
    sum, 
    breadthFirst,
    superCoverBresenham,
    checkTargetIsVisible,
    checkNodeVisibilityIsBlocked,
    revealCorridor,
    revealRoom,
    moveCharacter
} from './game-logic';

import {
    cycleCharactersTurn,
    getActiveCharacter,
    startCharactersTurn
} from "./turn-order.js";

import {
    gameState,
    initiateScenario,
    setupBlocks,
    setupDoors,
    setupFurniture,
    setupCharacters,
    setupMonsters,
    setupTraps
} from './state-management';

import {
    getRoomLines,
    drawBoard,
    drawRooms
} from './map-building';

import { cellWidth, padding } from './map-constants';

// Setting up map
var myCanvas = document.querySelector('#canvas');
var myDynamicCanvas = document.querySelector('#canvasDynamic');
var context = myCanvas.getContext("2d");
var contextDynamic = myDynamicCanvas.getContext("2d");
const canvasFabric = new fabric.Canvas('canvasDynamic', {allowTouchScrolling: true});
const roomLines = getRoomLines(mapData);

function drawMap(context, roomLines) {
    drawBoard(context);
    drawRooms(context, roomLines);
}

function setUpEverything() {
    setupBlocks();
    setupDoors();
    setupFurniture(); 
    setupCharacters();
    setupMonsters();
    setupTraps();
    revealRoom(gameState.charactersInPlay[0].getCurrentRoom(), gameState);
}

initiateScenario(canvasFabric);
drawMap(context, roomLines);
setUpEverything();
startCharactersTurn(gameState);
// Current goal
// * Implement spells
//  Current sub-goals
// * Fix bug with turn order (have to reproduce)
// * Finish implementing all spells
// * Test all spells
//  * Will need movement across map (simply count the number of squares and handle
//  edge cases where you veer off the map -> calculate by leaving one coordinate for each room
//  or corridor and do a path search)
// Future
// * Implement character choice and replacement
// * Implement inventory management and inventory that can be bought
// * Implement leaving the dungeon

// * Implement starting placement of characters
// * Implement victory conditions
// * Implement scenario transition (incl. removing items)
// * Implement buying equipment

// * Implement non-searchability on 1st scenario
// * Implement special events on movement
// * Implement artifacts
// * Implement scenario craziness (villains that cast spells, characters losing equipment, illusory
// gold, etc.)

// * Make sure that character that is using a double attack effect cannot attack + do another action
// (finish implementing hasAttackedThisTurn)
// * Allow movement on mobile
// * Quest text
// * Movement with confirmation
// * Attack with confirmation
// * Implement attack animations (?)
// * More refined AI with monsters that get closer if path is blocked
// * Improve wandering monster so that it continues
// searching if adjacent coordinates are blocked
// * Make sure that monsters can trigger revealed traps
// * CI on Gitlab (?)

// Notes
// * Relax visibility restriction for corridor corners?

// PoC testing - on mouse click

function cycleTurnOnButtonPress() {
    cycleCharactersTurn(gameState);
}

const endTurnButton = document.getElementById("endTurn");
endTurnButton.addEventListener("click", cycleTurnOnButtonPress, false);

function searchTreasure() {
    const char = getActiveCharacter(gameState);
    char.searchForTreasure();
}

function searchTrapsAndSecDoors() {
    const char = getActiveCharacter(gameState);
    char.searchForSecretDoorsAndTraps();
}

const searchTreasureButton = document.getElementById("searchTreasure");
searchTreasureButton.addEventListener("click", searchTreasure, false);

const searchTrapsAndDoorsButton = document.getElementById("searchTrapsAndSecDoors");
searchTrapsAndDoorsButton.addEventListener("click", searchTrapsAndSecDoors, false);

// Spellcasting listener
canvasFabric.on('mouse:down', function(event) {
    // If active char is not casting a spell don't do anything
    const activeChar = getActiveCharacter(gameState);
    if (activeChar.isReadyToCastSpell() === false) {
        return;
    }
    // If they are, check if target is eligible; if so, cast the spell
    // TODO break to separate testable function
    if (event.target !== null) {
        var coordinates;
        // This awfulness is to handle doors
        if (event.target.mapObject.open) {
            return;
        }
        if (event.target.mapObject.coordinates.length === 2) {
            coordinates = event.target.mapObject.coordinates[0];
        } else {
            coordinates = event.target.mapObject.coordinates;
        }
        const isTargetableBySpell = activeChar.spellTargetList.includes(event.target.mapObject);
        const isVisibleBySpellcaster = (
            activeChar.readySpell.requiresVisibility === false || 
            activeChar.coordinatesAreInAttackRange(coordinates)
        );
        if (isTargetableBySpell === true && isVisibleBySpellcaster === true) {
            activeChar.castSpell(event.target.mapObject, activeChar.readySpell);
            return;
        } else {
            activeChar.logAction('cannot cast the spell on that target', activeChar.name);
        }
    }
}, false);

// Movement listener
canvasFabric.on('mouse:down', function(event) {
    if (event.target !== null) {
        // We don't want to move unless we're clicking on an empty square;
        return;
    }
    const activeChar = getActiveCharacter(gameState);
    // If active char is casting a spell don't do anything
    if (activeChar.isReadyToCastSpell() === true) {
        return;
    }
    var rect = myCanvas.getBoundingClientRect();
    var x = Math.floor((event.e.clientX - rect.left - padding) / cellWidth);
    var y = Math.floor((event.e.clientY - rect.top - padding) / cellWidth);
    var startNode = gameState.mapGraph.nodes.get(activeChar.coordinates);
    var destNode = gameState.mapGraph.nodes.get(x.toString() + "-" + y.toString());
    if (destNode === undefined) {
        // someone clicked outside the map
        return;
    }
    activeChar.move(destNode, gameState);
}, false);
