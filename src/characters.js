import fabric from '../node_modules/fabric/dist/fabric.js';

import BarbarianImage from './assets/Barbarian_EU.svg';
import DwarfImage from './assets/Dwarf_EU.svg';
import WizardImage from './assets/Wizard_EU.svg';
import ElfImage from './assets/Elf_EU.svg';

import FimirImage from './assets/Fimir_EU.svg';
import OrcImage from './assets/Orc_EU.svg';
import MummyImage from './assets/Mummy_EU.svg';
import ZombieImage from './assets/Zombie_EU.svg';
import GoblinImage from './assets/Goblin_EU.svg';
import GargoyleImage from './assets/Gargoyle_EU.svg';
import SkeletonImage from './assets/Skeleton_EU.svg';
import ChaosWarriorImage from './assets/ChaosWarrior_EU.svg';

import {
    cellWidth
} from './map-constants';

import {
    getIntegerCoords,
    getTopLeftCanvasSquareCoords,
    logAction
} from './utils';

import {
    cycleCharactersTurn,
    getActiveCharacter,
    startCharactersTurn
} from "./turn-order.js";

import {
    characterOpensDoor,
    findCharacterMovementPath,
    checkTargetIsVisible,
    compareMonsterAttackTargets,
    revealCorridor,
    revealRoom
} from './game-logic';

import {
    spells
} from './spells';


class Character {
    constructor(
        coordinates,
        imageSrc,
        gameState,
        name = 'Someone',
        bodyPoints = 1,
        mindPoints = 1,
        attackDice = 1,
        defenseDice = 1,
        visible = false,
        moves = 0
    ) {
        this.coordinates = coordinates;
        this.imageSrc = imageSrc;
        this.gameState = gameState;
        this.visible = visible;
        this.moves = moves;
        this.name = name;
        this.attackDice = attackDice;
        this.defenseDice = defenseDice;
        this.bodyPoints = bodyPoints;
        this.mindPoints = mindPoints;
        this.startingBodyPoints = bodyPoints;
        this.startingMindPoints = mindPoints;
        this.type = 'monster';
        this.offset = [5, 5];
        this.image = null;
        this.hasAttackedThisTurn = false;
        this.hasActedThisTurn = false;
        this.hasMovedThisTurn = false;
        this.losesTurn = false;
        this.isInAPit = false;
        this.isAsleep = false;
        this.veilOfMist = false;
    }

    getTopLeftCanvasSquareCoords() {
        const integerCoords = getIntegerCoords(this.coordinates);
        const squareCoords = getTopLeftCanvasSquareCoords(integerCoords[0], integerCoords[1]);
        return squareCoords;
    }

    getCurrentNode() {
    	const mapGraph = this.gameState.mapGraph;
        return mapGraph.nodes.get(this.coordinates);
    }

    getCurrentRoom() {
        const currentNode = this.getCurrentNode(this.gameState.mapGraph);
        if (currentNode.rooms.length > 0) {
            return currentNode.rooms[0];
        }
        return undefined;
    }

    updateCoordinates(newCoordinates) {
    	this.coordinates = newCoordinates;
    }

    drawMovement() {
        this.removeFromCanvas();
        this.draw();
    }

    getWidthModifier() {
    	return 0.8;
    }

    getImpassableCoordinates() {
        if (this.veilOfMist) {
            this.veilOfMist = false;
            return [];
        }
    	if (this.type === 'hero') {
    		return this.gameState.monstersInPlay.map(monster => monster.coordinates);
    	}

    	return this.gameState.charactersInPlay.map(hero => hero.coordinates); 
    }

    getBlockedLastSquareCoordinates() {
    	const heroes = this.gameState.charactersInPlay.map(hero => hero.coordinates);
    	const monsters = this.gameState.monstersInPlay.map(monster => monster.coordinates);
    	return heroes.concat(monsters);
    }

    getAttackDice() {
        return this.attackDice;
    }

    getDefenseDice() {
        return this.defenseDice;
    }

    getMovesDice() {
        return this.movesDice;
    }

    hasEquippedWeapon() {
        return false;
    }

    heal(healedPoints) {
        var currentBodyPoints = this.bodyPoints;
        const withHealingEffect = currentBodyPoints += healedPoints;
        const bodyPtsChoices = [withHealingEffect, this.startingBodyPoints];
        this.bodyPoints = Math.min(...bodyPtsChoices);
    }

    rollAttackDice(attackDice) {
        const attackResults = [];
        for (let i = 0; i < attackDice; i++) {
            const roll = Math.floor(Math.random() * 6) + 1;
            attackResults.push(roll);
        }
        return attackResults;
    }

    rollDefenseDice(defenseDice) {
        const defenseResults = [];
        for (let i = 0; i < defenseDice; i++) {
            const roll = Math.floor(Math.random() * 6) + 1;
            defenseResults.push(roll);
        }
        return defenseResults;
    }

    getAttackDiceValidValues() {
        return [1, 2, 3];
    }

    getDefenseDiceValidValues() {
        if (this.type === 'monster') {
            return [6];
        }
        return [4, 5];
    }

    checkHeroTurnNeedsToCycle() {
        if (this.type === 'hero' && this.hasActedThisTurn === true && this.hasMovedThisTurn === true) {
            cycleCharactersTurn(this.gameState);
        }
    }

    coordinatesAreInAttackRange(coordinates) {
        const charNode = this.gameState.mapGraph.nodes.get(this.coordinates);
        return charNode.isAdjacent(coordinates);
    }

    move(destNode) {
        const gameState = this.gameState;
        const character = this;
        const destCoords = destNode.coordinates;
        const blockedLastSquareCoords = character.getBlockedLastSquareCoordinates();

        // Character is in a pit and can't move - exit early
        if (character.isInAPit === true) {
            this.logAction("is in a pit trap and can't move", this.name);
            return;
        }

        // Trying to complete movement on an occupied square - exit early
        if (blockedLastSquareCoords.includes(destCoords)) {
            return;
        }

        const impassableCoords = character.getImpassableCoordinates();
        const startNode = character.getCurrentNode();
        const movesLeft = character.moves;

        var pathFound = findCharacterMovementPath(
            startNode,
            destNode,
            impassableCoords,
            gameState,
            character.passThroughRock
        );
        // pathFound also returns the first node
        const mapTraps = new Map();
        gameState.trapsInPlay.forEach(
            (trap) => {
                mapTraps.set(trap.coordinates, trap);
            }
        );

        if (pathFound.length > 0 && (pathFound.length - 1) <= movesLeft) {
            for (let i = 0; i < pathFound.length; i++) {
                const newNode = gameState.mapGraph.nodes.get(pathFound[i]);
                if (newNode.rooms.length === 0) {
                    revealCorridor(newNode, gameState);
                } else {
                    revealRoom(newNode.rooms[0], gameState);
                }
                character.updateCoordinates(pathFound[i]);
                if (i < pathFound.length - 1) {
                    character.moves = character.moves - 1;
                }
                if (mapTraps.has(newNode.coordinates) === true) {
                    const trap = mapTraps.get(newNode.coordinates);
                    if (trap.visible === false) {
                        trap.activate(character);
                        break;
                    } else {
                        // Characters shouldn't be able to end their
                        // movement on a trap so we assume they can always
                        // bypass
                        trap.bypass(character);
                    }
                }
            }

            character.drawMovement();
            character.hasMovedThisTurn = true;
            character.updateActiveCharacterDisplay();

            // If character moved away from monsters in sight, remove Courage effect
            this.checkCourageEffect();

            // Slightly different case than checkHeroTurnNeedsToCycle - we want to
            // allow player to move up to their full range before cycling turns
            if (character.hasActedThisTurn === true && character.moves === 0) {
                cycleCharactersTurn(gameState);
            }
            return;
        }

        this.logAction("cannot reach that location", this.name);
        return;
    }

    checkCourageEffect() {
        if (this.courage === true && !this.checkEnemiesAreInSight()) {
            this.courage = false;
        }
    }

    attackEnemy(defender, attackType = 'body') {
        // Check that the character is in the correct position to attack - adjacent node
        if (!(this.coordinatesAreInAttackRange(defender.coordinates))) {
            this.logAction("is not in range for attacking", this.name, defender.name);
            return;
        }
        if (this.type === 'hero') {
            if (this.doubleAttack === true && this.hasAttackedThisTurn === true) {
                this.logAction("is able to attack a second time", this.name);
                this.doubleAttack = false;
            } else if (this.hasActedThisTurn === true) {
                this.logAction("has already acted the maximum number of times this turn", this.name);                
                return;
            }
        }

        this.logAction("attacks", this.name, defender.name);
        const attacker = this;

        // Roll attack and defense dice, sum all relevant results
        const attackDiceResults = attacker.rollAttackDice(
            attacker.getAttackDice()
        ).filter(
            result => attacker.getAttackDiceValidValues().includes(result)
        ).reduce((partialSum, a) => partialSum + 1, 0);
        this.logAction(`rolled ${attackDiceResults} skulls`, this.name);

        var defenseDice = defender.getDefenseDice();

        if (defender.isAsleep) {
            defenseDice = 0;
            // Attack causes defender to awake
            defender.isAsleep = false;
        }

        const defenseDiceResults = defender.rollDefenseDice(
            defenseDice
        ).filter(
            result => defender.getDefenseDiceValidValues().includes(result)
        ).reduce((partialSum, a) => partialSum + 1, 0);
        this.logAction(`rolled ${defenseDiceResults} shields`, defender.name);

        // Attacked character awakes
        this.isAsleep = false;

        // If we made it this far, we can check if the attack has been done with
        // a thrown weapon, and in case discard it
        if (this.hasEquippedWeapon() === true && this.equippedWeapon.thrown === true) {
            const attackerNode = this.gameState.mapGraph.nodes.get(this.coordinates);
            if (attackerNode.isAdjacent(defender.coordinates) === false) {
                this.logAction("attacked by throwing their weapon, which is now lost", this.name);
                this.equippedWeapon = null;
            }
        }

        // Resolve attack
        defender.resolveAttack(attacker, attackDiceResults, defenseDiceResults, attackType);
        this.updateAttackerState();
    }

    checkEnemyVisibility() {
        return;
    }

    updateAttackerState() {
        this.checkCourageEffect();
        
        if (this.doubleAttack === true) {
            this.hasAttackedThisTurn = true;
            return;
        }
        this.hasActedThisTurn = true;
        this.checkHeroTurnNeedsToCycle();
    }

    resolveAttack(attacker, attackDiceResult, defenseDiceResult, attackType) {
        // Attacker passed in arguments for handling cases where defender is immune
        // to specific attacks, f.e. Witch Lord

        const gameState = this.gameState;
        const pointsLoss = attackDiceResult - defenseDiceResult;

        // Attack had no effect - exit early
        if (pointsLoss <= 0) {
            this.logAction("parried the attack by", this.name, attacker.name);
            return;
        }

        // Determine defense stat to apply based on attack type
        const defender = this;
        var defenseStat;
        var defenseStatName;
        if (attackType === 'body') {
            defenseStat = defender.bodyPoints;
            defenseStatName = 'bodyPoints';
        } else {
            defenseStat = defender.mindPoints;
            defenseStatName = 'mindPoints';
        }

        // Update defender stat
        defender[defenseStatName] -= pointsLoss;

        // If attackDiceResult - defenseDiceResult > defender.bodyPoints, defender is dead
        if (pointsLoss >= defenseStat) {
            this.logAction("was killed by", this.name, attacker.name);

            // Remove dead char from play
            defender.processDeath();
            return;
        }

        this.logAction(`lost ${pointsLoss} hit points due to an attack by`, this.name, attacker.name);

        if (defender.rockSkin === true) {
            // Break active spells due to successful attack
            defender.rockSkin = false;
            this.logAction('The effects of the Rock Skin spell wear off');
        }
        return;
    }

    reveal() {
        this.visible = true;
        this.draw();
    }

    draw() {
        const state = this.gameState;
        const context = this.gameState.fabricContext;
        const [startX, startY] = this.getTopLeftCanvasSquareCoords();
        const imageSrc = this.imageSrc;
        const widthModifier = this.getWidthModifier();
        const character = this;
        const img = fabric.fabric.Image.fromURL(
            imageSrc, function(oImg) {
                context.add(oImg);
                oImg.scaleToWidth(cellWidth * widthModifier);
                character.image = oImg;
                oImg.mapObject = character;
                oImg.on('mousedown', function(data){
                    if (character.type !== 'monster') {
                        return;
                    }
                    const activeCharacter = state.activeCharacter;
                    if (activeCharacter === null) {
                    this.logAction("Characters cannot attack out of their turn");
                    }
                    if (activeCharacter.isReadyToCastSpell()) {
                        return;
                    }
                    // For edge cases where a character has been killed by a spell
                    // and we're still getting a click event even after a turn has
                    // cycled
                    if (character.bodyPoints <= 0) {
                        return;
                    }
                    activeCharacter.attackEnemy(character);
                    return;
                });
            }, 
            {
                'left': startX + this.offset[0],
                'top': startY + this.offset[1],
                'selectable': false,
                'hoverCursor': 'pointer'
            }
        );
    }

    checkEnemiesAreInSight() {
        var activatedEnemies = this.gameState.monstersInPlay.filter(monster => monster.visible === true);
        if (this.type === 'monster') {
            activatedEnemies = this.gameState.charactersInPlay;
        }
        for (let i = 0; i < activatedEnemies.length; i++) {
            const enemy = activatedEnemies[i];
            if (this.checkTargetIsInSight(enemy, false) === true) {
                return true;
            }
        }
        return false;
    }

    attemptToAwake() {
        const roll = Math.floor(Math.random() * 6) + 1;
        if (roll === 6) {
            this.isAsleep = false;
        }
    }

    removeFromCanvas() {
        const context = this.gameState.fabricContext;
        context.remove(this.image);
    }

    logAction(verb, actor='', target = '') {
        logAction(verb, this.gameState, actor, target);
    }
}

class Hero extends Character {
    constructor(
        coordinates, 
        image, 
        gameState,
        name,
        bodyPoints = 1,
        mindPoints = 1,
        attackDice = 1,
        defenseDice = 1,
        movesDice = 2,
        visible = true, 
        moves = 0
    ) {
    	super(
            coordinates,
            image,
            gameState,
            name,
            bodyPoints,
            mindPoints,
            attackDice,
            defenseDice,
            visible,
            moves
        );
        this.movesDice = movesDice;
    	this.type = 'hero';

        this.gold = 0;
        this.equippedWeapon = null;
        this.equippedShield = null;
        this.equippedArmour = null;
        this.equippedCloak = null;
        this.equippedBracers = null;
        this.equippedHelmet = null;
        this.potions = [];
        this.utilityObjects = [];
        this.weapons = [];
        this.armour = [];
        this.shields = [];
        this.protectiveObjects = [];

        this.doubleAttack = false;
        this.potionOfResilience = false;
        this.doubleMovement = false;
        this.potionOfStrength = false;

        this.spells = [];
        this.readySpell = null;
        this.spellTargetList = [];
        this.passThroughRock = false;
        this.rockSkin = false;
        this.courage = false;

        this.scenariosCompleted = [];
        this.outlawed = false;
    }


    updateActiveCharacterDisplay() {
        const nameElement = document.getElementById("activeCharName");
        if (nameElement !== null) {
            nameElement.textContent = this.name;        
        }
        const bodyElement = document.getElementById("bodyPointsActive");
        if (bodyElement !== null) {
            bodyElement.textContent = this.bodyPoints;
        }
        const mindElement = document.getElementById("mindPointsActive");
        if (mindElement !== null) {
            mindElement.textContent = this.mindPoints;
        }
        const moveElement = document.getElementById("movementActive");
        if (moveElement !== null) {
            moveElement.textContent = this.moves;
        }
        const attackElement = document.getElementById("attackDiceActive");
        if (attackElement !== null) {
            attackElement.textContent = this.getAttackDice();
        }
        const defenseElement = document.getElementById("defenseDiceActive");
        if (defenseElement !== null) {
            defenseElement.textContent = this.getDefenseDice();
        }
        const goldElement = document.getElementById("goldActive");
        if (goldElement !== null) {
            goldElement.textContent = this.gold;
        }
        const spellsElement = document.getElementById("spellsActive");
        if (spellsElement !== null) {
            const spells = this.spells;
            var listContent = "";
            for (let i = 0; i < spells.length; i++) {
                const spellObj = spells[i];
                const spellName = spellObj.name;
                const spellType = spellObj.type;
                const buttonCast = `<button class="spellCast" id="${spellName}">Cast ${spellName} (${spellType})</button>`;
                const buttonCancel = '<button id="spellCancel">Cancel</button>';
                listContent += `<li>${buttonCast}${buttonCancel}</li>`;
            }
            var list = `<ul>${listContent}</ul>`;
            spellsElement.innerHTML = list;
            const character = this;
            for (let i = 0; i < spells.length; i++) {
                const spellObj = spells[i];
                const spellName = spellObj.name;
                const castSpellElem = document.getElementById(spellName);
                castSpellElem.addEventListener("click", () => character.startSpellcasting(spellObj), false);
            }
            const cancelButtons = document.querySelectorAll('#spellCancel');
            for (let i = 0; i < cancelButtons.length; i++) {
                const cancelButton = cancelButtons[i];
                cancelButton.addEventListener("click", () => character.cancelSpellCasting());
            }
        }
    }

    isReadyToCastSpell() {
        return this.readySpell !== null;
    }

    startSpellcasting(spellObj) {
        if (this.hasActedThisTurn === true) {
            this.logAction("has already acted the maximum number of times this turn", this.name);                
            return;
        }
        this.readySpell = spellObj;
        const spellName = spellObj.name;

        if (spellObj.targets.length > 0) {
            // Spell has targets
            this.logAction('must select a target for this spell', this.name);
            const otherButtons = document.querySelectorAll(`.spellCast[id]:not([id="${spellName}"])`);
            otherButtons.forEach((button) => button.disabled = true);

            // Select appropriate targets and add them to eligible list
            for (let i = 0; i < spellObj.targets.length; i++) {
                this.spellTargetList.push(...this.gameState[spellObj.targets[i]]);
            }

        } else {
            // Easy case - spell has no targets
            this.castSpell(null, spellObj);
        }
    }

    cancelSpellCasting() {
        // Remove casting flag and active spell
        this.resetSpellCastingProps();

        // Make all spell buttons selectable again
        const spellButtons = document.querySelectorAll('.spellCast');
        spellButtons.forEach((button) => button.disabled = false);
    }

    resetSpellCastingProps() {
        this.readySpell = null;

        // Clear target list
        this.spellTargetList = [];
    }

    castSpell(target, spellObj) {
        this.logAction('casts', this.name, spellObj.name);

        // resolve effects of spell
        spellObj.func(this, target);

        // remove spell from character's inventory
        const spellIndex = this.spells.indexOf(spellObj);
        this.spells.splice(spellIndex, 1);

        this.resetSpellCastingProps();

        // character has acted this turn
        this.hasActedThisTurn = true;

        // refresh char spell list
        this.updateActiveCharacterDisplay();

        // see if turn needs to cycle
        this.checkHeroTurnNeedsToCycle();
    }

    hasEquippedWeapon() {
        return this.equippedWeapon !== null;
    }

    hasEquippedArmour() {
        return this.equippedArmour !== null;
    }

    hasEquippedShield() {
        return this.equippedShield !== null;
    }

    hasEquippedHelmet() {
        return this.equippedHelmet !== null;
    }

    hasEquippedBracers() {
        return this.equippedBracers !== null;
    }

    hasEquippedCloak() {
        return this.equippedCloak !== null;
    }

    getAttackDice() {
        var dice = this.attackDice;
        if (this.hasEquippedWeapon() === true) {
            dice = this.equippedWeapon.attackDice;
        }
        if (this.potionOfStrength === true) {
            dice += 2;
            this.potionOfStrength = false;
        }
        if (this.courage === true) {
            dice += 2;
        }
        return dice;
    }

    getDefenseDice() {
        var dice = this.defenseDice;
        if (this.hasEquippedArmour() === true) {
            dice = this.equippedArmour.defenseDice;
        }
        if (this.hasEquippedShield() === true) {
            dice += this.equippedShield.defenseDice;
        }
        if (this.hasEquippedBracers() === true) {
            dice += this.equippedBracers.defenseDice;
        }
        if (this.hasEquippedCloak() === true) {
            dice += this.equippedCloak.defenseDice;
        }
        if (this.hasEquippedHelmet() === true) {
            dice += this.equippedHelmet.defenseDice;
        }
        if (this.potionOfResilience === true) {
            dice += 2;
            this.potionOfResilience = false;
        }
        if (this.rockSkin === true) {
            dice += 2;
        }
        return dice;
    }

    isSlowedDown() {
        // TODO: update for Prince Magnus' gold scenario
        return this.hasEquippedArmour() === true && this.equippedArmour.limitsMovement === true;
    }

    getMovesDice() {
        var dice = this.movesDice;
        if (this.isSlowedDown() === true) {
            dice = 1;
        }
        if (this.doubleMovement === true) {
            dice *= 2;
            this.doubleMovement = false;
        }
        return dice;
    }

    coordinatesAreInAttackRange(coordinates) {
        const charNode = this.gameState.mapGraph.nodes.get(this.coordinates);
        const blockedCoords = this.getBlockedLastSquareCoordinates();

        if (this.isReadyToCastSpell() === true) {
            const targetNode = this.gameState.mapGraph.nodes.get(coordinates);
            const isVisible = checkTargetIsVisible(charNode, targetNode, this.gameState.mapGraph, blockedCoords, true);
            return isVisible;
        }

        if (this.hasEquippedWeapon() === true) {

            if (this.equippedWeapon.diagonalAttack === true) {
                const targetNode = this.gameState.mapGraph.nodes.get(coordinates);
                const isAdjacent = charNode.isAdjacent(coordinates);
                const nodeDistance = charNode.calculateNodeDistance(targetNode);
                const isDiagonal =  nodeDistance[0] === 1 && nodeDistance[1] === 1;
                const isVisible = checkTargetIsVisible(charNode, targetNode, this.gameState.mapGraph, blockedCoords);
                return isAdjacent || (isDiagonal && isVisible);
            }

            if (this.equippedWeapon.ranged === true) {
                const targetNode = this.gameState.mapGraph.nodes.get(coordinates);
                const isAdjacent = charNode.isAdjacent(coordinates);
                const isVisible = checkTargetIsVisible(charNode, targetNode, this.gameState.mapGraph, blockedCoords, true);
                return !(isAdjacent) && isVisible;
            }

            if (this.equippedWeapon.thrown === true) {
                const targetNode = this.gameState.mapGraph.nodes.get(coordinates);
                const isVisible = checkTargetIsVisible(charNode, targetNode, this.gameState.mapGraph, blockedCoords, true);
                return isVisible;
            }

        }
        return charNode.isAdjacent(coordinates);
    }

    rollForMovement(movesDice) {
        const moveResults = [];
        for (let i = 0; i < movesDice; i++) {
            const roll = Math.floor(Math.random() * 5) + 1;
            moveResults.push(roll);
        }
        this.moves = moveResults.reduce((elem1, elem2) => elem1 + elem2);
        const moves = this.moves;
        this.logAction(`rolled a total of ${moves} for movement`, this.name);
    }

    drawTreasureCard() {
        const treasureDeck = this.gameState.treasureDeck;
        const drawIndex = Math.floor(Math.random() * treasureDeck.length);
        var card = treasureDeck[drawIndex];
        card.func(this);
        if (card.removeFromDeck) {
            treasureDeck.splice(drawIndex, 1);
        }
    }

    checkCharacterCanSearch() {
        const state = this.gameState;
        if (this.hasActedThisTurn) {
            this.logAction("already acted this turn", this.name);
            return false;
        }
        if (this.checkEnemiesAreInSight()) {
            this.logAction("cannot search when there are enemies in sight", this.name);
            return false;
        }
        return true;
    }

    searchForTrapsAndSecretDoorsInRoom() {
        const state = this.gameState;
        const currentRoom = this.getCurrentRoom();
        if (state.roomsSearchedForTrapsAndSecretDoors.includes(currentRoom)) {
            this.logAction("This room was already searched for traps and secret doors");
            return;
        }

        // Check if there are any special traps related to treasure
        if (currentRoom in state.specialTrapSearches) {
            const specialTrapSearch = state.specialTrapSearches[currentRoom];
            specialTrapSearch.func(this);
            this.hasActedThisTurn = true;
            if (specialTrapSearch.removeFromDeck === true) {
                delete state.specialTrapSearches[currentRoom];
            }
        }

        // Reveal any traps found in the room
        this.logAction("searches the room for traps and secret doors", this.name);
        const trapsInRoom = state.trapsInPlay.filter(
            trap => trap.getCurrentRoom() === currentRoom
        );
        const secretDoorsInRoom = state.secretDoorsInPlay.filter(
            secretDoor => secretDoor.getCurrentRooms().includes(currentRoom)
        );
        trapsInRoom.forEach(trap => trap.reveal());
        secretDoorsInRoom.forEach(secretDoor => secretDoor.reveal());
        state.roomsSearchedForTrapsAndSecretDoors.push(currentRoom);
        this.hasActedThisTurn = true;
        this.updateActiveCharacterDisplay();
        this.checkHeroTurnNeedsToCycle();
    }

    searchForSecretDoorsAndTraps() {
        if (this.checkCharacterCanSearch() === false) {
            return;
        }
        const state = this.gameState;
        const currentRoom = this.getCurrentRoom();
        if (currentRoom !== undefined) {
            // Acted this turn set in here
            this.searchForTrapsAndSecretDoorsInRoom();
            return;
        } else {
            // corridor search logic
            const currentNode = this.getCurrentNode();
            const coordinatesToCheck = [];
            state.mapGraph.nodes.forEach((targetNode, coordinates) => {
                if (state.corridorCoordinatesSearchedForSecretDoorsAndTraps.includes(coordinates)) {
                    return;
                }
                if (targetNode.rooms.length > 0) {
                    return;
                }
                const visible = checkTargetIsVisible(
                    currentNode,
                    targetNode,
                    this.gameState.mapGraph
                );
                if (visible === true) {
                    coordinatesToCheck.push(targetNode.coordinates);
                }
            });
            if (coordinatesToCheck.length > 0) {
                this.logAction("searches the corridor for traps and secret doors", this.name);
                const trapsToReveal = state.trapsInPlay.filter(
                    trap => trap.visible === false && coordinatesToCheck.includes(trap.coordinates)
                );
                const secretDoorsToReveal = state.secretDoorsInPlay.filter(
                    secretDoor => secretDoor.visible === false && 
                    (coordinatesToCheck.includes(secretDoor.coordinates[0]) || coordinatesToCheck.includes(secretDoor.coordinates[1])) 
                );
                const allObjectsToReveal = trapsToReveal.concat(secretDoorsToReveal);
                allObjectsToReveal.forEach(
                    mapObject => mapObject.reveal()
                );
                coordinatesToCheck.forEach(
                    coordinates => state.corridorCoordinatesSearchedForSecretDoorsAndTraps.push(coordinates)
                );
                this.hasActedThisTurn = true;
            }
            this.logAction("This corridor was already searched for traps and secret doors");
        }
        this.updateActiveCharacterDisplay();
        this.checkHeroTurnNeedsToCycle();
    }

    searchForTreasure() {
        if (this.checkCharacterCanSearch() === false) {
            return;
        }
        const state = this.gameState;
        const currentRoom = this.getCurrentRoom();
        if (currentRoom !== undefined) {
            if (state.roomsSearchedForTreasure.includes(currentRoom)) {
                this.logAction("This room was already searched for treasure");
                return;
            }
            if (currentRoom in state.specialTreasureSearches) {
                const specialSearch = state.specialTreasureSearches[currentRoom];
                specialSearch.func(this);
                this.hasActedThisTurn = true;
                if (specialSearch.removeFromDeck === true) {
                    delete state.specialTreasureSearches[currentRoom];
                }
                state.roomsSearchedForTreasure.push(currentRoom);
                return;
            }
            this.logAction("searches the room for treasure", this.name);
            this.drawTreasureCard();
            state.roomsSearchedForTreasure.push(currentRoom);
            this.hasActedThisTurn = true;
            this.updateActiveCharacterDisplay();
            this.checkHeroTurnNeedsToCycle();
            return;
        }

        const currentNode = this.getCurrentNode();
        const nodesToCheck = [];
        state.mapGraph.nodes.forEach((targetNode, coordinates) => {
            if (state.corridorCoordinatesSearchedForTreasure.includes(coordinates)) {
                return;
            }
            if (targetNode.rooms.length > 0) {
                return;
            }
            const visible = checkTargetIsVisible(
                currentNode,
                targetNode,
                this.gameState.mapGraph
            );
            if (visible === true) {
                nodesToCheck.push(targetNode);
            }
        });
        if (nodesToCheck.length > 0) {
            this.logAction("searches the corridor for treasure", this.name);
            this.drawTreasureCard();
            nodesToCheck.forEach(
                node => state.corridorCoordinatesSearchedForTreasure.push(node.coordinates)
            );
            this.hasActedThisTurn = true;
            this.updateActiveCharacterDisplay();
            this.checkHeroTurnNeedsToCycle();
            return;
        }
        this.logAction("This corridor was already searched for treasure");
    }

    checkTargetIsInSight(target, strict=true) {
        const charNode = this.getCurrentNode();
        const targetNode = target.getCurrentNode();

        if (this.getCurrentRoom() !== undefined && this.getCurrentRoom() === target.getCurrentRoom()) {
            return true;
        }

        var blockedList = [];
        if (strict === true) {
            blockedList = this.getBlockedLastSquareCoordinates();
        }

        return checkTargetIsVisible(
            charNode,
            targetNode,
            this.gameState.mapGraph,
            blockedList
        );
    }

    checkCanDisarmTraps() {
        for (let i = 0; i < this.utilityObjects.length; i++) {
            const utilObj = this.utilityObjects[i];
            if (utilObj.canDisarmTraps === true) {
                return true;
            } 
        }
        return false;
    }

    disarmTrap(trap) {
        if (this.checkCanDisarmTraps() === false) {
            this.logAction("does not have the skills or tools to disarm", this.name);
            return;  
        }
        const charNode = this.gameState.mapGraph.nodes.get(this.coordinates);
        const adjacent = charNode.isAdjacent(trap.coordinates);
        if (adjacent === false) {
            this.logAction("is not in range for removing the trap", this.name);
            return;
        }
        trap.disable();
        this.logAction("disarmed the trap!", this.name);
        this.hasActedThisTurn = true;
        this.checkHeroTurnNeedsToCycle();
    }

    processDeath() {
        const gameState = this.gameState;
        for(let i = 0; i < gameState.charactersInPlay.length; i++){ 
            if (gameState.charactersInPlay[i].coordinates === this.coordinates) {
                const deadChar = gameState.charactersInPlay[i];
                gameState.charactersInPlay.splice(i, 1); 
                gameState.deadCharacters.push(deadChar);
            }
        }

        // If there are no characters in play, scenario ends
        if (gameState.charactersInPlay.length === 0) {
            gameState.currentScenarioIsOver = true;

            // If all characters are also dead, scenario is lost
            if (gameState.escapedCharacters.length === 0) {
                gameState.currentScenarioIsLost = true;
                // TODO - what happens now that things have ended?
            }
            // Defender not removed from canvas in this case
            return;
        }

        this.removeFromCanvas();
        return;
    }
}

class Barbarian extends Hero {
    constructor(coordinates, gameState, name = 'Barbarian', visible = true, moves  = 6) {
    	super(coordinates, BarbarianImage, gameState, name, 8, 2, 3, 2, 2, visible, moves);
    }
}

class Dwarf extends Hero {
    constructor(coordinates, gameState, name = 'Dwarf', visible = true, moves  = 6) {
        super(coordinates, DwarfImage, gameState, name, 7, 3, 2, 2, 2, visible, moves);
    }

    checkCanDisarmTraps() {
        return true;
    }
}

class Elf extends Hero {
    constructor(coordinates, gameState, name = 'Elf', visible = true, moves  = 6) {
        super(coordinates, ElfImage, gameState, name, 6, 4, 2, 2, 2, visible, moves);
    }
}

class Wizard extends Hero {
    constructor(coordinates, gameState, name = 'Wizard', visible = true, moves  = 6) {
        super(coordinates, WizardImage, gameState, name, 4, 6, 1, 2, 2, visible, moves);
        this.spells = spells;
    }
}

class Monster extends Character {
    constructor(
        coordinates, 
        image, 
        gameState,
        name,
        bodyPoints = 1,
        mindPoints = 1,
        attackDice = 1,
        defenseDice = 1,
        visible = true, 
        moves = 0
    ) {
        super(
            coordinates,
            image,
            gameState,
            name,
            bodyPoints,
            mindPoints,
            attackDice,
            defenseDice,
            visible,
            moves
        );
    }

    processDeath() {
        const gameState = this.gameState;
        for(let i = 0; i < gameState.monstersInPlay.length; i++){ 
            if (gameState.monstersInPlay[i].coordinates === this.coordinates) {
                gameState.monstersInPlay.splice(i, 1); 
            }
        }
        this.removeFromCanvas();
    }


    runMonsterTurn() {
        if (this.losesTurn) {
            this.logAction("skips a turn", this.name);
            this.losesTurn = false;
            return;
        }

        this.attemptToAwake();
        if (this.isAsleep) {
            this.logAction("is still asleep and skips a turn", this.name);
            return;
        }

        // If there is a visible character in range, attack
        this.logAction("starts his turn", this.name);
        const charactersInPlay = this.gameState.charactersInPlay;
        const impassableCoords = this.getImpassableCoordinates();
        const blockedLastSquareCoords = this.getBlockedLastSquareCoordinates();
        const startNode = this.getCurrentNode();
        const mapGraph = this.gameState.mapGraph;
        const moves = this.moves;
        const monsterCoords = this.coordinates;
        const monster = this;
        const possibleTargets = [];

        // Calculate if there is a viable attack path; if so, attack; if more than one
        // prioritize closest character and then character with the least body points
        for (let i = 0; i < charactersInPlay.length; i++) {
            const character = charactersInPlay[i];
            const charNode = character.getCurrentNode();
            const adjacentNodes = Array.from(charNode.getAdjacents());

            // Edge case - monster is already adjacent to a character - exit early
            if (adjacentNodes.includes(monsterCoords)) {
                monster.attackEnemy(character);
                return;
            }

            for (let j = 0; j < adjacentNodes.length; j++) {

                const destCoords = adjacentNodes[j];
                if (blockedLastSquareCoords.includes(destCoords)) {
                    continue;
                }

                const destNode = mapGraph.nodes.get(destCoords);
                // Note that this will allow monsters to bypass revealed traps
                // TODO: fix
                const path = findCharacterMovementPath(
                    startNode,
                    destNode,
                    impassableCoords,
                    this.gameState
                );
                if (path.length > 0) {
                    const target = {
                        "path": path,
                        "character": character,
                        "distance": path.length -1,
                    };
                    possibleTargets.push(target);
                }                
            } 

        }
        if (possibleTargets.length > 0) {
            const sorted = possibleTargets.sort(compareMonsterAttackTargets);
            const toAttack = sorted[0];
            if (toAttack.distance <= moves) {
                this.updateCoordinates(toAttack.path[toAttack.path.length - 1]);
                this.drawMovement();
                this.attackEnemy(toAttack.character);
            } else {
                this.logAction("moves towards", this.name, toAttack.character.name);
                this.updateCoordinates(toAttack.path[this.moves - 1]);
                this.drawMovement();
            }
            return;
        }

        // If no viable path to any character, pass
        this.logAction("has no viable target", this.name);
        return;
    }
}

class Fimir extends Monster {
    constructor(
        coordinates,
        gameState,
        name = 'Fimir',
        bodyPoints = 1,
        mindPoints = 3,
        attackDice = 3,
        defenseDice = 3,
        visible = false, 
        moves = 6
    ) {
    	super(coordinates, FimirImage, gameState, name, bodyPoints, mindPoints, attackDice, defenseDice, visible, moves);
    }
}

class Zombie extends Monster {
    constructor(
        coordinates,
        gameState,
        name = 'Zombie',
        bodyPoints = 1,
        mindPoints = 0,
        attackDice = 2,
        defenseDice = 3,
        visible = false, 
        moves = 4
    ) {
        super(coordinates, ZombieImage, gameState, name, bodyPoints, mindPoints, attackDice, defenseDice, visible, moves);
    }
}

class Mummy extends Monster {
    constructor(
        coordinates,
        gameState,
        name = 'Mummy',
        bodyPoints = 1,
        mindPoints = 0,
        attackDice = 3,
        defenseDice = 4,
        visible = false, 
        moves = 4
    ) {
        super(coordinates, MummyImage, gameState, name, bodyPoints, mindPoints, attackDice, defenseDice, visible, moves);
    }
}

class ChaosWarrior extends Monster {
    constructor(
        coordinates,
        gameState,
        name = 'Chaos Warrior',
        bodyPoints = 1,
        mindPoints = 3,
        attackDice = 3,
        defenseDice = 4,
        visible = false, 
        moves = 6
    ) {
        super(coordinates, ChaosWarriorImage, gameState, name, bodyPoints, mindPoints, attackDice, defenseDice, visible, moves);
    }
}

class Skeleton extends Monster {
    constructor(
        coordinates,
        gameState,
        name = 'Skeleton',
        bodyPoints = 1,
        mindPoints = 0,
        attackDice = 2,
        defenseDice = 2,
        visible = false, 
        moves = 6
    ) {
        super(coordinates, SkeletonImage, gameState, name, bodyPoints, mindPoints, attackDice, defenseDice, visible, moves);
    }
}

class Gargoyle extends Monster {
    constructor(
        coordinates,
        gameState,
        name = 'Gargoyle',
        bodyPoints = 1,
        mindPoints = 4,
        attackDice = 4,
        defenseDice = 4,
        visible = false, 
        moves = 6
    ) {
        super(coordinates, GargoyleImage, gameState, name, bodyPoints, mindPoints, attackDice, defenseDice, visible, moves);
        this.offset = [2, 4];
    }

    getWidthModifier() {
    	return 1.0;
    }
}

class Orc extends Monster {
    constructor(
        coordinates,
        gameState,
        name = 'Orc',
        bodyPoints = 1,
        mindPoints = 2,
        attackDice = 3,
        defenseDice = 2,
        visible = false, 
        moves = 8
    ) {
        super(coordinates, OrcImage, gameState, name, bodyPoints, mindPoints, attackDice, defenseDice, visible, moves);
    }
}

class Goblin extends Monster {
    constructor(
        coordinates,
        gameState,
        name = 'Goblin',
        bodyPoints = 1,
        mindPoints = 1,
        attackDice = 2,
        defenseDice = 1,
        visible = false, 
        moves = 10
    ) {
        super(coordinates, GoblinImage, gameState, name, bodyPoints, mindPoints, attackDice, defenseDice, visible, moves);
    }
}

export { 
	Barbarian,
    Dwarf,
    Elf,
    Wizard,
	ChaosWarrior,
	Fimir,
	Gargoyle,
	Goblin,
	Mummy,
	Orc,
	Skeleton,
	Zombie
};