import mapData from "./mapdata";
import { buildMapGraph } from './map-building';

import {
    AlchemistsBench,
    Bookcase,
    Cupboard, 
    Door,
    FallingBoulderTrap,
    Fireplace,
    OneSquareBlock,
    PieceOfFurniture,
    PitTrap,
    Rack,
    Table,
    Tomb,
    Throne,
    SecretDoor,
    SorcerersTable,
    SpearTrap,
    Stairs,
    TreasureChest,
    TwoSquareBlock,
    WeaponsRack
} from './map-objects';

import {  
    Barbarian,
    Dwarf,
    Elf,
    Wizard,
    ChaosWarrior,
    Fimir,
    Gargoyle,
    Goblin,
    Mummy,
    Orc,
    Skeleton,
    Zombie
} from './characters';

import {
    generateTreasureCards,
    goldTreasure
} from './treasure-cards';

const gameState = {
    "mapGraph": null,
    "activeCharacter": null,
    "blocksInPlay": [],
    "charactersInPlay": [],
    "monstersInPlay": [],
    "doorsInPlay": [],
    "furnitureInPlay": [],
    "trapsInPlay": [],
    "secretDoorsInPlay": [],
    "currentScenario": null,
    "fabricContext": null,
    "currentScenarioIsWon": false,
    "currentScenarioIsLost": false,
    "escapedCharacters": [],
    "deadCharacters": [],
    "currentScenarioIsOver": false,
    "roomsSearchedForTreasure": [],
    "roomsSearchedForTrapsAndSecretDoors": [],
    "corridorCoordinatesSearchedForTreasure": [],
    "corridorCoordinatesSearchedForSecretDoorsAndTraps": [],
    "specialTreasureSearches": {},
    "specialTrapSearches": {},
    "wanderingMonster": null,
    "logElementId": "gameLog"
};

function initiateScenario(context, map=mapData, scenarioNo=1) {
        const newScenario = scenarioData[scenarioNo];
        gameState.blocksInPlay = newScenario.blocks;
        gameState.startingCharacters = newScenario.characters;
        gameState.charactersInPlay = newScenario.characters;
        gameState.monstersInPlay = newScenario.monsters;
        gameState.doorsInPlay = newScenario.doors;
        gameState.furnitureInPlay = newScenario.furniture;
        gameState.trapsInPlay = newScenario.traps;
        gameState.secretDoorsInPlay = newScenario.secretDoors;
        gameState.mapGraph = buildMapGraph(map);
        gameState.fabricContext = context;
        gameState.specialTreasureSearches = newScenario.specialTreasureSearches;
        gameState.wanderingMonster = newScenario.wanderingMonster;
        gameState.roomsSearchedForTreasure = [];
        gameState.corridorCoordinatesSearchedForTreasure = [];
        gameState.currentScenarioIsWon = false;
        gameState.currentScenarioIsLost = false;
        gameState.escapedCharacters = [];
        gameState.deadCharacters = [];
        gameState.currentScenarioIsOver = false;
        gameState.treasureDeck = generateTreasureCards();
        gameState.unreachableCorridorCoords = newScenario.unreachableCorridorCoords;
}

function setupBlocks() {
    const blocksData = gameState.blocksInPlay;
    for (let i = 0; i < blocksData.length; i++) {
        const block = blocksData[i];
        block.blockGridCoordinates();
        if (block.visible) {
            block.draw();
        }
    }
}

function setupDoors() {
    const doorData = gameState.doorsInPlay;
    for (let i = 0; i < doorData.length; i++) {
        const door = doorData[i];
        if (door.visible) {
            door.draw();
        }
    }
}

function setupTraps() {
    const trapData = gameState.trapsInPlay;
    for (let i = 0; i < trapData.length; i++) {
        const trap = trapData[i];
        if (trap.visible) {
            trap.draw();
        }
    }
}

function setupFurniture() {
    const furnitureData = gameState.furnitureInPlay;
    for (let i = 0; i < furnitureData.length; i++) {
        const pieceOfFurniture = furnitureData[i];
        pieceOfFurniture.blockGridCoordinates();
        if (pieceOfFurniture.visible) {
            pieceOfFurniture.draw();
        }
    }
}

function setupCharacters() {
    const characterData = gameState.charactersInPlay;
    for (let i = 0; i < characterData.length; i++) {
        const character = characterData[i];
        if (character.visible) {
            character.draw();
        }
    }
}

function setupMonsters() {
    const monsterData = gameState.monstersInPlay;
    for (let i = 0; i < monsterData.length; i++) {
        const monster = monsterData[i];
        if (monster.visible) {
            monster.draw();
        }
    }
}

const scenarioData = {
    1: {
        "name": "The Trial",
        "blocks": [
            new OneSquareBlock(["0-1"], ["0-1"], gameState),
            new TwoSquareBlock(["12-5", "13-5"], ["12-5", "12-5"], gameState),
            new OneSquareBlock(["17-9"], ["17-9"], gameState),
            new OneSquareBlock(["14-18"], ["14-18"], gameState)
        ],
        "doors": [
            new Door(["8-15", "9-15"], gameState),
            new Door(["8-2", "9-2"], gameState),
            new Door(["4-2", "5-2"], gameState),
            new Door(["3-3", "3-4"], gameState),
            new Door(["15-9", "16-9"], gameState),
            new Door(["13-15", "14-15"], gameState),
            new Door(["3-18", "3-17"], gameState, true),
            new Door(["0-11", "1-11"], gameState),
            new Door(["10-12", "10-13"], gameState),
            new Door(["7-17", "7-18"], gameState),
            new Door(["7-8", "7-9"], gameState),
            new Door(["3-8", "3-9"], gameState)
        ],
        "furniture": [
            new Table(["11-9", "13-9"], ["11-9", "11-10"], gameState),
            new Table(["8-5", "6-5"], ["6-5", "6-6"], gameState),
            new Rack(["3-10", "4-10"], ["3-10", "3-12"], gameState),
            new Cupboard(["15-13", "17-16"], ["15-13", "15-13"], gameState),
            new Bookcase(["15-17", "17-17"], ["15-17", "15-17"], gameState),
            new Bookcase(["5-13", "7-13"], ["5-13", "5-13"], gameState),
            new AlchemistsBench(["5-15", "6-15"], ["5-15", "5-17"], gameState),
            new TreasureChest(["1-1"], ["1-1"], gameState),
            new TreasureChest(["9-17"], ["9-17"], gameState),
            new TreasureChest(["10-5"], ["10-5"], gameState),
            new Throne(["10-8"], ["10-8"], gameState),
            new WeaponsRack(["11-15", "11-15"], ["11-15", "11-17"], gameState),
            new Stairs(["1-14", "2-14"], ["1-14", "1-15"], gameState, true),
            new Fireplace(["12-7", "14-7"], ["12-7", "12-7"], gameState),
            new SorcerersTable(["1-5", "2-5"], ["1-5", "1-7"], gameState),
            new Tomb(["10-1", "11-1"], ["10-1", "10-3"], gameState)
        ],
        "characters": [
            new Wizard("1-16", gameState),
            new Barbarian("2-16", gameState),
            new Dwarf("3-15", gameState)
        ],
        "monsters": [
            new Gargoyle("12-8", gameState),
            new Goblin("0-13", gameState),
            new Goblin("10-14", gameState),
            new Goblin("2-11", gameState),
            new Goblin("6-7", gameState),
            new Goblin("7-7", gameState),
            new Goblin("3-6", gameState),
            new Orc("2-12", gameState),
            new Orc("8-15", gameState),
            new Orc("12-18", gameState),
            new Orc("7-14", gameState),
            new Fimir("10-16", gameState),
            new Fimir("17-14", gameState),
            new Fimir("14-8", gameState),
            new Mummy("6-2", gameState, 'Guardian of Fellmarg\'s Tomb', 1, 0, 4, 4, false, 4),
            new Mummy("9-4", gameState),
            new Skeleton("2-2", gameState),
            new Skeleton("3-2", gameState),
            new Skeleton("9-3", gameState),
            new Skeleton("10-4", gameState),
            new ChaosWarrior("14-11", gameState),
            new ChaosWarrior("11-11", gameState),
            new ChaosWarrior("16-15", gameState),
            new ChaosWarrior("15-16", gameState),
            new Zombie("6-1", gameState),
            new Zombie("6-3", gameState)
        ],
        "secretDoors": [
            new SecretDoor(["0-17", "1-17"], gameState),
            new SecretDoor(["2-18", "2-17"], gameState)
        ],
        "specialTreasureSearches": {
            0: {"func": goldTreasure(100, "You look into the chest, and you find 100 gold coins."), "removeFromDeck": true},
            9: {"func": goldTreasure(50, "You look into the chest, and you find 150 gold coins."), "removeFromDeck": true},
            10: {"func": goldTreasure(50, "You look into the chest, but it seems to be empty."), "removeFromDeck": true},
        },
        "traps": [
            new FallingBoulderTrap("3-16", gameState, "4-16"),
            new SpearTrap("3-14", gameState)
        ],
        "wanderingMonster": null,
        "unreachableCorridorCoords": [
            "0-0"
        ]
    }
};

export {
    gameState,
    scenarioData,
    initiateScenario,
    setupBlocks,
    setupDoors,
    setupFurniture,
    setupCharacters,
    setupMonsters,
    setupTraps
};
