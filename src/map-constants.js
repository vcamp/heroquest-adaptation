const padding = 10;
const cellWidth = 50;
const horizontalCellCount = 26;
const verticalCellCount = 19;
const boxWidth = cellWidth * horizontalCellCount;
const boxHeight = cellWidth * verticalCellCount;

export { 
	cellWidth,
	padding,
	horizontalCellCount,
	verticalCellCount,
	boxWidth,
	boxHeight
};