import { 
    cellWidth,
    padding 
} from './map-constants';

function getIntegerCoords(strCoords) {
    const split = strCoords.split("-");
    return [parseInt(split[0]), parseInt(split[1])];
}

function setIntersection(setA, setB) {
  // From https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set
  const _intersection = new Set();
  for (const elem of setB) {
    if (setA.has(elem)) {
      _intersection.add(elem);
    }
  }
  return _intersection;
}

function getTopLeftCanvasSquareCoords(left, top) {
    return [
        padding + (left * cellWidth),
        padding + (top * cellWidth)
    ];
}

function logAction(verb, gameState, actor='', target = '') {
      const logElementId = gameState.logElementId;
      if (logElementId !== undefined) {
          const logElement = document.getElementById(logElementId);
          var newText = "\n";
          if (actor !== '') {
              newText += (actor + " ");
          }
          newText += verb;
          if (target !== '') {
              newText += (" " + target);
          }
          logElement.textContent += newText;
          logElement.scrollTop = logElement.scrollHeight;
      }
  }


export { 
    getIntegerCoords,
    setIntersection,
    getTopLeftCanvasSquareCoords,
    logAction
};
