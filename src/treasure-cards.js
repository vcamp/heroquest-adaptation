import {
    PowerUpPotion,
    HealingPotion,
    HolyWater
} from './character-equipment';

function generateTreasureCards() {
    return [
        {"func": goldTreasure(50, "Tucked into the toe of an old boot you find a valuable gem. The stone is worth 50 coins."), "removeFromDeck": true},
        {"func": goldTreasure(10, "A meagre haul of just 10 gold coins is found in the pocket of a smelly, worn jerkin."), "removeFromDeck": true},
        {"func": goldTreasure(20, "A rummage through several items of clothing reveals 20 gold coins."), "removeFromDeck": true},
        {"func": goldTreasure(25, "Amidst the clutter, the old rags, the greasy fur robes and soiled blankets, you find 25 gold coins."), "removeFromDeck": true},
        {"func": goldTreasure(25, "Foolishly left unhidden lies a small box containing 25 gold coins."), "removeFromDeck": true},
        {"func": goldTreasure(50, "You find a loose stone behind which is hidden a small leather pouch wrapped in an old rag. You look inside the pouch and find 50 gold coins."), "removeFromDeck": true},
        {"func": goldTreasure(50, "You find a small wooden box. The box is plain and old, but within... it is lined with velvet and contains 50 gold coins' worth of jewels."), "removeFromDeck": true},
        {"func": goldTreasure(100, "Luck is with you. A small treasure chest you find, hidden under an old fur, contains 100 gold coins."), "removeFromDeck": true},
        {"func": goldTreasure(0, "Despite a thorough search you find nothing."), "removeFromDeck": true},
        {"func": goldTreasureWithTimeLoss, "removeFromDeck": true},
        {"func": powerUpPotion("doubleAttack", "Heroic Brew", "A leather bag hanging on the wall contains a potion. It is a Heroic Brew.", "The potion may be taken just before you are about to attack. Any player who drinks the potion will be able to make two attacks instead of one, for one turn only. The potion is then discarded."), "removeFromDeck": true},
        // Note that it should be possible to take this one before a monster attack, but that'd require to allow players to control the steps of the monsters turn
        {"func": powerUpPotion("potionOfResilience", "Potion of Resilience", "Amdist a collection of old bottles and earthen jugs you find a small clear vial, a Potion of Resilience.", "It may be taken at any time. You may then roll two extra combat dice in defense when you next defend. The potion is then discarded."), "removeFromDeck": true},
        {"func": powerUpPotion("doubleMovement", "Potion of Speed", "Standing on a shelf you see a dusty bottle. As you wipe it clean, you realize it is a Potion of Speed.", "It may be taken at any time. It will allow you to roll twice as many dice as usual the next time you move. The potion is then discarded."), "removeFromDeck": true},
        {"func": powerUpPotion("potionOfStrength", "Potion of Strength", "You find a small purple bottle. It is a Potion of Strength.", "It may be taken at any time. It will enable you to roll two extra combat dice in attack in your next attack. The potion is then discarded."), "removeFromDeck": true},
        {"func": healingPotion("Potion of Healing", "Enveloped in a bundle of rags you find a small bottle of liquid. You recognise it as a healing potion.", "It may be taken at any time. It will restore up to four lost Body points. The potion is then discarded."), "removeFromDeck": true},
        {"func": healingPotion("Potion of Healing", "Enveloped in a bundle of rags you find a small bottle of liquid. You recognise it as a healing potion.", "It may be taken at any time. It will restore up to four lost Body points. The potion is then discarded."), "removeFromDeck": true},
        {"func": holyWater("Holy Water", "Discarded and forgotten in a corner of the room you find a vial of Holy Water.", "You may use the Holy Water instead of attacking. It will kill any undead creatures: Skeleton, Zombie, Mummy. Discard after use."), "removeFromDeck": true},
        {"func": wanderingMonster("As you are busy searching, a monster stalks up on you and attacks!"), "removeFromDeck": false},
        {"func": wanderingMonster("As you are busy searching, a monster stalks up on you and attacks!"), "removeFromDeck": false},
        {"func": wanderingMonster("As you are busy searching, a monster stalks up on you and attacks!"), "removeFromDeck": false},
        {"func": wanderingMonster("As you are busy searching, a monster stalks up on you and attacks!"), "removeFromDeck": false},
        {"func": wanderingMonster("As you are busy searching, a monster stalks up on you and attacks!"), "removeFromDeck": false},
        {"func": trap(1, "As you search, you unwittingly set off a trap. You must lose one body point."), "removeFromDeck": false},
        {"func": trap(1, "You feel the gentle pressure of a trip wire against your leg, you spin round... but it is too late. You lose one body point from the crossbow bolt that shoots from the wall."), "removeFromDeck": false},
        {"func": trap(1, "The stone beneath your foot begins to give way and all too late you realize it is a trap. You fall into a pit and lose one body point. You may climb out and move as normal on your next turn. The pit then closes.", true), "removeFromDeck": false},
    ];
}

function goldTreasure(quantity, message) {
    const q = quantity;
    const m = message;
    return function (character, quantity=q, message=m) {
        character.logAction(message);
        character.gold += quantity;
    };
}

function powerUpPotion(property, name, message, description) {
    const p = property;
    const n = name;
    const m = message;
    const d = description;
    return function (character, name=n, message=m, property=p, description=d) {
        character.logAction(message);
        const potion = new PowerUpPotion(name, description, property);
        potion.addToInventory(character);
    };
}

function healingPotion(name, message, description) {
    const n = name;
    const m = message;
    const d = description;
    return function (character, name=n, message=m, description=d) {
        character.logAction(message);
        const potion = new HealingPotion(name, description);
        potion.addToInventory(character);
    };
}

function holyWater(name, message, description) {
    const n = name;
    const m = message;
    const d = description;
    return function (character, name=n, message=m, description=d) {
        character.logAction(message);
        const holyWater = new HolyWater(name, description);
        holyWater.addToInventory(character);
    };
}

function wanderingMonster(message) {
	const m = message;
    return function (character, message=m) {
        const monsterClass = character.gameState.wanderingMonster;
        if (monsterClass !== null) {
            const adjacentCoords = Array.from(character.getCurrentNode().getAdjacents());
            const unViableCoords = character.getBlockedLastSquareCoordinates();
            for (let i = 0; i < adjacentCoords.length; i++) {
                const coord = adjacentCoords[i];
                if (!(unViableCoords.includes(coord))) {
                    character.logAction(message);
                    const monster = new monsterClass(coord, true);
                    character.gameState.monstersInPlay.push(monster);
                    monster.draw();
                    monster.runMonsterTurn();
                    return;
                }
            }
            character.logAction("No space for wandering monster!");
            console.log('No space for wandering monster!');
            return;
        }
        character.logAction("No wandering monster defined for this scenario.");
        return;
    };
}

function trap(damage, message, blockMovement=false) {
    const b = blockMovement;
    const m = message;
    const d = damage;
    return function (character, damage=d, blockMovement=b, message=m) {
        character.logAction(message);
        character.bodyPoints -= d;
        if (character.bodyPoints <= 0) {
            character.processDeath();
        }
        if (blockMovement === true) {
            character.moves = 0;
            character.hasMovedThisTurn = true;
        }
    };
}

function goldTreasureWithTimeLoss(character) {
    const message = "You search and find small amounts of gold hidden in several places. But you lose track of time.";
    character.logAction(message);
    const roll = Math.floor(Math.random() * 6) + 1;
    character.gold += (roll * 10);
    character.losesTurn = true;
}

export {
	generateTreasureCards,
	goldTreasure,
	powerUpPotion,
	healingPotion,
	holyWater,
	wanderingMonster,
	trap,
	goldTreasureWithTimeLoss
};