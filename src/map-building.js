import { Graph } from './data-structures';

import { 
    getIntegerCoords,
    getTopLeftCanvasSquareCoords
} from './utils';

import { 
    cellWidth,
    padding,
    horizontalCellCount,
    verticalCellCount,
    boxWidth,
    boxHeight
} from './map-constants';

function buildMapGraph(mapData) {
	var mapGraph = new Graph(mapData);

	for (var i = 0; i < Object.keys(mapData).length; i++) {
	    const nodeName = Object.keys(mapData)[i];
	    const nodeAdjacents = mapData[nodeName].adjacencyList;
	    nodeAdjacents.forEach(function(adjacent) {
	        mapGraph.addEdge(nodeName, adjacent);
	    });
	}

	return mapGraph;
}

// TODO: refactor this monstrosity
function getRoomLines(mapData) {
    const horizontal = {};
    const vertical = {};
    const xIntMap = {};
    const yIntMap = {};

    // Map string to integer coordinates for ease of
    // use
    for (let key of Object.keys(mapData)) {
        const [xInt, yInt] = getIntegerCoords(key);

        if (xInt in xIntMap) {
            xIntMap[xInt].push(key);
        } else {
            xIntMap[xInt] = [key];
        }

        if (yInt in yIntMap) {
            yIntMap[yInt].push(key);
        } else {
            yIntMap[yInt] = [key];
        }

    }

    for (var i = 0; i < Math.max(...Object.keys(yIntMap)); i++) {
        const output = [];

        yIntMap[i].forEach((coords) => {
            const [xNext, yNext] = getIntegerCoords(coords);
            const checkNext = xNext.toString() + "-" + (yNext + 1).toString();
            if (!(mapData[coords].adjacencyList.includes(checkNext))) {
                if (output.length === 0) {
                    output.push([xNext, xNext + 1]);
                } else if (output) {
                    const lastOutputArr = output[output.length - 1];
                    const lastElem = lastOutputArr[lastOutputArr.length - 1];
                    if ((lastElem) === xNext) {
                        lastOutputArr.splice(1, 1, xNext + 1);
                    } else {
                        output.push([xNext, xNext + 1]);
                    }
                }
            }
        });

        if (output.length > 0) {
            horizontal[i + 1] = output;
        }

    }

    for (var i = 0; i < Math.max(...Object.keys(xIntMap)); i++) {
        const output = [];

        xIntMap[i].forEach((coords) => {
            const [xNext, yNext] = getIntegerCoords(coords);
            const checkNext = (xNext + 1).toString() + "-" + yNext.toString();
            if (!(mapData[coords].adjacencyList.includes(checkNext))) {
                if (output.length === 0) {
                    output.push([yNext, yNext + 1]);
                } else if (output) {
                    const lastOutputArr = output[output.length - 1];
                    const lastElem = lastOutputArr[lastOutputArr.length - 1];
                    if ((lastElem) === yNext) {
                        lastOutputArr.splice(1, 1, yNext + 1);
                    } else {
                        output.push([yNext, yNext + 1]);
                    }
                }
            }
        });

        if (output.length > 0) {
            vertical[i + 1] = output;
        }
    }

    return {'horizontal': horizontal, 'vertical': vertical};

}

function drawBoard(context) {
    // Grid vertical lines
    context.beginPath();
    for (var x = 0; x <= boxWidth; x += cellWidth) {
        context.moveTo(x + padding, padding);
        context.lineTo(x + padding, boxHeight + padding);
        // Grid helpers
        context.fillText((x / cellWidth).toString(), x + (cellWidth/ 2) + padding, padding);
    }

    // Grid horizontal lines
    for (var x = 0; x <= boxHeight; x += cellWidth) {
        context.moveTo(padding, x + padding);
        context.lineTo(boxWidth + padding, x + padding);
        // Grid helpers
        context.fillText((x / cellWidth).toString(), 0, x + (cellWidth/ 2) + padding);
    }
    context.strokeStyle = "black";
    // context.lineWidth = 1;
    context.stroke();
    context.closePath();
}

function drawRooms(context, roomLines) {
    context.beginPath();
    context.lineWidth = 3;
    for (var yCoord in roomLines.horizontal) {
        const yVal = yCoord * cellWidth;
        roomLines.horizontal[yCoord].forEach(
            (xCoords) => {
                context.moveTo(
                    padding + (xCoords[0] * cellWidth),
                    padding + yVal
                );
                context.lineTo(
                    padding + (xCoords[1] * cellWidth),
                    padding + yVal
                );                
            }
        );
    }
    for (var xCoord in roomLines.vertical) {
        const xVal = xCoord * cellWidth;
        roomLines.vertical[xCoord].forEach(
            (yCoords) => {
                context.moveTo(
                    padding + xVal,
                    padding + (yCoords[0] * cellWidth)
                );
                context.lineTo(
                    padding + xVal,
                    padding + (yCoords[1] * cellWidth)
                );                
            }
        );
    }
    context.stroke();
    context.closePath();
}


export { 
	buildMapGraph,
    drawBoard,
    drawRooms,
    getRoomLines
};